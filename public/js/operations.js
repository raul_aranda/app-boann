/**
 * Created by raul on 1/02/17.
 */
/*function productos (){
    $.ajax({
        url:   'productos',
        type:  'get',
        beforeSend: function () {
            $("#producto").html("Procesando, espere por favor...");
        },
        success:  function (data) {
            $("#producto").html(data);
        },
        error: function () {
            alert ("error");
        }
    });
}*/

//calcula los gastos de envio
function gastosEnvio(){

    if ($("#idUser").val() != '43000000584'){
        $("#txtCodEnvio").val('0001PORTES');
        return;
    }
    //if (666666)
    //return;

    var newArray = new Array();
    var sum = 0.00;
    //var max = parseFloat($("#txtEnvio").val().replace(',', '.'));
    var cal = 0.00;
    var tra = $("#txtTrans").val();

    var values = $("input[id='task']") .map(function(){
        sum = sum + parseFloat($(this).val().replace(',', '.'));
        return $(this).val();
    }).get();

    $.ajax({
        url:'frontend/gastosEnvio',
        type: 'get',
        data: {
            'id': tra
        },
        success: function (data){
            if (data.nombre == 'ECI' || data.nombre == 'Correos'){
                $("#txtCodEnvio").val('0001PORTES');
                return;
            }

            if (data.nombre != 'Correos'){
                $("#txtEnvio").val(data.gratis)
                $("#txtDescuento").val(data.descuento)
                $("#txtMinDescuento").val(data.minDescuento);
                if (sum >= data.gratis){
                    $("#gastosEnvio").text("Gastos de envío GRATIS");
                    $("#txtCodEnvio").val('PORTESSC');
                }else if (sum >= data.minDescuento) {
                    $("#gastosEnvio").text("Gastos envio:" + data.descuento);
                    $("#txtCodEnvio").val('PORTES50');
                }else{
                    cal = data.gratis - sum;
                    $("#gastosEnvio").text("Añade artículos por valor de  " + cal.toFixed(2) + "€ para tener gastos de envío gratuitos");
                    $("#txtCodEnvio").val('0001PORTES');
                }
            }

            $("#total").text('Total artículos: '+ sum.toFixed(2));

        },
        error: function () {
            alert ("error gastos envio");
        }
    })
}



//click modelo
$("#option1").change(function (){
    var mod = $('#selectModelo option:selected').val();
    $("#txtModelo").val(mod);


});

//clik color
$("#option2").change(function (){
    var id = $('#option2 option:selected').val();
    var color = $('#option2 option:selected').val();

    $("#txtcolor").val(color);

    $.ajax({
        url:'frontend/createTalla',
        type: 'get',
        data: {
            'id':id
        },
        success: function (data){
            $("#option3").html(data.option3);
        },
        error: function () {
            alert ("error talla");
        }
    })
});

//click dibujo
$("#dibujo").change(function (){
    $("#txtTalla").val($('#dibujo option:selected').val());
});

//click opción dibujo
$("#opcion").change(function (){
    $("#txtTetina").val($('#opcion option:selected').val());
});

//clik talla
$("#option3").change(function (){
    var id = $('#option3 option:selected').val();
    var idTetina = $('#option3 option:selected').attr("id");

    if (id == 'Tetina/Talla') id=' ';
    var tallatetina = id.split("_");
    var talla = tallatetina[1];
    var tetina = tallatetina[0];
    //var idTalla = tetina.split("?");
    //var tetina1 = idTalla[1];
    //var tetina2 = idTalla[0];

    $("#txtTalla").val(talla);
    $("#txtTetina").val(tetina);
    $("#txtIdTetinaTalla").val(idTetina);

    $.ajax({
        url:'frontend/createGrabacion',
        type: 'get',
        data: {
            'id':idTetina
        },
        success: function (data){
            $("#option4").html(data);
        },
        error: function () {
            //alert ("error grabacion");
        }
    })
});

//limpia campos formulario
function clean(){
    $("#txtopcional").val('');
    $("#txtTetina").val('');
    $("#txtTalla").val('');
    $("#txtcantidad").val('1');
    $("#txtcolor").val('');
    $("#txtModelo").val('');
}

//click boton +
$("#acept").click(function (){
    var id = $('#option3 option:selected').val();
    var color = $('#option2 option:selected').val();

    $("#personalizado").val($('#personalizados').val());

    if (id == '--- ---') {
        var tallatetina = id.split(" ");
        var talla = tallatetina[1];
        var tetina = tallatetina[0];

        $("#txtTetina").val(tetina);
        $("#txtTalla").val(talla);
    }

    if (color == '-- --'){
        $('#option2').val(1).trigger('change');
    }

    $("#txtcolor").val(color);

    if ($("#dibujo").is(":visible")){
        $("#txtTetina").val($('#dibujo option:selected').val());
        $("#txtTalla").val($('#opcion option:selected').val());
    }

    gastosEnvio();

    $("#send").submit();

});


/*---------------------------------------------------*/
$("#producto").change(function () {
    var id = $('#producto option:selected').val();

    $("#txtproducto").val(id);
    $("#txtcantidad").val('1');
    $("#txtcolor").val('');

    $.ajax({
        url:    'frontend/createSelect',
        type:   'get',
        data: {
            'id': id
        },
        success: function (data) {
            //$("#colour").html(data.color);
            //$("#divTalla").html(data.talla);
            $("#grabacion").html(data.perso);
            selectDibujo = data.dibujo;
            //$("#frases").html(data.frase);

            //nuevo código
            if (data.option1 == null){
                $("#option1").hide();
            }else{
                $("#option1").show();
                $("#option1").html(data.option1);
            }
            $("#option2").html(data.option2);
            $("#option3").html(data.option3);
            $("#option4").html(data.option4);

            if (selectDibujo != null){
                $("#dibujo")
                    .show()
                    .html(data.dibujo);
                $("#opcion")
                    .show()
                    .html(data.opcion);
                $("#option3").hide();
            }else{
                $("#dibujo").hide();
                $("#opcion").hide();
                selectDibujo = null;
                $("#option3").show();
            }

            if ($("#selectModelo").is(":visible") || selectDibujo != null) {
                var model = $("#txtModelo").val();
                $("#option4").css({"width": "180px", "padding-left": "7px"});
            }else{
                var model = 'no';
                $("#option4").css("width", '330px');
            }

            var color = $('#option2 option:selected').val();
            if (color == '-- --'){
                $('#option2').val(1).trigger('change');
            }


            /*if(data.frase != 'false'){
                $("#personalizado").hide();
                $("#frases").show();
            }else{
                $("#frases").hide();
            }

            $("#pegatinas").html(data.opciones);

            if(data.opciones != 'false'){
                $("#divTalla").hide();
                $("#pegatinas").show();
            }else{
                $("#pegatinas").hide();
                $("#divTalla").show();
            }

            $('#colour').trigger('change');
            $('#divTalla').trigger('change');*/

            readCountLineas();
            readCountPedidos();

        },
        error: function () {
            alert ("error product");
        }
    });


});

$("#cantidad").change(function () {
    var cantidad = $('#cantidad option:selected').val();

    $("#txtcantidad").val(cantidad);
});

/*$("#colour").change(function () {
    var color = $('#colour option:selected').val();

    if (color == "Blanco") {
        color = "94";
    }

    $("#txtcolor").val(color);

    if (color == 'Color') return;
    if (color == 'Modelo') return;
    if (color == 'Formato') return;
    if (color == 'Opcion') return;

    $.ajax({
        url:    'frontend/selectTalla',
        type:   'get',
        data: {
            'id': color
        },
        success: function (data) {
            $("#divTalla")
                .html(data.talla)
                .trigger('change');
            $("#grabacion").html(data.perso);
            $("#frases").html(data.frase);

            if(data.frase != 'false'){
                $("#personalizado").hide();
            }else{
                $("#frases").hide();
            }
        },
        error: function () {
            alert ("error colour");
        }
    });
});*/

/*$("#divTalla").change(function () {
    var id = $("#divTalla option:selected").val();
    if (id == 'Tetina/Talla') id=' ';
    var tallatetina = id.split(" ");
    var talla = tallatetina[1];
    var tetina = tallatetina[0];

    $("#txtTalla").val(talla);
    $("#txtTetina").val(tetina);

    if (talla == 'sin'){
        $("#personalizado")
            .val('')
            .prop("disabled", true);
    }
    if (talla == 'per'){
        $("#personalizado")
            .prop("disabled", false);
    }

    if (talla == 'Frase') {
        $("#personalizado")
            .val('')
            .hide();
        $("#frases").css('display', 'block');
    }

    if (talla == "Person"){
        $("#personalizado")
            .val('')
            .show()
            .prop("disabled", false);
        //$("#frases").css('display', 'none');
    }

    if (talla == "Principe" || talla == "Princesa"){
        $("#personalizado")
            .val('')
            .prop("disabled", true);
    }else{
        $("#personalizado")
            .val('')
            .prop("disabled", false);
    }
});*/

/*$("#frases").change(function () {
    var frase = $("#frases option:selected").val();

    $("#personalizado").val(frase);
});*/

function ajaxRenderSection(url) {
    var color   = $("#txtcolor").val();
    var talla   = $("#txtTalla").val();
    var tetina  = $("#txtTetina").val();
    var perso   = $("#personalizado").val();
    var produ   = $("#txtproducto").val();
    var canti   = $("#txtcantidad").val();

    if ($("#selectModelo").is(":visible")) {
        var model = $("#txtModelo").val();
    }else{
        var model = 'no';
    }

    if (produ == 38 || produ == 53){
        perso = 'disabled';
    }

    if ($("#personalizados").is(":disabled")) {
        perso = 'disabled';
    }

    /*if ($("#personalizado").is(":disabled")) {

        if (produ ==21){
            if (perso == ''){
                perso = '';
            }else if(perso != ''){
                perso = perso;
            }else{
                perso = 'disabled';
            }
        }

        if (produ ==27){
            if (perso == ''){
                perso = '';
            }else if(perso != ''){
                perso = perso;
            }else{
                perso = 'disabled';
            }
        }

        if (produ == 13 || produ == 8){
            perso = 'disabled';
        }

        //principe, princesa, te quiero mama
        if (produ == 1){
            if (color == 80 || color == 21 || color == 79){
                perso = 'disabled';
            }
        }

        if (produ == 12 || produ == 37 || produ == 38 || produ==39 || produ == 40 || produ == 41 || produ == 51 || produ == 52 || produ == 53 || produ == 54 || produ == 61 ){
            perso = 'disabled';
        }
    }*/


    $.ajax({
        type: 'get',
        url: url,
        data: {color:color, talla:talla, tetina:tetina, perso:perso, produ:produ, canti:canti, model:model},
        success: function (data) {
            //aquí muestra los errores
            showError();
            $('#msg').html(data);
            closeError();
            var txtE = $("#txtError").val();

            if(txtE == 'si'){
                $('div.table-container').load('frontend/pedidos', function() {
                    $('div.table-container').fadeIn();
                });
                readCountLineas();
                $('#producto').val(1).trigger('change');
                $('#cantidad').val(1).trigger('change');
                clean();
            }
        },
        error: function (data) {
            var errors = data.responseJSON;
            if (errors) {
                $.each(errors, function (i) {
                    console.log(errors[i]);
                });
            }
        }
    });
}

function ajaxRenderMensajes(url){
    $.ajax({
        type: 'get',
        url: url,
        success: function (data) {
            $('#noticias').html(data);
        },
        error: function (data) {
            //alert('no');
        }
    });
}

function leido(id) {
    $.ajax({
        type: 'get',
        url: 'mensajes/leido',
        data: {
            'id': id
        },
        success: function (data) {
            //$('#noticias').html(data);
            countMessages();
            ajaxRenderMensajes('mensajes/noti');
        },
        error: function (data) {
            alert('error')
        }
    });
}

$('#send').on('submit', function (e) {
    e.preventDefault(e);
    ajaxRenderSection('frontend/addLinea');
});

function closeError(){
    setTimeout(function() {
        $("#msg").fadeOut(1500);
    },3000);
}

function showError(){
    $("#msg").fadeIn(1500);
}

$('#pegatinas').change(function () {
    var op = $("#opcion option:selected").val();
    var di = $("#dibujo option:selected").val();

    $("#txtTetina").val(op);
    $("#txtTalla").val(di);
});


function readCountLineas() {
    $.ajax({
        url:   'frontend/contlineas',
        type:  'get',
        success:  function (data) {
            $("#orders_tmp").html(data);
            gastosEnvio();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            if(xhr.status==403) {
                $(location).attr("href", "/app-boann/public");
            }
            if (xhr.status == 500){
                $(location).attr("href", "/app-boann/public");
            }
        }
    });
}



function readCountPedidos(){
    $.ajax({
        url: 'frontend/contpedidos',
        type: 'get',
        success: function (data) {
            $("#count_pedidos").html(data);
        },
        error: function (errorThrown) {
            alert(errorThrown);
        }
    });
}

function countMessages(){
    $.ajax({
        url: 'mensajes/count',
        type: 'get',
        success: function (data) {
            $("#count_messages").html(data);
            $("#txtCount").val(data);
            alertMessages();
        },
        error: function (errorThrown) {
            alert(errorThrown);
        }
    });
}

function guardaComercioOnline(){
    var com = $("#txtcomercio").val();
    var id = $("#txtuserId").val();

    $.ajax({
        url: 'frontend/guardaComercio',
        type: 'get',
        data: {
            'comercio': com ,'id' : id
        },
        success: function (data) {

        },
        error: function (errorThrown) {
            alert(errorThrown);
        }
    });
}


function deleteRecord(id){
    var conf = confirm("¿Estás seguro de querer borar la línea de producto?");
    if (conf == true){
        $.ajax({
            url: 'deleteRecord',
            data: {
                'id': id
            },
            success: function (data) {
                $("#msg").html(data);
                $('div.table-container').load('frontend/pedidos', function() {
                    //$('div.table-container').fadeIn();
                });

                readCountLineas();
            },
            error: function (errorThrown) {
                alert(errorThrown);
            }
        });
    }


}

function confirmAddRecord(){
    var comment = $("#comentario").val();
    var codEnvio =  $("#txtCodEnvio").val();

    $.ajax({
        async: false,
        url: 'frontend/addPedidos',
        data: {
            'comentario': comment,
            'codEnvio': codEnvio
        },
        beforeSend: function () {
            $('div.table-container').fadeOut();
            $("#procesar")
                .addClass("btn btn-primary")
                .html('<i class="fa fa-circle-o-notch fa-spin"></i> Procesando...');
        },
        success: function (data) {
            if (data == "vacio"){
                //return "error";
                /*$("#procesar-pedido")
                    .addClass("oaerror danger")
                    .show("slow")
                    .css('margin-bottom','10px')
                    .html('<strong>Error:</strong> No se puede procesar el pedido ya que no hay ningún producto');*/
                $("#procesar").html('Procesar pedido');
                //closeErrorPedido();
            }else{
                //$(".orders_content").html(data);
                //alert ("funciona");
                //$("#msg").html(data);
                $("#procesar").html('Procesar pedido');

                $('div.table-container').load('frontend/pedidos', function() {
                    $('div.table-container').fadeIn();
                });
                $('div.table-order').load('orders', function() {
                    //$('div.table-container').fadeIn();
                });
                /*$("#procesar-pedido")
                    .addClass("oaerror success")
                    .show("slow")
                    .html('<strong>Ok:</strong> Pedido realizado con éxito!<br>');*/

                $("#comentario").val('');
                readCountPedidos();
                readCountLineas();
            }
        },
        error: function (errorThrown) {
            //alert('errorThrown');
        }
    });
}

function addOrder(){
    var lineas = $("#orders_tmp").text();
    swal({
            title: "¿Está segura/o que desea finalizar el pedido?",
            text: "No se podrán hacer aumentos",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sí, aceptar",
            cancelButtonText: "No, cancelar",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm) {
                if (lineas > 0){
                    swal("Procesado", "Su pedido ha sido procesado", "success");
                    confirmAddRecord();
                }else{
                    swal("Error", "No ha añadido ninguna línea de producto", "error");
                }


            } else {
                swal("Cancelado", "Su pedido no ha sido procesado :)", "error");
            }
        });
}

function closeErrorPedido(){
    setTimeout(function() {
        $("#procesar-pedido")
            .fadeOut(1500);
    },3000);
}

function alertMessages(){
    var count = $("#txtCount").val();
    if (count > 0){
        bootbox.confirm({
            closeButton: false,
            title: "Mensajes pendientes",
            message: "Tienes <strong>"+count+" </strong>mensajes pendientes de leer",
            buttons: {
                confirm: {
                    label: '<i class="fa fa-check"></i> Aceptar'
                },
                cancel: {
                    label: 'No'
                }
            },
            callback: function (result) {
                //console.log('This was logged in the callback: ' + result);
                //alert(result);
                if (result == true || result == false){
                    $('.nav-tabs a[href="#' + 'tab_c' + '"]').tab('show');
                }
            }
        });
    }
}







/*---------------------------------------------------*/



/*function readRecords() {
    $.ajax({
        url:   'readRecords',
        type:  'get',
        beforeSend: function () {
            $(".records_content").html("Procesando, espere por favor...");
        },
        success:  function (data) {
            $(".records_content").html(data);
            readCountOrdersTmp();
            readCountPedidos();
        },
        error: function () {
            alert ("error");
        }
    });
}*/



/*function getColor(id){
    $.ajax({
        url:   'colores',
        data: {
            'id': id
        },
        beforeSend: function () {
            //$("#color").hide();
            $('#color')
                .children()
                .remove()
                .end()
                .append('<option value="">Loading...</option>');
        },
        success:  function (data) {
            $("#color").html(data);
        },
        error: function () {
            alert ("error");
        }
    });
}*/

/*function getTalla(id){
    $.ajax({
        url: 'talla',
        data: {
            'id': id
        },
        beforeSend: function () {
            //$("#color").hide();
            $('#talla')
                .children()
                .remove()
                .end()
                .append('<option value="">Loading...</option>');
        },
        success:  function (data) {
            $("#talla").html(data);
        },
        error: function () {
            alert ("error");
        }
    });
}*/

/*function getFrase(){
    $.ajax({
        url: 'frase',
        beforeSend: function () {
            //$("#color").hide();
            $('#selectFrase')
                .children()
                .remove()
                .end()
                .append('<option value="">Loading...</option>');
        },
        success: function (data) {
            $("#selectFrase").html(data);
        },
        error: function (errorThrown) {
            alert(errorThrown);
        }
    });
}*/

/*function getFraseBody(){
    $.ajax({
        url: 'frase_body',
        beforeSend: function () {
            //$("#color").hide();
            $('#selectFrase')
                .children()
                .remove()
                .end()
                .append('<option value="">Loading...</option>');
        },
        success: function (data) {
            $("#selectFrase").html(data);
        },
        error: function (errorThrown) {
            alert(errorThrown);
        }
    });
}*/

/*function getOpcional(){
    $.ajax({
        url: 'opcional',
        beforeSend: function () {
            //$("#color").hide();
            $('#selectOpcional')
                .children()
                .remove()
                .end()
                .append('<option value="">Loading...</option>');
        },
        success: function (data) {
            $("#selectopcional").html(data);
        },
        error: function (errorThrown) {
            alert(errorThrown);
        }
    });
}*/







/*function addRecord() {




    var producto    = $("#producto").val();
    var cantidad    = $("#cantidad").val();
    var color       = $("#color").val();
    var tetina      = $("#txtTetina").val();
    var talla       = $("#txtTalla").val();
    var nombre      = $("#personalizado").val();


    //validate(producto, talla, color, tetina, nombre);
    var resultado = validate(producto, talla, color, nombre);

    if (resultado == "false"){

        $.ajax({
            url: 'addRecord',
            data: {
                'producto': producto,
                'cantidad': cantidad,
                'color': color,
                'tetina': tetina,
                'talla': talla,
                'nombre': nombre
            },
            success: function (data) {
                //$("#actualizaciones").html(data);
                $("#msg")
                    .removeClass("oaerror danger")
                    .addClass("oaerror success")
                    .html("<strong>Ok:</strong> Línea producto añadido correctamente");
                //readRecords();
                clean();
                //$('div.table-container').fadeOut();
                $('div.table-container').load('pedidos', function() {
                    $('div.table-container').fadeIn();
                });
                readCountOrdersTmp();
            }
        });
    }
}*/

/*function validate(producto, talla, color , nombre){
    $("#msg").removeClass("oaerror success");
    var error = "false";

    if (producto == "Selecciona artículo"){
        $("#msg")
            .addClass("oaerror danger")
            .show("slow")
            .html('<strong>Error:</strong> No hay seleccionado ningún producto');
        error = "true";
        closeError();
        return error;
    }

    if (color == "Color"){
        $("#msg")
            .addClass("oaerror danger")
            .show("slow")
            .html('<strong>Error:</strong> Color no seleccionado');
        error = "true";
        closeError();
        return error;
    }

    if ($("#talla").is(":visible") && $("#talla").is(":enabled")) {
        if (talla == "Tetina/talla" || talla == ""  || talla == "Opción" ){
            $("#msg")
                .addClass("oaerror danger")
                .show("slow")
                .html('<strong>Error:</strong> Talla/tetina no seleccionada');
            error = "true";
            closeError();
            return error;
        }
    }

    if ($("#personalizado").is(":enabled")){
        if (!nombre){
            $("#msg")
                .addClass("oaerror danger")
                .show("slow")
                .html('<strong>Error:</strong> El campo personalización/frase está vacío');
            error = "true";
            closeError();
            return error;
        }
    }

    return error;
}*/

/*function closeError(){
    setTimeout(function() {
        $("#msg").fadeOut(1500);
    },3000);
}*/

/*function clean() {
    $("#personalizado")
        .val('')
        .show();

    $("#producto").val('');


    $("#cantidad").val(1);
    $("#color")
        .val('')
        .prop("disabled", false );
    $("#talla")
        .val('')
        .prop("disabled", false);
    $("#txtTalla").val('');
    $("#txtTetina").val('');

    $("#selectFrase")
        .prop("disabled", false)
        .hide();

    getColor("color");
    getTalla("normal");
    //productos();
    $("#producto").selectpicker('render');
}*/


$(document).ready(function () {

    /*$('div.table-container').load('pedido', function() {
        $('div.table-container').fadeIn();
    });*/

    /*$("#selectFrase").hide();
    $("#opcional").hide();


    //productos();
    readRecords();
    readOrders();
    getColor('001');
    getTalla('silicona');
    $("#personalizado").attr('maxlength', '100');*/



    /*$("#productosss").change(function(){

        var id = $('#producto option:selected').val();
        $("#color").prop("disabled", false );
        $("#personalizado")
            .prop("disabled", false)
            .val('')
            .show()
            .attr("placeholder", "Grabación")
            .attr('maxlength', '100');
        $("#selectFrase").hide();
        $("#opcional").hide();
        $("#divTalla").width(200);

        switch (id){
            case '1': //Chupete
                chupetes();
                break;
            case '2': //cadena
                cadena();
                break;
            case '3': //cepillo y peine
                cepilloPeine();
                break;
            case '4': //biberon
                biberon();
                break;
            case '5': //cepillo de dientes
                cepilloDientes();
                break;
            case '6': //portachupetes
                portaChupetes();
                break;
            case '7': //sonajero
                sonajero();
                break;
            case '8': //caja premium
                cajaPremium();
                break;
            case '9': //cadena premium
                cadenaPremium();
                break;
            case '10': //packs premium
                packsPremium();
                break;
            case '11': //conjunto chupete y cadena
                chupeteyCadena();
                break;
            case '12': //babero
                babero();
                break;
            case '13': //babero frase
                baberoFrase();
                break;
            case '14': //quitababas
                quitaBabas();
                break;

            case '15': //bolsamerienda
                bolsaMerienda();
                break;
            case '16': //sujeta chupete tela
                sujetaChupeteTela();
                break;
            case '17': //etiqueta
                etiquetasTextil();
                break;
            case '20': //conjunto gorro
                conjuntoGorro();
                break;
            case '21': //camiseta
                camisetaPremama();
                break;
            case '22': //pegatina
                pegatinas();
                break;
            case '23': //pack cancion
                packCancion();
                break;
            case '24': //chapas
                chapas();
                break;
            case '25': //zapatillas
                zapatillas();
                break
            case '26': //pulsera
                pulsera();
                break;
            case '27': //body
                body();
                break;
            case '28': //bolsa cambio
                bolsa();
                break;
        }

        function chupetes(){ //1
            getColor("001");
            getTalla();
            $("#talla").prop("disabled", false );
            $("#color").prop("disabled", false );
        }

        function cadena(){ //2
            disableTallaTetina();
            getColor("002");
        }

        function disableTallaTetina(){
            $("#talla")
                .prop( "disabled", true )
                .empty()
                .append('<option value="--- ---">--- ---</option>');
        }

        function cepilloPeine(){ //3
            disableTallaTetina();
            getColor("003");
        }

        function biberon(){ //4
            $("#personalizado").attr('maxlength', '20');
            disableTallaTetina();
            getColor("004");
        }

        function cepilloDientes() { //5
            $("#personalizado").attr('maxlength', '15');
            disableTallaTetina();
            getColor("005");
        }

        function portaChupetes() { //6
            disableTallaTetina();
            getColor("006");
        }

        function sonajero() { //7
            disableTallaTetina();
            getColor("007");
        }

        function cajaPremium() { //8
            disableTallaTetina();
            $("#color")
                .prop("disabled", true)
                .empty()
                .append('<option value="64">---</option>');
            $("#personalizado")
                .prop("disabled", true)
                .val("---");

        }

        function cadenaPremium() { //9
            disableTallaTetina();
            getColor("009");
        }

        function packsPremium() { //10
            getTalla();
            getColor("010");
            $("#talla").prop("disabled", false );
            $("#personalizado").attr("placeholder", "nombre");
        }

        function chupeteyCadena(){ //11
            $("#talla").prop("disabled", false );
            $("#color").prop("disabled", false );
            getColor("011");
            getTalla();
        }

        function babero() { //12
            disableTallaTetina();
            $("#talla").prop("disabled", true );
            //$("#color").prop("disabled", true );
            //$("#color").empty();

            //$("#color").append('<option value="64">---</option>');
            getColor("012");
        }

        function baberoFrase(){ //13
            disableTallaTetina();

            $("#personalizado")
                .prop("disabled", true)
                .val("---");
            getColor("013");
        }

        function quitaBabas() { //14
            disableTallaTetina();
            getColor("014");
        }

        function bolsaMerienda() { //15
            disableTallaTetina();
            getColor("015");
        }

        function sujetaChupeteTela() { //16
            //disableTallaTetina();
            $("#talla")
                .empty()
                .prop("disabled", false)
                .append('<option selected="selected">Opción</option>')
                .append('<option value="--- per">Personalizado</option>')
                .append('<option value="--- sin">Sin personalizar</option>');
            getColor("016");
        }

        function etiquetasTextil() { //17
            //disableTallaTetina();

            $("#talla")
                .empty()
                .append('<option selected="selected">Opción</option>')
                .append('<option value="--- Mixto">Mixto</option>')
                .append('<option value="--- Multiuso">Multiuso</option>')
                .append('<option value="--- Textil">Textil</option>');
            getColor("017");
        }

        function conjuntoGorro() { //20
            disableTallaTetina();
            getColor("020");
        }

        function camisetaPremama(){ //21
            $("#selectFrase").show();
            disableTallaTetina();
            getTalla('ropa');
            $("#talla").prop("disabled", false);
            getColor("021");
            $("#personalizado").hide();
            getFrase();
        }

        function pegatinas() { //22
            disableTallaTetina();
            getColor("022");
            $("#talla").prop("disabled", false);
            getTalla("pegatina");
            $("#opcional").show();
            $("#divTalla").width(90);
            getOpcional();
        }

        function packCancion(){ //23
            disableTallaTetina();
            getColor("017");
            $("#talla").prop("disabled", false);
            getTalla();
        }

        function chapas() { //24
            disableTallaTetina();
            getColor("017");
        }

        function zapatillas() { //25
            getColor("017");
            $("#talla")
                .empty()
                .append('<option selected="selected">Talla</option>')
                .append('<option value="--- 20">20</option>')
                .append('<option value="--- 21">21</option>');
        }

        function pulsera(){ //26
            disableTallaTetina();
            getColor("017");
        }

        function body(){ //27
            disableTallaTetina();
            getTalla('body');
            $("#talla").prop("disabled", false);
            $("#color")
                .empty()
                .append('<option value="3">Blanco</option>')
            $("#personalizado").show();
            getFraseBody();
        }

        function bolsa(){ //28
            disableTallaTetina();
            getColor("017");
        }

    });
    //activaTab('tab_a');

    $("#selectFrase").change(function(){
        var idFrase = $('#selectFrase option:selected').val();
        if (idFrase != "Frase"){
            $("#personalizado").val(idFrase);
        }
    });

    $("#color").change(function(){
        var id = $("#color option:selected").val();
        var pr = $("#producto option:selected").val();

        if (pr == 1){
            getTalla();
        }

        // 79 - princiesa
        // 80 - te quiero mama
        // 21 - principe
        if (id == 79 || id == 21 || id == 80 || id == 81){
            $("#personalizado").prop("disabled", true);
            $("#txtTalla").val('');
            $("#txtTetina").val('');

            if (id == 81){
                getTalla("silicona_pequeña");
            }
            else {
                getTalla("silicona");
            }
        }else if(pr == 17 || pr == 16){
            $("#personalizado").prop("disabled", false);
        }else if (id == 25 || id == 26) {
            $("#personalizado").prop("disabled", true);
        }else{
            $("#personalizado").prop("disabled", false);
        }

        if (pr == 13){
            $("#personalizado").prop("disabled", true);
        }

    });

    $("#talla").change(function () {
        var id = $("#talla option:selected").val();
        var tallatetina = id.split(" ");
        var talla = tallatetina[1];
        var tetina = tallatetina[0];

        $("#txtTalla").val(talla);
        $("#txtTetina").val(tetina);

        if (talla == "Perso"){
            $("#personalizado").prop("disabled", false);
            $("#personalizado")
                .val('')
                .show();
            $("#selectFrase").hide();
        }else if (talla == "Frase"){
            $("#selectFrase").show();
            $("#personalizado")
                .val('')
                .hide();
        }

        if (talla == "sin"){
            $("#personalizado").prop("disabled", true);
            $("#txtTalla").val("---");
        }else{
            $("#personalizado").prop("disabled", false);
        }

        var id = $("#color option:selected").val();

        if (id == 79 || id == 21 || id == 80 || id == 81){
            $("#personalizado")
                .val('')
                .prop("disabled", true);
        }
    });

    $("#selectopcional").change(function () {
        var id = $("#opcional option:selected").val();
        //var opcional = id.split(" ");
        $("#txtTalla").val(id);
    });*/

});
