<?php

namespace App\Modules\Mapa\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'mapa');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'mapa');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'mapa');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
