<?php

namespace App\Modules\Catalogo\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'catalogo');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'catalogo');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'catalogo');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
