<?php

namespace App\Modules\Catalogo\Http\Controllers;

use App\Modules\Catalogo\Models\Catalogo;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CatalogoController extends Controller
{
    /**
     * @param $idCliente
     * @return mixed
     */
    public static function compruebaCatalogoCliente($idCliente){
        $pedidos = self::compruebaPedido($idCliente);

        if ($pedidos == 0){
            return -1;
        }else{
            if ($pedidos > 0){
                $comprueba = Catalogo::where('idCliente', '=', $idCliente)->count();

                return $comprueba;
            }else{
                return 0;
            }
        }
    }

    /**
     * @param $idCliente
     */
    public static function guardaCatalogoCliente($idCliente){

        $catalogo = new Catalogo;

        $catalogo->idCliente = $idCliente;
        $catalogo->fecha = Carbon::now();

        $catalogo->save();
    }

    /**
     * @return mixed
     */
    public static function compruebaPedido($idCliente){
        $today =  Carbon::today();
        $twoMonth = Carbon::today()->subMonths(2);

        $pedidos = DB::table('pedidos')
            ->where('id_cliente', '=' , $idCliente)
            ->whereBetween('fecha', [$twoMonth, $today])
            ->get();

        return count($pedidos);
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getCatalogoCliente($id){
        $comprueba = Catalogo::where('idCliente', '=', $id)->count();

        return $comprueba;
    }
}
