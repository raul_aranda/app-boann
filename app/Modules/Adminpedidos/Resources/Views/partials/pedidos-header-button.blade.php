<div class="pull-right mb-10 hidden-sm hidden-xs">
    {{ link_to_route('pedidos.listado', 'Todos los Pedidos', [], ['class' => 'btn btn-primary btn-xs']) }}
    {{ Form::button('Excel Taller', ['class' => 'btn btn-success btn-xs', 'id'=> 'btntaller']) }}

    {{ Form::button('Cabecera Alb', ['class' => 'btn btn-warning btn-xs', 'id'=>'btnalbaranes']) }}
    {{ Form::button('Detalles Alb', ['class' => 'btn btn-warning btn-xs', 'id'=>'btndetalles']) }}
    {{ link_to_route('users.index', 'Usuarios sin Pedidos', [], ['class' => 'btn btn-danger btn-xs']) }}

    {{ Form::button('CSV Textil', ['class' => 'btn btn-success btn-xs', 'id'=> 'btnTextil']) }}
    {{ Form::button('CSV Multiusos', ['class' => 'btn btn-success btn-xs', 'id'=> 'btnMultiusos']) }}
    {{--{{ Form::button('CSV Pulseras', ['class' => 'btn btn-success btn-xs', 'id'=> 'btncsv']) }}
    {{ Form::button('CSV Llavero', ['class' => 'btn btn-success btn-xs', 'id'=> 'btncsv']) }--}}
</div><!--pull right-->

<div class="clearfix"></div>