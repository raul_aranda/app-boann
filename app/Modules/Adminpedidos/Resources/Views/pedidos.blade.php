@extends('admin.layouts.back')

@section('after-styles')
    {{ Html::style("/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css") }}
    <style>
        .table-condensed > tbody > tr > td, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > td, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > thead > tr > th {
            padding: 1px 1px 0px 1px;
        }

        .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
            padding: 5px 0px 0px 0px;
            line-height: 1.42857143;
            vertical-align: middle;
            border-top: 1px solid #ddd;
        }
        .fa{ font-size: 16px;
        }

        .fa-search{
            color: #26c6da;
        }
        .mrw-send{
            color: #f44336;
        }
        .mrw-send-ok{
            color: #66bb6a;
        }
        .fa-envelope-o{
            color: #9c27b0;
        }
        .fa-check{
            color: #ff9800;
        }
    </style>
@stop

@section('page-header')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../index.html">Home</a>
        </li>
        <li class="breadcrumb-item active">Basic UI</li>
    </ol>
    <h1 class="page-title">
        Listado de Pedidos
    </h1>
    <p class="page-description">Pedidos Realizados</p>
    <div class="page-header-actions">
        @include('adminpedidos::partials.pedidos-header-button')
    </div>
@endsection

@section('content')
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Listado de Pedidos</h3>
        </div><!-- /.box-header -->
        <div class="panel-toolbar">
            <div class="filtros">
                {!! Form::open(['route' => 'filtro.pedidos', 'method' => 'post']) !!}
                <div class="container">
                    <div class='col-lg-3'>
                        <div class="form-group">
                            <div class='input-group date' id='datetimepicker6'>
                                {{ Form::text('date1', $fecha1, ['class' => 'form-control',  'id' => 'date1' ]) }}
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                            </div>
                        </div>
                    </div>
                    <div class='col-lg-3'>
                        <div class="form-group">
                            <div class='input-group date' id='datetimepicker7'>
                                {{ Form::text('date2', $fecha2, ['class' => 'form-control',  'id' => 'date2' ]) }}
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    {{ Form::submit('Filtro', ['class' => 'btn btn-success']) }}
                    <button type="button" class="btn btn-primary" id="validarSeleccionados">Validar seleccionados</button>

                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="panel-body">
            {!! Form::open(['url' => '', 'method' => 'post', 'id' => 'listado']) !!}
            <div class="table-responsive">
                @if($data)
                    <table id="users-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            @if($filtro == 'true')
                                <th><input type="checkbox" id="checkMain" checked></th>
                            @else
                                <th><input type="checkbox" id="checkMain"></th>
                            @endif
                            <th style="font-weight: 600">ID</th>
                            <th style="font-weight: 600"><a href="{{ route('pedidos.orden', ['filtro' => 'fecha']) }}">Fecha</a> </th>
                            <th style="font-weight: 600">Estado</th>
                            <th style="font-weight: 600"><a href="{{ route('pedidos.orden', ['filtro' => 'cliente']) }}"> Código Cliente</a></th>
                            <th style="font-weight: 600"><a href="{{ route('pedidos.orden', ['filtro' => 'nombre']) }}">Nombre</a></th>
                            <th style="text-align: center; font-weight: 600">Num Artículos</th>
                            <th style="font-weight: 600">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $row)
                            @php ($fecha = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->fecha))
                            @php ($fecha = $fecha->format('d-m-Y H:i:s'))

                            <tr>
                                <td>
                                    @if($filtro == 'true')
                                        <input type="checkbox" name="pedido[]" class="checkAll" value="{{ $row->id }}" checked="checked">
                                        {{-- Form::checkbox('pedido[]', $row->id, 'yes', ['class'=>'checkAll']) --}}
                                    @else
                                        @if($row->validate == 1)
                                            <input type="checkbox" name="pedido[]" class="checkNone" value="{{ $row->id }}">
                                        @else
                                            <input type="checkbox" name="pedido[]" class="checkAll" value="{{ $row->id }}" >
                                        @endif
                                        {{-- Form::checkbox('pedido[]', $row->id, null, ['class'=>'checkAll'], 'disabled:true') --}}
                                    @endif
                                </td>
                                <td>{{ $row->id }}</td>
                                <td>{{ $fecha }}</td>
                                <td>
                                    @if($row->validate == 1)
                                        <button class="btn btn-pure btn-success fa fa-check waves-effect waves-circle" type="button"></button>
                                    @else
                                        <button class="btn btn-pure btn-warning icon md-star waves-effect waves-circle waves-classic" type="button"></button>
                                    @endif
                                </td>
                                <td>{{ $row->id_cliente }}</td>
                                <td>{{ $row->name }}</td>
                                <td style="text-align: center">{{ $row->num_art }}</td>
                                <td>
                                    <a href="{{ route('pedidos.pdf', ['id' => $row->id])  }}" class=""><i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="Ver"></i></a>
                                    <a href="{{ route('pedidos.mail', ['id' => $row->id])  }}" class=""><i class="fa fa-envelope" aria-hidden="true"></i></a>
                                    @if($row->validate <> 1)
                                        <a href="{{ route('pedidos.validate', ['id' => $row->id])  }}" class=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                    @endif
                                    @if($row->idDropshipping != null)
                                        <a href="{{ route('pedidos.mrw', ['id' => $row->idDropshipping])  }}" class="">
                                            <i class="fa fa-send {{ $class = $row->printMRW == 0 ? 'mrw-send' : 'mrw-send-ok' }}"  data-toggle="tooltip" data-placement="top" title="MRW"></i>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </table>
                @endif
            </div>
            {!! Form::close() !!}
            @if ($filtro == 'false')
                <div class="container" align="center">{{ $data->links() }}</div>
            @endif

        </div>
    </div>
@endsection
@section('after-scripts')
    {{ Html::script("/bower_components/moment/min/moment-with-locales.min.js") }}
    {{ Html::script("/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js") }}
    <script type="text/javascript">
        $(function () {
            //var dateNow = new Date();

            //alert(dateNow.setFullYear(d.getFullYear()-1));

            $('#datetimepicker6').datetimepicker({
                locale: 'es',
                format: 'DD-MM-YYYY H:mm:ss'
                //defaultDate: dateNow
            });

            $('#datetimepicker7').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                locale: 'es',
                format: 'DD-MM-YYYY H:mm:ss'
                //defaultDate: dateNow.setDate(dateNow.getDate()-122)
            });

            $('#btntaller').click (function () {
                var form = $('#listado');
                var baseUrl = "{{ URL::to('/excel-taller') }}";

                alert(baseUrl);

                form.attr('action', baseUrl);
                form.submit();
            });

            $('#btnTextil').click(function () {
                var form = $('#listado');
                var baseUrl = "{{ URL::to('/textil') }}";

                form.attr('action', baseUrl);
                form.submit();
            });

            $('#btnMultiusos').click(function () {
                var form = $('#listado');
                var baseUrl = "{{ URL::to('/multiusos') }}";

                form.attr('action', baseUrl);
                form.submit();
            });
            $('#btnalbaranes').click (function () {
                var form = $('#listado');
                var baseUrl = "{{ URL::to('/excel-cabecera') }}";

                form.attr('action', baseUrl);
                form.submit();
            });

            $('#btndetalles').click (function () {
                var form = $('#listado');
                var baseUrl = "{{ URL::to('/excel-detalle') }}";

                form.attr('action', baseUrl);
                form.submit();
            });

            $('#checkMain').change( function (){
                $(".checkAll").prop('checked', $(this).prop("checked"));
            });

            $('#validarSeleccionados').click(function (){
                var form = $('#listado');
                var baseUrl = "{{ URL::to('adminpedidos/checkall') }}";

                form.attr('action', baseUrl);
                form.submit();

                /*var data = { 'user_ids[]' : []};
                $(":checked").each(function() {
                    data['user_ids[]'].push($(this).val());
                });

                console.log(data);*/
            });
        });

    </script>
@stop