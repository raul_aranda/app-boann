@extends('admin.layouts.back')

@section('after-styles')
@stop

@section('page-header')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../index.html">Home</a>
        </li>
        <li class="breadcrumb-item active">Basic UI</li>
    </ol>
    <h1 class="page-title">
        Detalle del pedido
    </h1>
    <div class="page-header-actions">
        @include('adminpedidos::partials.pedidos-header-button')
    </div>
@endsection

@foreach($pedido as $row)

@endforeach
@php
    //$valueNIF = str_limit(Auth::user()->barras, 9);
    $valueNIF = substr(Auth::user()->barras, 3, 9);
    /*$numPedido  = substr($row->fecha, 0, 4);
    $numPedido .= "-0" . $row->num_art;
    $numPedido .= "0" . $row->id;
    $numPedido .= "-" . substr($row->fecha,9, 2);*/

    $numPedido  = substr($row->fecha, 0, 4);
    $numPedido .= "-". $row->id;
@endphp
@section('content')
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Listado de Pedidos</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="form-group">
                    <div class="col-md-2">
                        <h3>Datos del Cliente</h3>
                        {{ 'Codigo Cliente: '.$row->id_cliente }}<br>
                        {{ 'Fecha: '.$row->fecha }}
                    </div>
                    <div class="col-md-2">
                        <h3>Hoja de Pedido</h3>
                        <h3 style="display: inline-block; margin-top: -10px;">
                            # {{ str_pad($numPedido,12,"0", STR_PAD_LEFT) }}</h3>
                    </div>
                </div><!--form control-->
            </div>
            <br>
            <div class="table-responsive">
                @if($data)
                    <table id="users-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th style="font-weight: 600">ID</th>
                            <th style="font-weight: 600">Artículo</th>
                            <th style="font-weight: 600">Uds</th>
                            <th style="font-weight: 600">Color</th>
                            <th style="font-weight: 600">Tetina</th>
                            <th style="font-weight: 600">Talla</th>
                            <th style="font-weight: 600">Nombre</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $count = 0
                        @endphp
                        @foreach($data as $line)
                            @php
                                $count = $count + 1;

                                if ($line->modelo == '0') $modelo = '';
                                else $modelo = $line->modelo;
                            @endphp
                            <tr>
                                <td>{{ $count }}</td>
                                <td>{{ $line->descripcion . ' '. $modelo }}</td>
                                <td style="color: {{ ($line->cantidad > 1) ? 'red' : '' }}">{{ $line->cantidad }}</td>
                                <td>{{ $line->codigo_color . ' ' . $line->descripcion_color }}</td>
                                <td>{{ $line->tetina }}</td>
                                <td>{{ $line->talla }}</td>
                                <td>{{ $line->nombre }}</td>
                            </tr>
                        @endforeach
                    </table>
                @endif
            </div>
        </div>
        <div class="panel-footer">
            Comentarios:
            @if($row->comentarios == '')
                Ningún comentario
            @else
                {{ $row->comentarios }}
            @endif
            <div class="pull-right"><strong> {{ 'Total: '.count($data) . " Líneas de producto" }}</strong></div>
        </div>
    </div>
@endsection

