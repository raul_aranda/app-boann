<?php

namespace App\Modules\Adminpedidos\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Mail;

class PdfPedidosController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request){

        $get = DB::table('pedidosoks')
            ->join('productos', 'productos.id', '=', 'pedidosoks.id_producto')
            ->leftjoin('color', 'pedidosoks.id_color', '=', 'color.id_color')
            ->where('id_pedidook', '=', $request->id)
            ->get();

        $pedido = DB::table('pedidos')
            ->where('id', '=', $request->id)
            ->get();

        return view('adminpedidos::detallePedido', ['data' => $get, 'pedido' => $pedido]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sendMail(Request $request){
        $get = DB::table('pedidosoks')
            ->join('productos', 'productos.id', '=', 'pedidosoks.id_producto')
            ->leftjoin('color', 'pedidosoks.id_color', '=', 'color.id_color')
            ->where('id_pedidook', '=', $request->id)
            ->get();

        $pedido = DB::table('pedidos')
            ->where('id', '=', $request->id)
            ->get();

        foreach ($pedido as $row){
            $codigo = $row->id_cliente;
        }

        $clientes = DB::table('users')
            ->where('codigo_cliente', $codigo)
            ->get();

        foreach ($clientes as $cliente){
            $nombre = $cliente->name;
        }

        // Dropshipping
        $clienteDropShipping = DB::table('dropshipping')
            ->select('*')
            ->leftjoin('clienteDrop', 'clienteDrop.id', '=', 'dropshipping.idCliente')
            ->where('dropshipping.idPedido', '=', $request->id)
            ->get();

        $pdf = PDF::loadView('emails.pdf', ['data' => $get, 'pedido' => $pedido, 'dropShipping' => $clienteDropShipping]);

        Mail::send('emails.mail', ['html' => 'view', 'data' => $get, 'pedido' => $pedido, 'dropShipping' => $clienteDropShipping], function ($message)
        use($pdf, $codigo, $nombre)
        {
            $message->from('app@chupetesboann.com', 'App Boann');
            $message->to('pedidos@chupetesboann.com')->subject('Pedido '.$codigo. ' '.$nombre);
            $message->attachData($pdf->output(), "pedido.pdf");
        });

        return view('adminpedidos::detallePedido', ['data' => $get, 'pedido' => $pedido]);
    }

}
