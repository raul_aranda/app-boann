<?php

namespace App\Modules\Adminpedidos\Http\Controllers;

use App\Pedidos;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class PedidosBackendController extends Controller
{
    /**
     * Listado de todos los pedidos con paginación
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $fromDate = Carbon::now()->toDateTimeString();
        $toDate = Carbon::now()->subMonths(12)->toDateTimeString();

        /*$dt1 = $date->format('d-m-Y H:i:s');
        $dt2 = $anterior->format('d-m-Y H:i:s');*/

        //$dt1 = $date->format('Y-m-d H:i:s');
        //$dt2 = $anterior->format('Y-m-d H:i:s');



        $pedidos = DB::table('pedidos')
            ->select('pedidos.*', 'users.name', 'dropshipping.*')
            ->leftJoin('users', 'pedidos.id_cliente', '=', 'users.codigo_cliente')
            ->leftJoin('dropshipping', 'pedidos.id' , '=', 'dropshipping.idPedido')
            ->whereBetween('fecha',[$toDate, $fromDate])
            ->orderBy('id', 'desc')
            ->paginate(20);

        Session::forget('fecha1');
        Session::forget('fecha2');

        return view('adminpedidos::pedidos', ['data' => $pedidos, 'filtro' => 'false',
            'fecha1'=>'', 'fecha2'=>'']);
    }

    /**
     * Activación filtro por fechas
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function filtro(Request $request){
        $dt1 = Carbon::createFromFormat('d-m-Y H:i:s', $request->date1);
        $dt2 = Carbon::createFromFormat('d-m-Y H:i:s', $request->date2);

        Session::put('fecha1', $request->date1);
        Session::put('fecha2', $request->date2);

        $pedidos = DB::table('pedidos')
            ->select('pedidos.*', 'users.name', 'dropshipping.*')
            ->leftJoin('users', 'pedidos.id_cliente', '=', 'users.codigo_cliente')
            ->leftJoin('dropshipping', 'pedidos.id' , '=', 'dropshipping.idPedido')
            ->whereBetween('fecha',[$dt1, $dt2])
            ->orderBy('id', 'desc')
            ->get();

        return view('adminpedidos::pedidos', ['data' => $pedidos, 'filtro' => 'true',
            'fecha1' => $request->date1, 'fecha2'=>$request->date2]);

    }

    /**
     * Ordenación por código cliente
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function orden(Request $request){
        if (isset($request->filtro)) {
            if ($request->filtro == 'cliente'){
                $pedidos = $this->orderBy('users.codigo_cliente');
            }else if($request->filtro == 'nombre') {
                $pedidos = $this->orderBy('users.name');
            }else{
                $pedidos = $this->orderBy('pedidos.fecha');
            }

        }


        /*if (Session::has('fecha1')){
            $dt1 = Carbon::createFromFormat('d-m-Y H:i:s', Session::get('fecha1'));
            $dt2 = Carbon::createFromFormat('d-m-Y H:i:s', Session::get('fecha2'));

            $pedidos = DB::table('pedidos')
                ->leftJoin('users', 'pedidos.id_cliente', '=', 'users.codigo_cliente')
                ->select('pedidos.*', 'users.name')
                ->whereBetween('fecha',[$dt1, $dt2])
                ->orderBy('users.codigo_cliente', 'desc')
                ->get();
        }else{
            $pedidos = DB::table('pedidos')
                ->leftJoin('users', 'pedidos.id_cliente', '=', 'users.codigo_cliente')
                ->select('pedidos.*', 'users.name')
                ->orderBy('users.codigo_cliente', 'desc')
                ->get();
        }*/
        if (Session::has('fecha1')) {
            $dt1 = Carbon::createFromFormat('d-m-Y H:i:s', Session::get('fecha1'));
            $dt2 = Carbon::createFromFormat('d-m-Y H:i:s', Session::get('fecha2'));
        }else{
            $dt1 = '';
            $dt2 = '';
        }

        return view('adminpedidos::pedidos', ['data' => $pedidos, 'filtro' => 'true',
            'fecha1' => $dt1, 'fecha2' => $dt2]);

    }

    /**
     * @param $filtro
     * @return mixed
     */
    public function orderBy($filtro){
        if (Session::has('fecha1')){
            $dt1 = Carbon::createFromFormat('d-m-Y H:i:s', Session::get('fecha1'));
            $dt2 = Carbon::createFromFormat('d-m-Y H:i:s', Session::get('fecha2'));

            $pedidos = DB::table('pedidos')
                ->select('pedidos.*', 'users.name', 'dropshipping.*')
                ->leftJoin('users', 'pedidos.id_cliente', '=', 'users.codigo_cliente')
                ->leftJoin('dropshipping', 'pedidos.id' , '=', 'dropshipping.idPedido')
                ->whereBetween('fecha',[$dt1, $dt2])
                ->orderBy($filtro, 'desc')
                ->get();
        }else{
            $pedidos = DB::table('pedidos')
                ->select('pedidos.*', 'users.name', 'dropshipping.*')
                ->leftJoin('users', 'pedidos.id_cliente', '=', 'users.codigo_cliente')
                ->leftJoin('dropshipping', 'pedidos.id' , '=', 'dropshipping.idPedido')
                ->orderBy($filtro, 'desc')
                ->paginate(10);
        }

        return $pedidos;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function validatePedido(Request $request){
        $pedido = Pedidos::find($request->id);

        $pedido->validate = 1;
        $pedido->save();

        return redirect()->route('pedidos.listado')->withFlashSuccess('Pedido Validado');
    }

    public function countValidate(){

    }

    /**
     * Procedimiento para validar todos los pedidos marcados en los checkboxes
     *
     * @param Request $request
     * @return mixed
     */
    public function validateMultiPedido(Request $request){
        $input = $request->all();

        $input['pedido'] = $request->input('pedido');

        if (!$input['pedido']){
            return redirect()->route('pedidos.listado')->withFlashInfo('No se ha seleccionado ningun pedido');
        }

        for ($i = 0; $i < count($input['pedido']); $i++){
            $pedido = Pedidos::find($input['pedido'][$i]);

            $pedido->validate = 1;
            $pedido->save();
        }

        return redirect()->route('pedidos.listado')->withFlashSuccess('Todos los pedidos seleccionados han sido validados');
    }
}
