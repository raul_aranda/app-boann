<?php

namespace App\Modules\Adminpedidos\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class PedidosTallerController extends Controller
{
    public function index(Request $request){
        $miId = $request->input('pedido');

        $dataTextil[]       = array();
        $dataMultiuso[]     = array();
        $dataCamiseta[]     = array();
        $dataMixto[]        = array();
        $dataPulsera[]      = array();
        $dataZapatilla[]    = array();
        $dataChapas[]       = array();
        $dataPegatina[]     = array();
        $dataNavidad[]      = array();

        for ($i = 0; $i < count($miId); $i++){
            $textil = $this->consultaEtiquetas($miId[$i], 'Textil');
            $multiusos = $this->consultaEtiquetas($miId[$i], 'Multiuso');
            $mixtos = $this->consultaEtiquetas($miId[$i], 'Mixto');
            $pulseras = $this->consultaPulsera($miId[$i]);
            $zapatillas = $this->consultaZapatillas($miId[$i]);
            $chapas = $this->consultaChapas($miId[$i]);
            $pegatinas = $this->consultaPegatinas($miId[$i]);
            $camisetas = $this->consultaCamisetas($miId[$i]);
            $navidades = $this->consultaNavidad($miId[$i]);

            // Textil
            foreach ($textil as $pTextil){
                //if ($pTextil->id_color);
                $dataTextil[] = array(
                    'textil' => 'TEXTIL',
                    'niño' => $color = ($pTextil->id_color == 56 || $pTextil->id_color == 100)? 'ROSA': 'AZUL',
                    'opcion' => '',
                    'Nombre' => $pTextil->nombre,
                    'Id' => $pTextil->id_cliente,
                    'Pedido' => $pTextil->id_pedidook,
                );
            }

            //Multiuso
            foreach ($multiusos as $multiuso){
                //if ($pTextil->id_color);
                $dataMultiuso[] = array(
                    'textil' => 'MULTIUSOS',
                    'niño' => $color = ($multiuso->id_color == 56 || $multiuso->id_color == 100)? 'ROSA': 'AZUL',
                    'opcion' => '',
                    'Nombre' => $multiuso->nombre,
                    'Id' => $multiuso->id_cliente,
                    'Pedido' => $multiuso->id_pedidook,
                );
            }

            // Mixto
            foreach ($mixtos as $mixto){
                //if ($pTextil->id_color);
                $dataMixto[] = array(
                    'textil' => 'MIXTO',
                    'niño' => $color = ($mixto->id_color == 56 || $mixto->id_color == 100)? 'ROSA': 'AZUL',
                    'opcion' => '',
                    'Nombre' => $mixto->nombre,
                    'Id' => $mixto->id_cliente,
                    'Pedido' => $mixto->id_pedidook,
                );
            }

            // Pegatinas
            foreach ($pegatinas as $pegatina){
                //if ($pTextil->id_color);
                $colorPegatina = '';
                if ($pegatina->id_color == 61){
                    $colorPegatina = 'ROSA';
                }elseif ($pegatina->id_color == 62){
                    $colorPegatina = 'AZUL';
                }elseif ($colorPegatina->id_color == 63){
                    $colorPegatina = 'ROJO';
                }
                $dataPegatina[] = array(
                    'textil' => 'BB A BORDO',
                    'niño' => $color = $colorPegatina,
                    'opcion' => '',
                    'Nombre' => $pegatina->nombre,
                    'Id' => $pegatina->id_cliente,
                    'Pedido' => $pegatina->id_pedidook,
                );
            }

            // Zapatillas
            foreach ($zapatillas as $zapatilla) {
                //if ($pTextil->id_color);
                $dataZapatilla[] = array(
                    'textil' => 'ZAPAS',
                    'niño' => $color = ($zapatilla->id_color == 56 || $zapatilla->id_color == 96)? 'ROSA': 'AZUL',
                    'opcion' => '',
                    'Nombre' => $zapatilla->nombre,
                    'Id' => $zapatilla->id_cliente,
                    'Pedido' => $zapatilla->id_pedidook,
                );
            }

            // Chapas
            foreach ($chapas as $chapa) {
                //if ($pTextil->id_color);
                $dataChapas[] = array(
                    'textil' => 'CHAPAS',
                    'niño' => $color = ($chapa->id_color == 56)? 'ROSA': 'AZUL',
                    'opcion' => '',
                    'Nombre' => $chapa->nombre,
                    'Id' => $chapa->id_cliente,
                    'Pedido' => $chapa->id_pedidook,
                );
            }

            // Pulseras
            foreach ($pulseras as $pulsera) {
                //if ($pTextil->id_color);
                $dataPulsera[] = array(
                    'textil' => 'PULSERA',
                    'niño' => $color = ($pulsera->id_color == 56)? 'ROSA': 'AZUL',
                    'opcion' => '',
                    'Nombre' => $pulsera->nombre,
                    'Id' => $pulsera->id_cliente,
                    'Pedido' => $pulsera->id_pedidook,
                );
            }

            // Camisetas premamá
            foreach ($camisetas as $camiseta) {
                //if ($pTextil->id_color);
                $dataCamiseta[] = array(
                    'textil' => 'CAMISETA',
                    'niño' => $color = ($camiseta->id_color == 56)? 'BLANCA': 'NEGRA',
                    'opcion' => '',
                    'Nombre' => $camiseta->nombre,
                    'Id' => $camiseta->id_cliente,
                    'Pedido' => $camiseta->id_pedidook,
                );
            }

            // Campaña Navidad
            foreach ($navidades as $navidad){
                $dataNavidad[] = array(
                    'textil' => 'NAVIDAD',
                    'niño' =>'niño',
                    'opcion' => '',
                    'Nombre' => '',
                    'Id' => $navidad->id_cliente,
                    'Pedido' => $navidad->id_pedidook,
                );
            }

            dd($dataNavidad);


        }
        // Array linea en blanco
        $dataBlanco[] = array(
            'textil' => '',
            'niño' => '',
            'opcion' => '',
            'Nombre' => '',
            'Id' => '',
        );

        /*Excel::create('Taller', function ($excel)
        use($dataTextil, $dataMultiuso, $dataCamiseta, $dataMixto, $dataBlanco, $dataPulsera,
            $dataZapatilla, $dataChapas, $dataPegatina, $dataNavidad){
            // Título
            $excel->setTitle('Pedidos Taller');

            $excel->setDescription('Ejemplo excel laravel');

            $excel->sheet('Nombre', function ($sheet)
            use($dataTextil, $dataMultiuso, $dataCamiseta, $dataMixto, $dataBlanco, $dataPulsera,
                $dataZapatilla, $dataChapas, $dataPegatina, $dataNavidad){
                $sheet->cell('B2', function ($cell){
                    $cell->setValue('Pedido');
                    $cell->setFont(array(
                        'family' => 'Calibri',
                        'size'  => '16',
                        'bold'  => true
                    ));
                });
                //$celda = count($dataTextil);
                $sheet->fromArray($dataTextil, null, 'B4', false, false);

                $sheet->fromArray($dataMultiuso, null, 'B4', false, false);
                $sheet->fromArray($dataMixto, null, 'B4', false, false);

                $sheet->fromArray($dataPegatina, null, 'B4', false, false);

                $sheet->fromArray($dataZapatilla, null, 'B4', false, false);

                $sheet->fromArray($dataChapas, null, 'B4', false, false);

                $sheet->fromArray($dataPulsera, null, 'B4', false, false);

                $sheet->fromArray($dataCamiseta, null, 'B4', false, false);

                $sheet->fromArray($dataNavidad, null, 'B4', false, false);
            });
        })->download('xlsx');*/

    }

    /**
     * Consulta pedidos etiquetas
     *
     * @param $id
     * @param $etiqueta
     * @return mixed
     */
    public function consultaEtiquetas($id, $etiqueta){
        /*$pedidos = DB::table('pedidos')
            ->leftJoin('users', 'pedidos.id_cliente', '=', 'users.codigo_cliente')
            ->leftJoin('pedidosoks', 'pedidosoks.id_pedidook', '=', 'pedidos.id')
            ->leftJoin('productos', 'productos.id', '=', 'pedidosoks.id_producto')
            ->select('pedidos.*', 'users.name', 'pedidosoks.id_producto', 'productos.descripcion', 'pedidosoks.nombre',
                'pedidosoks.id_color')
            ->where('pedidos.id', '=', $id)
            ->where('pedidosoks.id_producto', '=', 17)
            //->where('pedidosoks.talla', '=', $etiqueta)
            ->get();    */
        $pedidos = DB::table('pedidosoks')
            ->select('pedidosoks.*')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 17)
            ->where('pedidosoks.talla', '=', $etiqueta)
            ->get();

        return $pedidos;
    }

    /**
     * Consulta pedidos pegatinas bebé a bordo
     *
     * @param $id
     * @return mixed
     */
    public function consultaPegatinas($id){
        $pegatina = DB::table('pedidosoks')
            ->select('pedidosoks.*')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 22)
            ->orderBy('pedidosoks.id_color', 'DESC')
            ->get();

        return $pegatina;
    }

    /**
     * Consulta pedidos zapatillas
     *
     * @param $id
     * @return mixed
     */
    public function consultaZapatillas($id){
        $zapatilla = DB::table('pedidosoks')
            ->select('pedidosoks.*')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 25)
            ->orderBy('pedidosoks.id_color', 'DESC')
            ->get();

        return $zapatilla;
    }

    /**
     * Consulta pedidos chapas
     * @param $id
     * @return mixed
     */
    public function consultaChapas($id){
        $chapas = DB::table('pedidosoks')
            ->select('pedidosoks.*')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 24)
            ->get();

        return $chapas;
    }

    /**
     * Consulta pedidos pulseras identificativas
     *
     * @param $id
     * @return mixed
     */
    public function consultaPulsera($id){
        $pulsera = DB::table('pedidosoks')
            ->select('pedidosoks.*')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 26)
            ->get();

        return $pulsera;
    }

    /**
     * Consulta pedidos camisetas
     *
     * @param $id
     * @return mixed
     */
    public function consultaCamisetas($id){
        $camiseta = DB::table('pedidosoks')
            ->select('pedidosoks.*')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 21)
            ->get();

        return $camiseta;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function consultaNavidad($id){
        $navidad = DB::table('pedidosoks')
            ->select('pedidosoks.*')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 0)
            ->get();

        return $navidad;
    }

    public function csv(){
        dd('si');
    }

}
