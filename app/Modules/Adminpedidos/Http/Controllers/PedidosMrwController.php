<?php

namespace App\Modules\Adminpedidos\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use SoapClient;

class PedidosMrwController extends Controller
{
    // Datos de acceso a MRW
    private $FranquiciaMRW = '02707';
    private $AbonadoMRW = '002316';
    private $DepartamentoMRW = '';
    private $usuarioMRW = '02707PSBOANN';
    private $PasswordMRW = 'M46agCNr!QYwuoueeD';
    private $wsdl_url;

    /**
     * PedidosMrwController constructor.
     */
    public function __construct()
    {
        // http://sagec-test.mrw.es/MRWEnvio.asmx?WSDL (PRE)
        // http://sagec.mrw.es/MRWEnvio.asmx?WSDL (PRO)
        $this->wsdl_url = "https://sagec-test.mrw.es/MRWEnvio.asmx?WSDL";

        $this->cabeceras = array(
            'CodigoFranquicia' => $this->FranquiciaMRW, //Obligatorio
            'CodigoAbonado' => $this->AbonadoMRW, //Obligatorio
            'CodigoDepartamento' => $this->DepartamentoMRW, //Opcional - Se puede omitir si no se han creado departamentos en sistemas de MRW.
            'UserName' => $this->usuarioMRW, //Obligatorio
            'Password' => $this->PasswordMRW //Obligatorio
        );
    }

    /**
     * Inicializador SOAP
     *
     * @return false
     */
    private function init(){

        try {
            $this->clientMRW = new SoapClient(
                $this->wsdl_url,
                array(
                    'trace' => TRUE
                )
            );
        } catch (SoapFault $e) {
            printf("Error creando cliente SOAP: %s<br />\n", $e->__toString());
            return false;
        }
    }

    /**
     * Index de la llamada con el id
     *
     * @param Request $request
     */
    public function index(Request $request){
        $idDropshipping = $request->id;

        //dd($this->getDropShipping($idDropshipping));
        /*try {
            $this->clientMRW = new SoapClient(
                $this->wsdl_url,
                array(
                    'trace' => TRUE
                )
            );
        } catch (SoapFault $e) {
            printf("Error creando cliente SOAP: %s<br />\n", $e->__toString());
            return false;
        }*/
        $printMRW = DB::table('dropshipping')
            ->where('idDropshipping', $idDropshipping)
            ->update(array(
                "printMRW" => 1
            ));

        $this->enviarEtiqueta($this->getDropShipping($idDropshipping));
    }

    /**
     * SQL obtiene el los valores de la tabla dropshiping con los correspondientes al cliente
     *
     * @param $idDropshipping
     * @return mixed
     */
    public function getDropShipping($idDropshipping){
        $drop = DB::table('dropshipping')
            ->select('dropshipping.*', 'clienteDrop.*')
            ->join('clienteDrop', 'clienteDrop.id', '=', 'dropshipping.idCliente')
            ->where('idDropshipping', $idDropshipping)
            ->get();

        return $drop;
    }

    /**
     * @param $datosCliente
     * @return string
     */
    public function enviarEtiqueta($datosCliente){
        $responseCode = '';

        ## PARAMETROS DEL ENVIO ##
        // Datos de fecha actual
        date_default_timezone_set('CET');
        $hoy = date("d/m/Y");
        $hoyFull = date('Ymd|His');

        foreach ($datosCliente as $datoCliente){
            $params = array(
                'request' => array(
                    'DatosEntrega' => array(
                        ## DATOS DESTINATARIO ##
                        'Direccion' => array(
                            'CodigoDireccion' => '', //Opcional - Se puede omitir. Si se indica sustituira al resto de parametros
                            'CodigoTipoVia' => $datoCliente->via, //Opcional - Se puede omitir aunque es recomendable usarlo
                            'Via' => $datoCliente->calle, //Obligatorio
                            'Numero' => $datoCliente->numero, //Obligatorio - Recomendable que sea el dato real. Si no se puede extraer el dato real se pondra 0 (cero)
                            'Resto' => $datoCliente->resto, //Opcional - Se puede omitir.
                            'CodigoPostal' => $datoCliente->cp, //Obligatorio
                            'Poblacion' => $datoCliente->localidad, //Obligatorio
                            'Provincia' => $datoCliente->provincia, //Obligatorio
                            //'Estado' => '', //Opcional - Se debe omitir para envios nacionales.

                        ),
                        'Nif' => '', //Opcional - Se puede omitir.
                        'Nombre' => $datoCliente->nombre . ' ' . $datoCliente->apellido1 . ' ' . $datoCliente->apellido2, //Obligatorio
                        'Telefono' => $datoCliente->telefono, //Obligatorio
                        'Contacto' => '', //Opcional - Muy recomendable
                        'ALaAtencionDe' => '', //Opcional - Se puede omitir.
                        'Observaciones' => $datoCliente->idTienda //Opcional - Se puede omitir.
                    ),
                    ## DATOS DEL SERVICIO ##
                    'DatosServicio' => array(
                        'Fecha' => $hoy,  //Obligatorio. Fecha >= Hoy()
                        'Referencia' => 'Boann|' . $hoyFull,  //Obligatorio. ¿numero de pedido/albaran/factura?
                        'EnFranquicia' => 'N',
                        'CodigoServicio' => '0200', //Obligatorio. Cada servicio deberá ser activado por la franquicia
                        ## Desglose de Bultos <--
                        'NumeroBultos' => '1', //Obligatorio. Solo puede haber un bulto por envio en eCommerce
                        'Peso' => '2', //Obligatorio. Debe ser igual al valor Peso en BultoRequest si se ha usado
                        'Reembolso' => '', //Opcional - Se puede omitir. (coste adicional)
                        'ImporteReembolso' => '', //Obligatorio si hay reembolso. Los decimales se indican con , (coma)
                        
                        'TramoHorario'	=>	'0', //Opcional - Horario de entrega (coste adicional)
                        // 0 = Sin tramo (8:30h a 19h). Por defecto si se omite.
                        // 1 = Mañana (8:30h a 14h)
                        // 2 = Tarde (14h a 19h)
                    )
                )
            );
        }

        // Conectamos con el servicio SOAP
        try {
            $clientMRW = new SoapClient(
                $this->wsdl_url,
                array(
                    'trace' => true,
                    'cache_wsdl' => WSDL_CACHE_MEMORY
                )
            );
        } catch (\Exception $e) {
            $err = "sendCarrierMRW:-> Error creando cliente SOAP: %s" . PHP_EOL . $e->__toString();
            $this->writeToFileParam($err);
            return false;
        }

        // Cargamos los headers sobre el objeto cliente SOAP
        $header = new \SoapHeader('http://www.mrw.es/', 'AuthInfo', $this->cabeceras);


        $clientMRW->__setSoapHeaders($header);

        // Invocamos el metodo TransmEnvio pasandole los parametros del envio
        try {
            $responseCode = $clientMRW->TransmEnvio($params);

            //dd($params);
            //echo "<br /><br />\$responseCode:<br /><pre>";
            //print_r($responseCode);
            //echo '</pre>';
        } catch (SoapFault $exception) {
            printf("Error llamando al metodo TransmEnvio del WebService MRW: %s<br />\n", $exception->__toString());
            echo "<br /><br />Solicitud SOAP enviada (aparece como un XML plano):<br />" . $clientMRW->__getLastRequest();
            return false;
        }

        // tomar la respuesta y comprobar si ocurrio algun error
        if ( 0 == $responseCode->TransmEnvioResult->Estado ) {
            // No se ha generado la orden de envio y mostramos el error generado
            echo 'ERROR:<br>'.$responseCode->TransmEnvioResult->Mensaje . "<br>
			Request enviada:<br>
			" . $clientMRW->__getLastRequest();
        } else if ( 1 == $responseCode->TransmEnvioResult->Estado ) {
            // La orden de envio se ha generado correctamente

            // Montamos la URL para recuperar la información del envío y la etiqueta
            $urlParams = $responseCode->TransmEnvioResult->Url .
                "?Franq=" . $this->FranquiciaMRW .
                "&Ab=" . $this->AbonadoMRW .
                "&Dep=" . $this->DepartamentoMRW .
                "&Pwd=" . $this->PasswordMRW .
                "&NumSol=" . $responseCode->TransmEnvioResult->NumeroSolicitud .
                "&Us=" . $this->usuarioMRW .
                "&NumEnv=" . $responseCode->TransmEnvioResult->NumeroEnvio;
            // Y cargamos dicha URL en un iframe para mostrar el resultado
            echo '
		<iframe name="resultados" src="'.$urlParams.'" width="100%" height="100%" ></iframe>
		';
        } else {
            // No sabemos que ha pasado y mostramos un mensaje
            echo "Se ha producido un error no identificado. Consulte al administrador de la web";
        }
        // Destruimos el objeto cliente
        unset($clientMRW);
    }

}