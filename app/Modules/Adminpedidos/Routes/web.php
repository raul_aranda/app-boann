<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'adminpedidos', 'middleware' => ['role:admin']], function () {
    // Listado de pedidos
    Route::get('/', 'PedidosBackendController@index')->name('pedidos.listado');

    // Filtro de pedidos
    Route::post('/filtro', 'PedidosBackendController@filtro')->name('filtro.pedidos');
    Route::get('orden-cliente', 'PedidosBackendController@orden')->name('pedidos.orden');

    // Excel taller
    Route::post('excel-taller', 'PedidosTallerController@index')->name('pedidos.taller');

    // CSV Etiquetas
    Route::post('csv-taller', 'PedidosTallerController@index')->name('csv.taller');

    // Albaranes
    Route::post('excel-cabecera', 'PedidosAlbaranController@cabecera')->name('pedidos.cabecera');
    Route::post('excel-detalle', 'PedidosAlbaranController@detalle')->name('pedidos.detalle');

    // Pdf pedido
    Route::get('/pedido', 'PdfPedidosController@index')->name('pedidos.pdf');

    // Send mail
    Route::get('/sendmail', 'PdfPedidosController@sendMail')->name('pedidos.mail');
    Route::get('/validate', 'PedidosBackendController@validatePedido')->name('pedidos.validate');

    // Dropshipping MRW
    Route::get('/mrw', 'PedidosMrwController@index')->name('pedidos.mrw');

    // Validar check seleccionados múltiples
    Route::post('/checkall', 'PedidosBackendController@validateMultiPedido');
});
