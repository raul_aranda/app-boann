<div class="pull-right mb-10 hidden-sm hidden-xs">
    {{ link_to_route('mensajes.index', 'Todos los Mensajes', [], ['class' => 'btn btn-primary btn-xs']) }}
    {{ link_to_route('mensajes.create', 'Nuevo Mensaje', [], ['class' => 'btn btn-success btn-xs']) }}
</div><!--pull right-->

<div class="clearfix"></div>