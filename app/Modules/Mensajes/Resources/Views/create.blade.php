@extends('admin.layouts.back')

@section('after-styles')
    {{ Html::style("/summernote/summernote.css") }}
@stop

@section('page-header')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../index.html">Home</a>
        </li>
        <li class="breadcrumb-item active">Mensajes</li>
    </ol>
    <h1 class="page-title">
        Administración de Mensajes
    </h1>
    <div class="page-header-actions">
        @include('mensajes::partials.mensajes-header-buttons')
    </div>
@endsection

@section('content')
    {{ Form::open(['route' => 'mensajes.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}

    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Modificar Mensaje</h3>
        </div><!-- /.box-header -->
        <div class="panel-toolbar"></div>

        <div class="panel-body">
            <div class="row">
                <div class="form-group">
                    {{ Form::label('txttitulo', 'Título', ['class' => 'col-md-2 control-label']) }}

                    <div class="col-md-10">
                        {{ Form::text('txttitulo', null, ['class' => 'form-control', 'placeholder' => 'Título']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div>

            <div class="row">
                <div class="form-group">
                    {{ Form::label('email', 'Mensaje', ['class' => 'col-md-2 control-label']) }}

                    <div class="col-md-10">
                        <textarea class="input-block-level" id="summernote" name="txtmensaje" rows="18"></textarea>
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div>

            <div class="row">
                <div class="form-group">
                    {{ Form::label('status', 'Activo', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-1">
                        {{ Form::checkbox('status', '1') }}
                    </div><!--col-lg-1-->
                </div><!--form control-->
            </div>
        </div><!-- /.box-body -->
    </div><!--box-->

    <div class="panel-footer">
        <div class="pull-left">
            {{ link_to_route('mensajes.index', 'Cancelar', [], ['class' => 'btn btn-danger']) }}
        </div><!--pull-left-->
        <div class="pull-right">
            {{ Form::submit('Actualizar', ['class' => 'btn btn-success']) }}
        </div><!--pull-right-->
        <div class="clearfix"></div>
    </div><!-- /.box-body -->

    </form>



@stop

@section('after-scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.js"></script>
    {{ Html::script("/summernote/summernote.js") }}
    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                height: 300,                 // set editor height
                minHeight: null,             // set minimum height of editor
                maxHeight: null,             // set maximum height of editor
                focus: true                  // set focus to editable area after initializing summernote
            });
        });
    </script>

@stop
