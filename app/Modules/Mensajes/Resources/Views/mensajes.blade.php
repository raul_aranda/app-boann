@extends('admin.layouts.back')

@section('after-styles')
    {{ Html::style("/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css") }}
@stop

@section('page-header')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../index.html">Home</a>
        </li>
        <li class="breadcrumb-item active">Mensajes</li>
    </ol>
    <h1 class="page-title">
        Listado de Mensajes
    </h1>
    <p class="page-description">Mensajes</p>
    <div class="page-header-actions">
        @include('mensajes::partials.mensajes-header-buttons')
    </div>
@endsection

@section('content')
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Listado de Mensajes</h3>
        </div><!-- /.box-header -->
        <div class="panel-body">
            {!! Form::open(['url' => '', 'method' => 'post', 'id' => 'listado']) !!}
            <div class="table-responsive">
                @if($data)
                    <table id="users-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>Título </th>
                            <th>Mensaje</th>
                            <th>Fecha</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $row)
                            <tr>
                                <td>
                                </td>
                                <td>{{ $row->id }}</td>
                                <td>{{ $row->titulo }}</td>
                                <td>
                                    {{ strip_tags($row->mensaje) }}
                                </td>
                                @php($fecha = \Carbon\Carbon::parse($row->created_at)->format('d-m-Y'))
                                <td>{{ $fecha }}</td>
                                <td>
                                    <a href="{{ route('mensajes.edit', $row->id) }}" class="btn btn-xs btn-info"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Editar"></i></a>
                                    @if ($row->activado == 0)
                                        <a href="{{ route('mensajes.activate', $row->id) }}" class="btn btn-xs btn-success"><i class="fa fa-play" data-toggle="tooltip" data-placement="top" title="Activar"></i></a>
                                    @else
                                        <a href="{{ route('mensajes.deactivate', $row->id) }}" class="btn btn-xs btn-warning"><i class="fa fa-pause" data-toggle="tooltip" data-placement="top" title="Desactivar"></i></a>
                                    @endif
                                    <a href="{{ route('mensajes.delete', $row->id) }}" class="btn btn-xs btn-danger"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Eliminar"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                @endif
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
@section('after-scripts')

@stop