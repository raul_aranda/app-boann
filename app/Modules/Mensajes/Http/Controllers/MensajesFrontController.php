<?php

namespace App\Modules\Mensajes\Http\Controllers;

use App\Modules\Mensajes\Models\Mensaje;
use App\Modules\Mensajes\Models\Mensajes_leido;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;

class MensajesFrontController extends Controller
{
    /**
     * MensajesFrontController constructor.
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request){
        $codigo = Auth::user()->codigo_cliente;

        $noticias = DB::table('mensajes as m')
            ->leftJoin('mensajes_leidos as l', function ($join) use ($codigo){
                $join->on('l.id_mensaje', '=', 'm.id')
                ->where('l.id_cliente', '=', $codigo);
            })
            ->select('m.*', 'l.id as lid', 'l.id_mensaje as lid_mensaje', 'l.id_cliente', 'l.leido')
            ->where('m.activado', 1)
            ->orderBy('m.id', 'desc')
            ->get();

        //dd($noticias);

        $view = View::make('partials.mensajes-user', ['mensaje' => $noticias]);

        if ($request->ajax()){
            $sections = $view->renderSections();
            return Response::json($sections['mensajes-user']);
        }else
            return $view;
    }

    /**
     * @return mixed
     */
    public function countMessages(){
        $codigo = Auth::user()->codigo_cliente;

        $contador = DB::table('mensajes as m')
            ->leftJoin('mensajes_leidos as l', function ($join) use ($codigo){
                $join->on('l.id_mensaje', '=', 'm.id')
                    ->where('l.id_cliente', '=', $codigo);
            })
            ->select('m.*', 'l.id as lid', 'l.id_mensaje as lid_mensaje', 'l.id_cliente', 'l.leido')
            ->where('m.activado', 1)
            ->whereNull('l.leido')
            ->count();

        //dd($contador);

        return $contador;
    }

    public function leido(Request $request){
        $codigo = Auth::user()->codigo_cliente;
        $leido = new Mensajes_leido();

        $leido->leido = 1;
        $leido->id_cliente = $codigo;
        $leido->id_mensaje = $request->id;

        $leido->save();
    }

}



