<?php

namespace App\Modules\Mensajes\Http\Controllers;

use App\Modules\Mensajes\Models\Mensaje;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MensajesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Mensaje::orderBy('id', 'desc')->get();

        return view('mensajes::mensajes', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mensajes::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mensaje = new Mensaje();

        $mensaje->titulo    = $request->txttitulo;
        $mensaje->mensaje   = $request->txtmensaje;
        $mensaje->activado  = (!$request->status) ? '0' : '1';

        $mensaje->save();

        return redirect()->route('mensajes.index')->withFlashSuccess('Mensaje creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mensaje = Mensaje::find($id);

        return view('mensajes::edit', compact('mensaje'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mensaje = Mensaje::find($id);

        $mensaje->titulo    = $request->txttitulo;
        $mensaje->mensaje   = $request->txtmensaje;
        $mensaje->activado  = (!$request->status) ? '0' : '1';

        $mensaje->save();

        return redirect()->route('mensajes.index')
            ->withFlashSuccess('Mensaje con título: <strong>'. $request->txttitulo .'</strong> modificado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $mensaje = Mensaje::find($id);

        $mensaje->delete();

        return redirect()->route('mensajes.index')->withFlashSuccess('Mensaje eliminado');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function activate($id){
        $mensaje = Mensaje::find($id);

        $mensaje->activado = 1;
        $mensaje->save();

        return redirect()->route('mensajes.index')->withFlashSuccess('Mensaje activado');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deactivate($id){
        $mensaje = Mensaje::find($id);

        $mensaje->activado = 0;
        $mensaje->save();

        return redirect()->route('mensajes.index')->withFlashSuccess('Mensaje desactivado');
    }
}
