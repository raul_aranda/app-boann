<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'mensajes'], function () {
    Route::get('/', function () {
        dd('This is the Mensajes module index page. Build something great!');
    });
    Route::resource('/mensajes', 'MensajesController');

    Route::get('/activate/{id}', 'MensajesController@activate')->name('mensajes.activate');
    Route::get('/deactivate/{id}', 'MensajesController@deactivate')->name('mensajes.deactivate');
    Route::get('/delete/{id}', 'MensajesController@delete')->name('mensajes.delete');

    Route::get('/noti', 'MensajesFrontController@index');

    Route::get('/count', 'MensajesFrontController@countMessages');

    Route::get('/leido', 'MensajesFrontController@leido')->name('mensaje.leido');
});
