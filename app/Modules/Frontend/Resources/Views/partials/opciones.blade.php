{{--Nuevo código --}}
<div class="row">
    <div class="col-lg-3" id="select">
        @include('frontend::partials.select.productos')
    </div>
    <div class="col-lg-1 modelo" id="contador">
        {{ Form::selectRange('cantidad', 1, 10, 1, ['class' => 'form-control', 'id' => 'cantidad']) }}
    </div>
    <div class="col-lg-1 modelo" id="option1">
    </div>
    <div class="col-lg-1 modelo"  id="option2">
    </div>
    <div class="col-lg-1 modelo"  id="option3" style="padding-left: 3px">
    </div>
    <div class="col-lg-1 modelo"  id="dibujo" style="padding-left: 3px">
    </div>
    <div class="col-lg-1 modelo"  id="opcion" style="padding-left: 5px;">
    </div>
    <div class="col-lg-1 modelo"  id="option4">
    </div>
    <div class="col-lg-1 modelo"  id="optionButton" style="padding-left: 5px;">
        {{ Form::button('<span class="fa fa-plus"></span',['type' => 'button', 'class' => 'btn btn-success right', 'id' => 'acept']) }}
    </div>

</div>
{{--Fin nuevo código--}}


<br><br><br><br><br><br><br>

<div class="col-md-3"style="padding-right: 5px; width: 300px; padding-left: 1px;">
    {{--    @include('frontend::partials.select.productos')--}}
</div>

<div class="col-md-1" style="padding-left: 5px; padding-right: 5px; width: 80px;">
    {{-- Form::selectRange('cantidad', 1, 10, 1, ['class' => 'form-control', 'id' => 'cantidad']) --}}
</div>
<div class="col-md-1" style="padding-left: 5px; padding-right: 5px; width: 180px;" id="colour">

</div>
<div class="col-md-2" style="padding-left: 5px; padding-right: 5px; width: 200px;">
    <div id="divTalla"></div>
    <div id="pegatinas"></div>
</div>
<div class="col-md-2" style="padding-left: 10px; padding-right: 5px; width: 110px;" id="opcional">

</div>
<div class="col-md-2" style="padding-left: 5px; padding-right: 5px; width: 242px;">
    <div id="grabacion"></div>
    <div id="frases"></div>
</div>
{!! Form::open(['route' => 'add.linea', 'method' => 'get', 'id' => 'send']) !!}

<div class="col-md-1" style="padding-left: 10px; padding-right: 1px; width: 40px;">
    {{-- Form::button('<span class="fa fa-plus"></span',['type' => 'submit', 'class' => 'btn btn-success right']) --}}
</div>

<input type="hidden" id="txtproducto" placeholder="producto" name="txtproducto">
<input type="hidden" id="txtcantidad" placeholder="cantidad" name="txtcantidad">
<input type="hidden" id="txtcolor" placeholder="color" name="txtcolor">
<input type="hidden" id="txtopcional" placeholder="opcional" name="txtopcional">
<input type="hidden" id="txtTetina" placeholder="tetina" name="txtTetina">
<input type="hidden" id="txtTalla" placeholder="talla" name="txtTalla">
<input type="hidden" id="txtIdTetinaTalla" placeholder="ID Tetina/talla" name="txtIdTetinaTalla">
<input type="hidden" id="txtModelo" placeholder="modelo" name="txtModelo">
<input type="hidden" id="personalizado" placeholder="personalizado" name="personalizado">

{!! Form::close() !!}
