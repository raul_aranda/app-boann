
<table id="users-table" class="table table-condensed table-hover">
    <thead>
    <tr>
        <th width="2%">No.</th>
        <th width="20%">Fecha</th>
        <th width="10%">Líneas productos</th>
        <th width="10%">Albarán</th>
        <th width="3%">Pdf</th>
    </tr>
    </thead>
    <tbody>
    @if (count($order) != 0)
        @php($contador = 1)
        @foreach($order as $item)
            @php
                //$valueNIF = str_limit(Auth::user()->barras, 9);
                $valueNIF = substr(Auth::user()->barras, 3, 9);
                $numPedido  = substr($item->fecha, 0, 4);
                $numPedido .= "-0" . $item->num_art;
                $numPedido .= "0" . $item->id;
                $numPedido .= "-" . substr($item->fecha,9, 2);
            @endphp
            <tr>
                <td>{{$contador}}</td>
                <td>{{$item->fecha}}</td>
                <td>{{$item->num_art}}</td>
                <td>{{ str_pad($numPedido,12,"0", STR_PAD_LEFT) }}</td>

                <td>
                    <a href="{{route('create.pdf', ['id' => $item->id])}}" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>
                </td>
            </tr>
            @php($contador ++)
        @endforeach
    @else
        <tr><td colspan="5">Records not found!</td></tr>
    @endif
    </tbody>
</table>

{{ $order->links() }}