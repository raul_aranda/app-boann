<div class="page-content">
    <ul class="taskboard-stages ui-sortable" id="taskboard-stages">
        <li class="taskboard-stage">
            <header class="taskboard-stage-header ui-sortable-handle">
                <h5 class="taskboard-stage-title">Mensajes</h5>
            </header>
            <div class="taskboard-stage-content">
                <ul class="list-group taskboard-list ui-sortable">
                    <li class="list-group-item priority-urgent ui-sortable-handle" data-taskboard="slidePanel">
                        <div class="checkbox-custom checkbox-primary">
                            <input name="checkbox" type="checkbox"><label class="task-title">Perspecta historiae studiis</label>
                        </div>
                        <div class="task-badges">
                            <span class="task-badge task-badge-subtask icon md-format-list-bulleted">1/2</span>
                            <span class="task-badge task-badge-attachments icon md-attachment-alt">2</span>
                            <span class="task-badge task-badge-comments icon md-comment">2</span></div>
                        <ul class="task-members">
                            <li>Marcar como leído</li>
                        </ul>
                    </li>
                    <li class="list-group-item priority-high ui-sortable-handle" data-taskboard="slidePanel">
                        <div class="checkbox-custom checkbox-primary"><input name="checkbox" type="checkbox">
                            <label class="task">neque profecta fictae</label>
                        </div>
                    </li>
                    <li class="list-group-item priority-normal ui-sortable-handle" data-taskboard="slidePanel">
                        <div class="checkbox-custom checkbox-primary"><input name="checkbox" type="checkbox">
                            <label class="task-title">neque profecta fictae</label>
                        </div>
                        <div class="task-badges"><span class="task-badge task-badge-subtask icon md-calendar">10/29</span></div>
                        <ul class="task-members">
                            <li>Marcar como leído</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </li>
    </ul>
</div>


<div class="page-content">
    <ul class="taskboard-stages ui-sortable" id="taskboard-stages">
        <li class="taskboard-stage">
            <header class="taskboard-stage-header ui-sortable-handle">
                <h5 class="taskboard-stage-title">Mensajes</h5>
            </header>
            <div class="taskboard-stage-content">
                <ul class="list-group taskboard-list ui-sortable">
                    @foreach($mensajes as $row)
                        <li class="list-group-item priority-high ui-sortable-handle" data-taskboard="slidePanel">
                            <div class="checkbox-custom checkbox-primary"><input name="checkbox" type="checkbox">
                                <label class="task">{{ $row->titulo }}</label>
                            </div>
                        </li>
                        <li class="list-group-item priority-normal ui-sortable-handle" data-taskboard="slidePanel">
                            <div class="checkbox-custom checkbox-primary"><input name="checkbox" type="checkbox">
                                <label class="task-title">{{ $row->mensaje }}</label>
                            </div>
                            <div class="task-badges"><span class="task-badge task-badge-subtask icon md-calendar">10/29</span></div>
                            <ul class="task-members">
                                <li>Marcar como leído</li>
                            </ul>
                        </li>
                    @endforeach