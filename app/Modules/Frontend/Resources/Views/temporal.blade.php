
    <table id="users-table" class="table table-condensed table-hover">
        <thead>
        <tr>
            <th width="2%">No.</th>
            <th width="28%">Producto</th>
            <th width="10%">Uds.</th>
            <th width="15%">Color</th>
            <th width="8%">Tetina</th>
            <th width="10%">Talla</th>
            <th width="25%">Nombre</th>
            <th width="2%"></th>
        </tr>
        </thead>
        <tbody>
        @if (count($data) != 0)
            @php($contador = 1)
            @foreach($data as $item)
                @if($item->modelo)
                    @php($precio)
                @endif
                <tr>
                    <td>{{$contador}}</td>
                    @if(!$item->modelo)
                        <td>{{$item->descripcion}}</td>
                    @else
                        <td>{{$item->descripcion . ' ' . $item->modelo}}</td>
                    @endif
                    <td>{{$item->descripcion . ' ' . $item->modelo}}</td>
                    <td>{{$item->cantidad}}</td>
                    <td>{{$item->descripcion_color}}</td>

                    <td>{{$item->tetina}}</td>
                    <td>{{$item->talla}}</td>

                    <td>{{$item->nombre}}</td>
                    @if($item->descripcion_color == 'Premium 01' || $item->descripcion_color == 'Premium 02' || $item->descripcion_color == 'Premium 03' || $item->descripcion_color == 'Premium 04'  )
                        <td><input type="hidden" value="{{ $item->cantidad * 5.95 }}" id="task" name="task[]"></td>
                    @elseif($item->price)
                        <td><input type="hidden" value="{{ $item->price * $item->cantidad }}" id="task" name="task[]"></td>
                    @elseif($item->talla == 'Mixto')
                        <td><input type="hidden" value="{{ $item->cantidad * 8.85 }}" id="task" name="task[]"></td>
                    @else
                        <td><input type="hidden" value="{{ $item->precio*$item->cantidad}}" id="task" name="task[]"></td>
                    @endif
                    <td style="text-align: right">
                        <a href="#" class="btn btn-danger waves-effect waves-light" onclick="deleteRecord('{{ $item->id_pedido }}')"><span class="fa fa-trash-o" aria-hidden="true"></span></a>
                    </td>
                </tr>
                @php($contador ++)
            @endforeach

        @else
            <tr><td colspan="8">No existe ninguna línea de producto</td></tr>
        @endif
        </tbody>
    </table>
