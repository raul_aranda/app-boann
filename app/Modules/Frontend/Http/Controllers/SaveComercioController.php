<?php

namespace App\Modules\Frontend\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SaveComercioController extends Controller
{
    function index(Request $request){
        $comercio = $request->comercio;
        $userId = $request->id;

        $usuario = User::find($userId);
        $usuario->agente = $comercio;
        $usuario->save();

        Return;
    }
}
