<?php

namespace App\Modules\Frontend\Http\Controllers;

use App\Pedidostmps;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;

class AddLineaController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request){
        $mensaje = array_add(['title' => 'title'], 'message', 'mensaje');

        if (!$request->model || $request->model == 'Modelo'){
            $mensaje = array_add(['title' => 'Error'], 'message', 'Modelo no seleccionado');
        }

        if(!$request->talla || $request->talla == 'Opciones' || !$request->tetina || $request->talla == 'Dibujo' || $request->tetina == 'Opción') {
            $mensaje = array_add(['title' => 'Error'], 'message', 'Talla/Tetina no seleccionado');
        }

        if ($request->talla == "Opción"){
            $mensaje = array_add(['title' => 'Error'], 'message', 'Opción no seleccionado');
        }

        if ($request->tetina == "Dibujo"){
            $mensaje = array_add(['title' => 'Error'], 'message', 'Dibujo no seleccionado');
        }

        if(!$request->color || $request->color == 'Color') {
            $mensaje = array_add(['title' => 'Error'], 'message', 'Color no seleccionado');
        }

        if(!$request->perso || $request->perso == 'Grabación') {
            $mensaje = array_add(['title' => 'Error'], 'message', 'El campo Personalización/Frase está vacío');
        }

        if($request->talla == 'manga'){
            $mensaje = array_add(['title' => 'Error'], 'message', 'Debes elegir el body con manga corta o larga');
        }

        if ($mensaje['title'] == 'title') {
            $mensaje = array_add(['title' => 'Ok'], 'message', 'Línea añadido correctamente');

            $codigo = Auth::user()->codigo_cliente;
            $now = new \DateTime();

            $add = new Pedidostmps();
            $add->id_producto   = $request->produ;

            if ($request->model == 'no'){
                $modelo = '';
            }else{
                $modelo = $request->model;
            }

            $add->modelo        = $modelo;
            $add->cantidad      = $request->canti;

            if ($request->color == "-- --" || $request->color == "--- ---"){
                $add->id_color = 0;
            }else{
                $add->id_color = $request->color;
            }

            //$add->id_color      = ($request->color == "--- ---") ? 0 : $request->color;
            $add->tetina        = $request->tetina;
            $add->talla         = $request->talla;
            $add->nombre        = ($request->perso == 'disabled') ? '': $request->perso;
            $add->fecha         = $now;
            $add->id_cliente    = $codigo;

            $add->save();
        }


        //$view = View::make('partials.messages-front')->with('mensaje', $mensaje);
        $view = View::make('partials.messages-front', ['mensaje' => $mensaje]);

        if ($request->ajax()){
            $sections = $view->renderSections();
            return Response::json($sections['mensajes']);
        }else

        return $view;
    }
}
