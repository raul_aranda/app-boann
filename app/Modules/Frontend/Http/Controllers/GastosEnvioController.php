<?php

namespace App\Modules\Frontend\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class GastosEnvioController extends Controller
{
    /**
     * @param Request $request
     * @return array
     */
   public function index(Request $request)
   {

       $gastosEnvio = DB::table('envios')
           ->where('envios.nombre', $request->id)
           ->get();

       foreach ($gastosEnvio as $gasto){
           $gratis = $gasto->gratis;
           $descuento = $gasto->descuento;
           $minDescuento = $gasto->min_descuento;
           $nombre = $gasto->nombre;
       }

       return array(
           'gratis' => $gratis,
           'descuento' => $descuento,
           'minDescuento' => $minDescuento,
           'nombre' => $nombre
       );
   }
}
