<?php

namespace App\Modules\Frontend\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PedidosController extends Controller
{
    /**
     * PedidosController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function pedidoTemporal(){
        $codigo = Auth::user()->codigo_cliente;

        $result = DB::table('pedidostmps')
            ->join('productos', 'productos.id', '=', 'pedidostmps.id_producto')
            ->leftjoin('color', 'pedidostmps.id_color', '=', 'color.id_color')
            ->leftjoin('opcion_1','opcion_1.value', '=', 'pedidostmps.modelo')
            ->where('pedidostmps.id_cliente','=', $codigo)
            ->orderby('id_pedido', 'asc')
            ->get();

        return view('frontend::temporal',['data' => $result]);
    }
}
