<?php

namespace App\Modules\Frontend\Http\Controllers;

use App\Color;
use App\Modules\Frontend\Models\Grabacion;
use App\Modules\Frontend\Models\productos_opciones;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CreateSelectController extends Controller
{
    /**
     * CreateSelectController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    var $option1;
    var $selectOption1;
    var $selectOption2;
    var $selectOption3;
    var $selectOption4;
    var $selectDibujo;
    var $selectOpcion;


    /**
     * Función que en caso de tener un subproducto, este se muestre en un select
     *
     * @param $idProducto
     * @return string
     */
    public function opcionPrimera($idProducto){
        /*$opciones = DB::table('productos_opciones')
            ->crossjoin('opcion_1', 'opcion_1.id_opcion' , '=', 'productos_opciones.opcion_1' )
            ->where('productos_opciones.id_producto', '=', $idProducto)
            ->orderBy('productos_opciones.id_opcion', 'ASC')
            ->get();*/

        $opciones = DB::table('opcion_1')
            ->where('id_opcion', '=', $idProducto)
            ->orderBy('orden', 'ASC')
            ->get();

        if (!$opciones->isEmpty()) {
            $this->selectOption1 = '<select class="form-control" id="selectModelo">';
            $this->selectOption1 .= '<option hidden selected="selected">Modelo</option>';
            foreach ($opciones as $opcion) {
                $this->selectOption1 .= '<option value='. $opcion->value .'>' . $opcion->name . '</option>';
            }
            $this->selectOption1 .= '</select>';
        }

        return $this->selectOption1;
    }

    /**
     * Funcion donde se crea el select Talla/tetina dependiendo del color seleccionado
     *
     * @param Request $request
     * @return string
     */
    public function createTalla(Request $request)
    {
        $idColor = $request->id;

        $tetinaTallas = DB::table('tetinaTalla')
            ->where('codigo_color', '=', $idColor)
            ->get();

        if ($tetinaTallas->isEmpty()) {
            $this->selectOption3 = '<select class="form-control" id="option3" disabled>';
            $this->selectOption3 .= '<option selected="selected" >--- ---</option>';
        } else {
            $this->selectOption3 = '<select class="form-control" id="option3">';
            $this->selectOption3 .= '<option hidden selected="selected">Talla/Tetina</option>';
            foreach ($tetinaTallas as $tetinaTalla) {
                //$opcion = $tetinaTalla->idTetinaTalla . '?' . $tetinaTalla->tetina . '_' . $tetinaTalla->talla;
                $opcion =  $tetinaTalla->tetina . '_' . $tetinaTalla->talla;
                //$this->selectOption3 .= '<option value=' . '"' . $opcion . '"'. '>' .$tetinaTalla->idTetinaTalla.'-'. $tetinaTalla->tetina . ' ' . $tetinaTalla->talla . '</option>';
                $this->selectOption3 .= '<option value=' . '"' . $opcion . '"'. 'id="' . $tetinaTalla->idTetinaTalla . '">' . $tetinaTalla->tetina . ' ' . $tetinaTalla->talla . '</option>';

            }
        }

        /*dd($tetinaTallas);

        if ($tetinaTallas[0]->grabacion == 'disabled'){
            $this->selectOption3 = '<select class="form-control" id="option3" disabled>';
            $this->selectOption3 .= '<option selected="selected" >--- ---</option>';

            $this->selectOption4 = '<input id="personalizados" class="form-control" placeholder="Grabación" type="text" disabled>';
        }else{
            $this->selectOption4 = '<input id="personalizados" class="form-control" placeholder="Grabación" type="text">';
        }*/

        $this->selectOption3 .= '</select>';

        return array(
            'option3' => $this->selectOption3,
            //'option4' => $this->selectOption4
        );
    }

    /**
     * Función donde se crea el campo de texto de Grabación
     *
     * @param Request $request
     * @return string
     */
    public function createGrabacion(Request $request){
        $idTetinaTalla = $request->id;
        $activo = '';

        //$grabacion = Grabacion::where('idTetinaTalla', '=', $idTetinaTalla)->first();
        $grabacion = DB::table('grabacion')
            ->where('idTetinaTalla' , '=', $idTetinaTalla)
            ->first();

        if (!$grabacion){
            $activo = '';
            $placeholder = 'Grabación';
        }else{
            $activo = $grabacion->activo;
            $placeholder = '-';
            $value = '-';
        }

        $this->selectOption4 = '<input id="personalizados" class="form-control" placeholder="Grabación" type="text" '. $activo. '>';

        return $this->selectOption4;
    }

    /**
     * Index
     *
     * @param Request $request
     * @return array
     */
    public function index(Request $request){
       $idProducto = $request->id;
       $dibujo = 0;

        //-----------------------------------------------------
        //Opcion color
        //-----------------------------------------------------

        $colores = Color::where('codigo_producto', '=' , str_pad($idProducto, 3, '0', STR_PAD_LEFT))->first();

        if ($colores->codigo_color == '--'){
            $this->selectOption2  = '<select class="form-control" id="option2" disabled>';
            $this->selectOption2 .= '<option selected="selected" >-- --</option>';
        }else{
            $colores = DB::table('color')
                ->where('color.codigo_producto', '=', str_pad($idProducto, 3, '0', STR_PAD_LEFT))
                ->orderBy('codigo_color')
                ->get();

            $this->selectOption2  = '<select class="form-control" id="option2">';
            $this->selectOption2 .= '<option hidden selected="selected">Color</option>';
            foreach ($colores as $color) {
                $this->selectOption2 .= '<option value='.$color->id_color.'>' . $color->codigo_color. ' '. $color->descripcion_color. '</option>';
                $dibujo = $color->dibujo;
            }
        }

        if ($dibujo == 1){
            //Select dibujo
            $this->selectDibujo  = '<select class="form-control" id="dibujo">';
            $this->selectDibujo .= '<option selected="selected">Dibujo</option>';
            $this->selectDibujo .= '<option value="Niño">Niño</option>';
            $this->selectDibujo .= '<option value="Niña">Niña</option>';
            $this->selectDibujo .= '<option value="Niño/Niña">Niño/Niña</option>';
            $this->selectDibujo .= '<option value="Niño/Niño">Niño/Niño</option>';
            $this->selectDibujo .= '<option value="Niña/Niña">Niña/Niña</option>';

            $this->selectDibujo .= '</select>';

            //Select opción dibujo
            $this->selectOpcion  = '<select class="form-control Sopcion" id="opcion">';
            $this->selectOpcion .= '<option  selected="selected">Opción</option>';
            $this->selectOpcion .= '<option value="Interior">Interior</option>';
            $this->selectOpcion .= '<option value="Exterior">Exerior</option>';

            $this->selectOpcion .= '</select>';
        }

        $this->selectOption2 .= '</select>';

        //-----------------------------------------------------
        //Opcion talla/tetina
        //-----------------------------------------------------

        $this->selectOption3 = '<select class="form-control" id="option3">';
        $this->selectOption3 .= '<option hidden selected="selected">Opciones</option>';
        //foreach ($colores as $color) {
        //    $this->selectOption3 .= '<option value='.$color->id_color.'>' . $color->codigo_color. ' '. $color->descripcion_color . '</option>';
        //}
        $this->selectOption3 .= '</select>';


        //-----------------------------------------------------
        //Opción grabación
        //-----------------------------------------------------
        if ($idProducto == 38 || $idProducto == 53 || $idProducto == 51){
            $this->selectOption4 = '<input id="personalizados" class="form-control" placeholder="Grabación" type="text" disabled>';
        }else{
            $this->selectOption4 = '<input id="personalizados" class="form-control" placeholder="Grabación" type="text">';
        }

        //Devuelve el array con los distintos select
        return array(
            'option1'   => $this->opcionPrimera($idProducto),
            'option2'   => $this->selectOption2,
            'option3'   => $this->selectOption3,
            'option4'   => $this->selectOption4,
            'dibujo'    => $this->selectDibujo,
            'opcion'    => $this->selectOpcion,
        );

        //dd($opcion);
        
        $selectF = 'false';
        $selectP = 'false';

        switch ($request->id){
            case 0: // Promoción Navidad
                $selectC = $this->getColor('001', 'enabled');
                $selectT = $this->getTalla('', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 1: //Chupete
                $selectC = $this->getColor('001', 'enabled');
                $selectT = $this->getTalla('', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 2: //Cadena
                $selectC = $this->getColor('002', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 3: //Cepillo y peine
                $selectC = $this->getColor('003', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 4: //Biberon
                $selectC = $this->getColor('004', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 5: //Cepillo de dientes
                $selectC = $this->getColor('005', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 6: //Portachupetes
                $selectC = $this->getColor('006', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 7: //Sonajero
                $selectC = $this->getColor('007', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 8: //Caja premium
                $selectC = $this->getColor('--', 'disabled');
                $selectT = $this->getTalla('ropa', 'disabled');
                $txtPers = $this->getPerso('disabled');
                break;
            case 9: //Cadena premium
                $selectC = $this->getColor('009', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 10: //Packs premium
                $selectC = $this->getColor('010', 'enabled');
                $selectT = $this->getTalla('todos', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 11: //Chupete y cadena
                $selectC = $this->getColor('011', 'enabled');
                $selectT = $this->getTalla('todos', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 12: //Babero
                $selectC = $this->getColor('012', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            /*case 13: //Babero con frase
                $selectC = $this->getColor('013', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('disabled');
                break;*/
            case 14: //Quitababas
                $selectC = $this->getColor('014', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 15: //Bolsa merienda
                $selectC = $this->getColor('015', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 16: //Sujeta chupete tela
                $selectC = $this->getColor('016', 'enabled');
                $selectT = $this->getTalla('sujetachupe', 'enabled');
                $txtPers = $this->getPerso('disabled');
                break;
            case 17: //Etiquetas textil
                $selectC = $this->getColor('0017', 'enabled');
                $selectT = $this->getTalla('etiquetas', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 20: //Conjunto gorro
                $selectC = $this->getColor('020', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 21: //Camiseta premamá
                $selectC = $this->getColor('021', 'enabled');
                $selectT = $this->getTalla('ropa', 'enabled');
                $txtPers = $this->getPersoHidden(); //FRASE
                $selectF = $this->getFrase();
                break;
            case 22: //Pegatinas
                $selectC = $this->getColor('022', 'enabled');
                $selectT = $this->getTalla('pegatina', 'enabled');
                $txtPers = $this->getPerso('enabled');
                $selectP = $this->getOpcion();
                $selectP .= $this->getDibujo();
                break;
            case 23: //Pack canción
                $selectC = $this->getColor('023', 'enabled');
                $selectT = $this->getTalla('todos', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 24: //chapas
                $selectC = $this->getColor('017', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 25: //zapatillas
                $selectC = $this->getColor('025', 'enabled');
                $selectT = $this->getTalla('zapatillas', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 26: //pulsera
                $selectC = $this->getColor('017', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 27: //body
                $selectC = $this->getColor('027', 'enabled');
                $selectT = $this->getTalla('body', 'enabled');
                $txtPers = $this->getPersoHidden(); //FRASE
                break;
            case 28: //bolsa
                $selectC = $this->getColor('017', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 32: //caja regalo 01
                $selectC = $this->getColor('032', 'enabled');
                $selectT = $this->getTalla('todos', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 33: //caja regalo 02
                $selectC = $this->getColor('033', 'enabled');
                $selectT = $this->getTalla('todos', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 34: //vajilla perso
                $selectC = $this->getColor('034', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 35: //vaso
                $selectC = $this->getColor('034', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 36: //cubiertos
                $selectC = $this->getColor('034', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 37: //ambientador atrapa clientes
                $selectC = $this->getColor('--', 'disabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('disabled');
                break;
            case 38: //caja regalo vacia
                $selectC = $this->getColor('--', 'disabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('disabled');
                break;
            case 39: //babero halloween
                $selectC = $this->getColor('039', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('disabled');
                break;
            case 40: //body halloween
                $selectC = $this->getColor('039', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('disabled');
                break;
            case 41: //capa baño
                $selectC = $this->getColor('041', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 42: //lamparita
                $selectC = $this->getColor('042', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 43: //ambientador personalizado
                $selectC = $this->getColor('043', 'endabled');
                $selectT = $this->getTalla('---', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 44: //mochila personalizadad
                $selectC = $this->getColor('044', 'enabled');
                $selectT = $this->getTalla('mochila', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 45: //Talega asas
                $selectC = $this->getColor('045', 'enabled');
                $selectT = $this->getTalla('talega', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 46: //Talega pequeña
                $selectC = $this->getColor('046', 'enabled');
                $selectT = $this->getTalla('mochila', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 47: //Caja regalo 3
                $selectC = $this->getColor('047', 'enabled');
                $selectT = $this->getTalla('todos', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 48: //Manta estrella
                $selectC = $this->getColor('048', 'enabled');
                $selectT = $this->getTalla('--', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 49: //Dou dou estrella
                $selectC = $this->getColor('049', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 50: //Agua de colonia
            case 55: //pegatina suelo
                $selectC = $this->getColor('-', 'disabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 51: //Termómetro
            case 53: //pantalla facial
            case 54: //desinfectante limpiador
                $selectC = $this->getColor('--', 'disabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('disabled');
                break;
            case 52: //Gel antiséptico
                $selectC = $this->getColor('052', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('disabled');
                break;
            case 56: //pegatina puerta
                $selectC = $this->getColor('056', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 57: //mascarilla infantil
                $selectC = $this->getColor('057', 'enabled');
                $selectT = $this->getTalla('mascarillaAdulto', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 58: //mascarilla adulto
                $selectC = $this->getColor('058', 'enabled');
                $selectT = $this->getTalla('mascarillaAdulto', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 59: //bolsa porta objetos
                $selectC = $this->getColor('059', 'enabled');
                $selectT = $this->getTalla('bolsa', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 60: //bolsa porta objetos
                $selectC = $this->getColor('060', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 61: //promo mascarilla
                $selectC = $this->getColor('061', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('disabled');
                break;
            case 62: //promo
                $selectC = $this->getColor('-', 'disabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 63: //cadena silicona
                $selectC = $this->getColor('063', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 64: //cadena silicona
                $selectC = $this->getColor('064', 'enabled');
                $selectT = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;

        }

        return array('color' => $selectC, 'talla' => $selectT, 'perso' => $txtPers, 'frase' => $selectF, 'opciones' => $selectP);
        //return $selectC;
    }


    /**
     * Obtiene color
     *
     * @param $idColor
     * @param $statusColor
     * @return string
     */
    public function getColor($idColor, $statusColor){
        if ($idColor == '099' || $idColor == '098'){
            $selected = 'Opción';
        }else{
            $selected = 'Color';
        }

        //lamparita estrella
        if ($idColor == '042'){
            $selected = 'Modelo';
        }

        //pegatina puerta exterior/interior
        if ($idColor == '056'){
            $selected = 'Opcion';
        }

        //Gel antiséptico en varios formatos
        if ($idColor == '052'){
            $selected = 'Formato';
        }

        $selectColor  = '<select class="form-control" ' . $statusColor .'>';

        switch ($idColor){
            case "--":
                $selectColor .= '<option selected="selected">--- ---</option>';
                break;
            case "-":
                $selectColor .= '<option selected="selected">-- --</option>';
                break;

            default:
                $datos = DB::table('color')
                    ->where('codigo_producto', '=', $idColor)
                    ->orderBy('codigo_color')
                    ->get();

                if ($idColor == "027"){
                    foreach ($datos as $dato){
                        $selectColor .= '<option selected='. $dato->id_color.'>'.$dato->codigo_color. ' '.$dato->descripcion_color.'</option>';
                    }
                }else{
                    $selectColor .= '<option selected="selected">'.$selected.'</option>';

                    foreach ($datos as $dato){
                        $selectColor .= '<option value='. $dato->id_color.'>'.$dato->codigo_color. ' '.$dato->descripcion_color.'</option>';
                    }
                }
        }

        $selectColor .= '</select>';

        return $selectColor;
    }

    /**
     * getTalla
     *
     * @param $idTalla
     * @param $statusTalla
     * @return string
     */
    public function getTalla($idTalla, $statusTalla){

        $selectTalla  = '<select class="form-control" '. $statusTalla.'>';

        switch ($idTalla){
            case "ropa":
                $selectTalla .= '<option selected="selected">Talla</option>';
                $selectTalla .= '<option value="--- S-M">S-M (36-38)</option>';
                $selectTalla .= '<option value="--- M-L">M-L (40-42)</option>';
                $selectTalla .= '<option value="--- L-XL">L-XL (44-46)</option>';
                break;
            case "pegatina":
                $selectTalla .= '<option selected="selected">Opción</option>';
                $selectTalla .= '<option value="Interior">Interior</option>';
                $selectTalla .= '<option value="Exterior">Exterior</option>';
                break;
            case "silicona":
                $selectTalla .= '<option selected="selected">Tetina/Talla</option>';
                $selectTalla .= '<option value="Silicona Grande">Silicona grande</option>';
                break;
            case "silicona_pequeña":
                $selectTalla .= '<option selected="selected">Tetina/Talla</option>';
                $selectTalla .= '<option value="Silicona Pequeña">Silicona pequeña</option>';
                break;
            case "body":
                //$selectTalla .= '<option selected="selected">Opción</option>';
                $selectTalla .= '<option value="--- manga">Manga</option>';
                $selectTalla .= '<option value="--- corta">Manga Corta</option>';
                $selectTalla .= '<option value="--- larga">Manga Larga</option>';
                break;
            case "-":
                $selectTalla .= '<option selected="selected">- -</option>';
                break;
            case "--":
                $selectTalla .= '<option selected="selected">--- ---</option>';
                break;
            case "---":
                $selectTalla .= '<option selected="selected">-- --</option>';
                break;
            case "zapatillas":
                $selectTalla .= '<option selected="selected">Talla</option>';
                $selectTalla .= '<option value="--- 21">21</option>';
                $selectTalla .= '<option value="--- 22">22</option>';
                break;
            case "zapatillasRosa":
                $selectTalla .= '<option selected="selected">Talla</option>';
                $selectTalla .= '<option value="--- 21">21</option>';
                $selectTalla .= '<option value="--- 22">22</option>';
                break;
            case "todos" :
                $selectTalla .= '<option selected="selected">Tetina/Talla</option>';
                $selectTalla .= '<option value="Latex Pequeña">Latex pequeña</option>';
                $selectTalla .= '<option value="Latex Grande">Latex grande</option>';
                $selectTalla .= '<option value="Silicona Pequeña">Silicona pequeña</option>';
                $selectTalla .= '<option value="Silicona Grande">Silicona grande</option>';
                break;
            case "sujetachupe":
                $selectTalla .= '<option selected="selected">Opción</option>';
                $selectTalla .= '<option value="--- Perso.">Personalizado</option>';
                break;
            case "etiquetas":
                $selectTalla .= '<option selected="selected">Opción</option>';
                $selectTalla .= '<option value="--- Mixto">Mixto</option>';
                $selectTalla .= '<option value="--- Multiuso">Multiuso</option>';
                $selectTalla .= '<option value="--- Textil">Textil</option>';
                break;
            case "chnav":
                $selectTalla .= '<option selected="selected">Opción</option>';
                $selectTalla .= '<option value="--- Rojo">Chupete Rojo - sil.peq</option>';
                $selectTalla .= '<option value="--- Blanco">Chupete Blanco - sil.peq</option>';
                $selectTalla .= '<option value="--- 5+2">5+2 Chupetes - sil.peq</option>';
                $selectTalla .= '<option value="--- 10+5">10+5 Chupetes - sil.peq</option>';
                break;
            case "babnav":
                $selectTalla .= '<option selected="selected">Opción</option>';
                $selectTalla .= '<option value="--- Reno">Babero Reno</option>';
                $selectTalla .= '<option value="--- Galleta">Babero Galleta</option>';
                $selectTalla .= '<option value="--- Noel">Babero Noel</option>';
                break;
            case 'frase_bordado':
                $selectTalla .= '<option selected="selected">Opción</option>';
                $selectTalla .= '<option value="--- Corazon-Rosa">Corazón Rosa</option>';
                $selectTalla .= '<option value="--- Corazon-Azul">Corazón Azul</option>';
                $selectTalla .= '<option value="--- Corona-Rosa">Corona Rosa</option>';
                $selectTalla .= '<option value="--- Corona-Azul">Corona Azul</option>';
                $selectTalla .= '<option value="--- Estrellas-Azul">Estrellas Azul</option>';
                $selectTalla .= '<option value="--- Estrellas-Rosa">Estrellas Rosa</option>';
                $selectTalla .= '<option value="--- Estrellas-Camel">Estrellas Camel</option>';
                $selectTalla .= '<option value="--- Principe">Príncipe</option>';
                $selectTalla .= '<option value="--- Princesa">Princesa</option>';
                $selectTalla .= '<option value="--- Nube-Azul">Nube Colorete Azul</option>';
                $selectTalla .= '<option value="--- Nube-Rosa">Nube Colorete Rosa</option>';
                break;
            case 'capa_bano':
                $selectTalla .= '<option selected="selected">Opción</option>';
                $selectTalla .= '<option value="--- Corazon-Rosa">Corazón Rosa</option>';
                $selectTalla .= '<option value="--- Corazon-Azul">Corazón Azul</option>';
                $selectTalla .= '<option value="--- Corona-Rosa">Corona Rosa</option>';
                $selectTalla .= '<option value="--- Corona-Azul">Corona Azul</option>';
                $selectTalla .= '<option value="--- Estrellas-Azul">Estrellas Azul</option>';
                $selectTalla .= '<option value="--- Estrellas-Rosa">Estrellas Rosa</option>';
                $selectTalla .= '<option value="--- Principe">Príncipe</option>';
                $selectTalla .= '<option value="--- Princesa">Princesa</option>';
                $selectTalla .= '<option value="--- Nube-Azul">Nube Colorete Azul</option>';
                $selectTalla .= '<option value="--- Nube-Rosa">Nube Colorete Rosa</option>';
                break;
            case '023':
                $selectTalla .= '<option selected="selected">Tetina/Talla</option>';
                $selectTalla .= '<option value="Latex Pequeña">Latex pequeña</option>';
                $selectTalla .= '<option value="Latex Grande">Latex grande</option>';
                $selectTalla .= '<option value="Silicona Grande">Silicona grande</option>';
                break;
            case 'mochila':
            case 'talega':
                $selectTalla .= '<option selected="selected">Diseño</option>';
                $selectTalla .= '<option value="--- Perro-pañuelo">Perro pañuelo</option>';
                $selectTalla .= '<option value="--- Mono">Mono</option>';
                $selectTalla .= '<option value="--- Conejito">Conejito</option>';
                $selectTalla .= '<option value="--- Zorro">Zorro</option>';
                $selectTalla .= '<option value="--- Pulpo">Pulpo</option>';
                $selectTalla .= '<option value="--- Pollo">Pollo</option>';
                $selectTalla .= '<option value="--- Cerdo">Cerdo</option>';
                $selectTalla .= '<option value="--- Gato">Gato</option>';
                $selectTalla .= '<option value="--- Oveja">Oveja</option>';
                $selectTalla .= '<option value="--- Vaca">Vaca</option>';
                $selectTalla .= '<option value="--- Leon">León</option>';
                $selectTalla .= '<option value="--- Koala">Koala</option>';
                $selectTalla .= '<option value="--- Panda">Panda</option>';
                $selectTalla .= '<option value="--- Buho">Búho</option>';
                $selectTalla .= '<option value="--- Rino">Rinoceronte</option>';
                $selectTalla .= '<option value="--- Hada">Hada</option>';
                $selectTalla .= '<option value="--- Sirena">Sirena</option>';
                $selectTalla .= '<option value="--- Unicornio">Unicornio</option>';
                $selectTalla .= '<option value="--- Unicornio-gafas">Unicornio gafas</option>';
                $selectTalla .= '<option value="--- Llama">Llama</option>';
                $selectTalla .= '<option value="--- Bambi">Bambi</option>';
                $selectTalla .= '<option value="--- Jirafa">Jirafa</option>';
                $selectTalla .= '<option value="--- Cangrejo">Cangrejo</option>';
                $selectTalla .= '<option value="--- Cocodrilo">Cocodrilo</option>';
                $selectTalla .= '<option value="--- Llama-gafa">Llama gafa</option>';
                $selectTalla .= '<option value="--- Nube">Nube</option>';
                $selectTalla .= '<option value="--- Naranja">Naranja</option>';
                $selectTalla .= '<option value="--- Fresa">Fresa</option>';
                $selectTalla .= '<option value="--- Pera">Pera</option>';
                $selectTalla .= '<option value="--- Sandia">Sandía</option>';
                break;
            case 'mascarillaAdulto':
                $selectTalla .= '<option selected="selected">Diseño</option>';
                $selectTalla .= '<option value="--- Sin-diseño">Sin diseño</option>';
                $selectTalla .= '<option value="--- Perro-pañuelo">Perro pañuelo</option>';
                $selectTalla .= '<option value="--- Mono">Mono</option>';
                $selectTalla .= '<option value="--- Conejito">Conejito</option>';
                $selectTalla .= '<option value="--- Zorro">Zorro</option>';
                $selectTalla .= '<option value="--- Pulpo">Pulpo</option>';
                $selectTalla .= '<option value="--- Pollo">Pollo</option>';
                $selectTalla .= '<option value="--- Cerdo">Cerdo</option>';
                $selectTalla .= '<option value="--- Gato">Gato</option>';
                $selectTalla .= '<option value="--- Oveja">Oveja</option>';
                $selectTalla .= '<option value="--- Vaca">Vaca</option>';
                $selectTalla .= '<option value="--- Leon">León</option>';
                $selectTalla .= '<option value="--- Koala">Koala</option>';
                $selectTalla .= '<option value="--- Panda">Panda</option>';
                $selectTalla .= '<option value="--- Buho">Búho</option>';
                $selectTalla .= '<option value="--- Rino">Rinoceronte</option>';
                $selectTalla .= '<option value="--- Hada">Hada</option>';
                $selectTalla .= '<option value="--- Sirena">Sirena</option>';
                $selectTalla .= '<option value="--- Unicornio">Unicornio</option>';
                $selectTalla .= '<option value="--- Unicornio-gafas">Unicornio gafas</option>';
                $selectTalla .= '<option value="--- Llama">Llama</option>';
                $selectTalla .= '<option value="--- Bambi">Bambi</option>';
                $selectTalla .= '<option value="--- Jirafa">Jirafa</option>';
                $selectTalla .= '<option value="--- Cangrejo">Cangrejo</option>';
                $selectTalla .= '<option value="--- Cocodrilo">Cocodrilo</option>';
                $selectTalla .= '<option value="--- Llama-gafa">Llama gafa</option>';
                $selectTalla .= '<option value="--- Nube">Nube</option>';
                $selectTalla .= '<option value="--- Naranja">Naranja</option>';
                $selectTalla .= '<option value="--- Fresa">Fresa</option>';
                $selectTalla .= '<option value="--- Pera">Pera</option>';
                $selectTalla .= '<option value="--- Sandia">Sandía</option>';
                break;
            case 'bolsa':
                $selectTalla .= '<option selected="selected">Diseño</option>';
                $selectTalla .= '<option value="--- Sin-diseño">Sin diseño</option>';
                $selectTalla .= '<option value="--- Perro-pañuelo">Perro pañuelo</option>';
                $selectTalla .= '<option value="--- Mono">Mono</option>';
                $selectTalla .= '<option value="--- Conejito">Conejito</option>';
                $selectTalla .= '<option value="--- Zorro">Zorro</option>';
                $selectTalla .= '<option value="--- Pulpo">Pulpo</option>';
                $selectTalla .= '<option value="--- Pollo">Pollo</option>';
                $selectTalla .= '<option value="--- Cerdo">Cerdo</option>';
                $selectTalla .= '<option value="--- Gato">Gato</option>';
                $selectTalla .= '<option value="--- Oveja">Oveja</option>';
                $selectTalla .= '<option value="--- Vaca">Vaca</option>';
                $selectTalla .= '<option value="--- Leon">León</option>';
                $selectTalla .= '<option value="--- Koala">Koala</option>';
                $selectTalla .= '<option value="--- Panda">Panda</option>';
                $selectTalla .= '<option value="--- Buho">Búho</option>';
                $selectTalla .= '<option value="--- Rino">Rinoceronte</option>';
                $selectTalla .= '<option value="--- Hada">Hada</option>';
                $selectTalla .= '<option value="--- Sirena">Sirena</option>';
                $selectTalla .= '<option value="--- Unicornio">Unicornio</option>';
                $selectTalla .= '<option value="--- Unicornio-gafas">Unicornio gafas</option>';
                $selectTalla .= '<option value="--- Llama">Llama</option>';
                $selectTalla .= '<option value="--- Bambi">Bambi</option>';
                $selectTalla .= '<option value="--- Jirafa">Jirafa</option>';
                $selectTalla .= '<option value="--- Cangrejo">Cangrejo</option>';
                $selectTalla .= '<option value="--- Cocodrilo">Cocodrilo</option>';
                $selectTalla .= '<option value="--- Llama-gafa">Llama gafa</option>';
                $selectTalla .= '<option value="--- Nube">Nube</option>';
                $selectTalla .= '<option value="--- Naranja">Naranja</option>';
                $selectTalla .= '<option value="--- Fresa">Fresa</option>';
                $selectTalla .= '<option value="--- Pera">Pera</option>';
                $selectTalla .= '<option value="--- Sandia">Sandía</option>';
                break;
            default:
                $selectTalla .= '<option selected="selected">Tetina/Talla</option>';
        }

        $selectTalla .= '</select>';

        return $selectTalla;
    }

    /**
     * Crear Select correspondiente a la talla dependiendo del color seleccionado
     *
     * @param Request $request
     * @return array
     */
    public function selectTalla(Request $request){
        $selectFrase = 'false';

        //dd($request->id);

        switch ($request->id){
            //Chupete 023
            case 14:
                $selectTalla = $this->getTalla('023', 'enabled');
                $txtPers = $this->getPerso('disabled');
                break;
            /**
             * promoción especial navidad
             */
            case 102:
                $selectTalla = $this->getTalla('chnav', 'enabled');
                $txtPers = $this->getPerso('disabled');
                break;
            case 103:
                $selectTalla = $this->getTalla('babnav', 'enabled');
                $txtPers = $this->getPerso('disabled');
                break;
            /**
             * 80: te quiero mama
             * 21: principe
             * 79: princesa
             */
            case 80:
            case 21:
            case 79:
                $selectTalla = $this->getTalla('silicona', 'enabled');
                $txtPers = $this->getPerso('disabled');
                break;
            case 25: //corona azul
            case 26: //corona rosa
                $selectTalla = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('disabled');
                break;
            //Cadena
            case 82: //premium 01
            case 83: //premium 02
            case 84: //premium 03
            case 101: //premium 04
            case 22: //017 blanca
            case 23: //018 rosa
            case 24: //019 azul
            // Cepillo y peine
            case 27: //rosa
            case 28: //azul
            case 29: //biberon verde
            case 30: //cepillo dientes morado
            case 75: //cepillo dientes azul
            case 31: //caja portachupetes rosa
            case 32: //caja portachupetes azul
            case 33: //caja portachupetes blanca
            case 34: //sonajero campanas rosa
            case 35: //sonajero campanas azul
            case 145: //sonajero campanas mint
            case 49: //quitababas estrellas rosa
            case 50: //quitababas estrellas celeste
            case 51: //bolsa merienda rosa
            case 52: //bolsa merienda celeste
            case 55: //bolsa cambio celeste
            case 56: //bolsa cambio rosa
            case 57: //conjunto gorro + manoplas rosa
            case 58: //conjunto gorro + manoplas azul
            case 74: //frases body
                //Vajilla, vaso y cubiertos
            case 115: //unicornio
            case 116: //zoo
            case 117: //banderines
            case 118: //blonda azul
            case 119: //blonda rosa
            case 128: //lamparita globo
            case 129: //lamparita corazones
            case 130: //lamparita mariposas
            case 131: //lamparita luna
            case 132: //lamparita estrellas
            case 171: //cojin globo rosa
            case 172: //cojin globo azul
            case 173: //cojin juguetero rosa
            case 174: //cojin juguetero azul
            case 175: //cojin tipi rosa
            case 176: //cojin tipi azul
            case 177: //vajilla globo rosa
            case 178: //vajilla globo azul
            case 179: //vajilla juguetero rosa
            case 180: //vajilla juguetero azul
            case 181: //vajilla tipi rosa
            case 182: //vajilla tipi azul

            case 183: //cadena 053 Rojo
            case 184: //cadena 054 Fucsia
            case 185: //cadena 055 Turquesa
            case 186: //cadena 056 Morado
            case 187: //cadena 057 Narjanja
            case 188: //cadena 058 pistacho
            case 189: //cadena 059 marino
            case 190: //cadena 060 beige
            case 191: //cadena 061 amarillo
            case 192: //cadena 062 verde
            case 195: //cadena 065 mint
            case 201: //cadena silicona rosa
            case 202: //cadena silicona azul
            case 203: //cadena silicona mint

            case 204:
            case 205:
            case 206:

                $selectTalla = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
                // Baberos con frase
            case 65:
            case 90:
            case 88:
            case 87:
            case 86:
            case 85:
            case 72:
            case 71:
            case 70:
            case 69:
            case 68:
            case 67:
            case 66:
            case 91:
                $selectTalla = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('disabled');
                break;
            //Camiseta premamá
            case 59:
            case 60:
                $selectTalla = $this->getTalla('ropa', 'enabled');
                $txtPers = $this->getPersoHidden();
                $selectFrase = $this->getFrase();
                break;
            case 94: //body personalizado blanco
                $selectTalla = $this->getTalla('body', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 95: //zapatillas azul
                $selectTalla = $this->getTalla('zapatillas', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 96: //zapatillas rosa
                $selectTalla = $this->getTalla('zapatillasRosa', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 99: //etiquetas
            case 100:
            case 162:
            case 163:
            case 164:
            case 165:
            case 166:
            case 167:
            case 168:
            case 169:
            case 170:
                $selectTalla = $this->getTalla('etiquetas', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 53:
            case 54:
                $selectTalla = $this->getTalla('sujetachupe', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case '-- --':
                $selectTalla = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case '--- ---':
                $selectTalla = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('disabled');
                break;
            case 73:    //bordado body
                $selectTalla = $this->getTalla('frase_bordado', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 122:case 123:case 200: //capa baño
                $selectTalla = $this->getTalla('capa_bano', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 120:
            case 124: //mochila
            case 125:
            case 133:
            case 135:
            case 136:
                $selectTalla = $this->getTalla('mochila', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 121:
            case 126:
            case 127:
            case 140:
            case 141:
            case 142:
            case 146:
            case 147:
                $selectTalla = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 133:
                $selectTalla = $this->getTalla('talega', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 151: //gel antiseptico 110ml
            case 152: //gel antiseptico 250ml
                $selectTalla = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('disabled');
                break;
            case 153: //pegatina distancia interior
            case 154: //pegatina distancia exterior
                $selectTalla = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 155: //mascarilla infantil
            case 156:
            case 157:
            case 158:
            case 159: //mascarilla adultomochila
            case 160:
                $selectTalla = $this->getTalla('mascarillaAdulto', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 161: // talega objetos
                $selectTalla = $this->getTalla('bolsa', 'enabled');
                $txtPers = $this->getPerso('enabled');
                break;
            case 196: //promo mascarilla 5 uds rosa y celeste
            case 197:
            case 198:
            case 199:
                $selectTalla = $this->getTalla('--', 'disabled');
                $txtPers = $this->getPerso('disabled');
                break;
            default:
                $selectTalla = $this->getTalla('todos', 'enabled');
                $txtPers = $this->getPerso('enabled');
        }

        return array('talla' => $selectTalla, 'perso' => $txtPers, 'frase' => $selectFrase);
    }

    /**
     * txtPersonalización
     *
     * @param $status
     * @return string
     */
    function getPerso($status){

        $personalizado = '<input id="personalizado" class="form-control" placeholder="Grabación" type="text"'.$status.'>';

        return $personalizado;
    }

    /**
     * Ocultar txtPersonalización para la frase
     * @return string
     */
    function getPersoHidden(){
        $personalizado = '<input id="personalizado" class="form-control" placeholder="Grabación" type="text" disabled>';

        return $personalizado;
    }

    /**
     * @return string
     */
    function getFrase(){
        $selectFrase  = '<select class="form-control" id="frases">';
        $selectFrase .= '<option selected="selected">Frase</option>';
        $selectFrase .= '<option value="Aquí empieza todo">Aquí empieza todo</option>';
        $selectFrase .= '<option value="Hecho con mogollón de amor">Hecho con mogollón de amor</option>';
        $selectFrase .= '<option value="Hecha con mogollón de amor">Hecha con mogollón de amor</option>';
        $selectFrase .= '<option value="Prometo ser buena">Prometo ser buena</option>';
        $selectFrase .= '<option value="Prometo ser bueno">Prometo ser bueno</option>';
        $selectFrase .= '<option value="Tengo dos corazones">Tengo dos corazones</option>';

        $selectFrase .= '</select>';

        return $selectFrase;
    }

    /**
     * Frases body
     *
     * @return string
     */
    public function getFraseBody(){
        $selectFraseBody  = '<select class="form-control" id="frases">';
        $selectFraseBody .= '<option selected="selected">Frase</option>';
        $selectFraseBody .= '<option value="Prometo ser buena">Prometo ser buena</option>';
        $selectFraseBody .= '<option value="Prometo ser bueno">Prometo ser bueno</option>';
        $selectFraseBody .= '<option value="Hecho con mogollón de amor">Hecho con mogollón de amor</option>';
        $selectFraseBody .= '<option value="Hecha con mogollón de amor">Hecha con mogollón de amor</option>';
        $selectFraseBody .= '<option value="Mi tía mola mucho">Mi tía mola mucho</option>';
        $selectFraseBody .= '<option value="Te reto a no sonreir cuando me mires">Te reto a no sonreir cuando me mires</option>';
        $selectFraseBody .= '<option value="Yo no digo ni pío">Yo no digo ni pío</option>';

        return $selectFraseBody;
    }

    /**
     * Select dibujo pegatina bebe a bordo
     *
     * @return string
     */
    public function getDibujo(){
        $selectDibujo  = '<select class="form-control Sdibujo" id="dibujo">';
        $selectDibujo .= '<option selected="selected">Dibujo</option>';
        $selectDibujo .= '<option value="Niño">Niño</option>';
        $selectDibujo .= '<option value="Niña">Niña</option>';
        $selectDibujo .= '<option value="Niño/Niña">Niño/Niña</option>';
        $selectDibujo .= '<option value="Niño/Niño">Niño/Niño</option>';
        $selectDibujo .= '<option value="Niña/Niña">Niña/Niña</option>';

        $selectDibujo .= '</select>';

        return $selectDibujo;
    }

    /**
     * Select dibujo pegatina bebe a bordo
     *
     * @return string
     */
    public function getOpcion(){
        $selectOpcional  = '<select class="form-control Sopcion" id="opcion">';
        $selectOpcional .= '<option selected="selected">Opcion</option>';
        $selectOpcional .= '<option value="Interior">Interior</option>';
        $selectOpcional .= '<option value="Exterior">Exerior</option>';


        $selectOpcional .= '</select>';

        return $selectOpcional;
    }
}
