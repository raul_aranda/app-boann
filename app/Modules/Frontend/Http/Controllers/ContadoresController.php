<?php

namespace App\Modules\Frontend\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ContadoresController extends Controller
{
    /**
     * ContadoresController constructor.
     */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Contador líneas de productos
     *
     * @return mixed
     */
    public function lineas(){
        $codigo = Auth::user()->codigo_cliente;

        $lineas = DB::table('pedidostmps')
            ->where('id_cliente', $codigo)
            ->count();

        return $lineas;
    }

    /**
     * Contador pedidos
     *
     * @return mixed
     */
    public function pedidos(){
        $codigo = Auth::user()->codigo_cliente;

        $pedidos = DB::table('pedidos')
            ->where('id_cliente', $codigo)
            ->count();

        return $pedidos;
    }
}
