<?php

namespace App\Modules\Frontend\Http\Controllers;

use App\Color;
use App\Modules\Dropshipping\Models\dropshipping;
use App\Modules\Stock\Models\Stock;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Pedidos;
use App\Pedidosoks;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class AddPedidosController extends Controller
{
    /**
     * @param Request $request
     */
    function addRecords (Request $request){
        $codigo = Auth::user()->codigo_cliente;
        $now = new \DateTime();

        $add = new Pedidostmps();
        $add->id_producto   = $request->producto;
        $add->cantidad      = $request->cantidad;
        $add->id_color      = $request->color;
        $add->tetina        = $request->tetina;
        $add->talla         = $request->talla;
        $add->nombre        = $request->nombre;
        $add->fecha         = $now;
        $add->id_cliente    = $codigo;

        $add->save();
    }

    /**
     * @param Request $request
     * @return string
     */
    function addOrder(Request $request){
        $codigo = Auth::user()->codigo_cliente;
        $now = new \DateTime();

        $records = \App\Pedidostmps::where('id_cliente', '=', $codigo)->get();

        if (count($records) == 0){
            return "vacio";
        }

        $contador = 0;

        $addOrder = new Pedidos();

        $addOrder->fecha        = $now;
        $addOrder->num_art      = 0;
        $addOrder->id_cliente   = $codigo;
        $addOrder->comentarios  = $request->comentario;
        $addOrder->codportes    = $request->codEnvio;

        $addOrder->save();
        $id_order = $addOrder->id;

        foreach ($records as $record){
            $pedido = new Pedidosoks();

            $pedido->id_producto    = $record->id_producto;
            $pedido->cantidad       = $record->cantidad;
            $pedido->id_color       = $record->id_color;
            $pedido->tetina         = $record->tetina;
            $pedido->talla          = $record->talla;
            $pedido->nombre         = $record->nombre;
            $pedido->id_cliente     = $codigo;
            $pedido->fecha          = $now;
            $pedido->id_pedidook    = $id_order;
            $pedido->modelo         = $record->modelo;

            $pedido->save();

            $borrado = \App\Pedidostmps::where('id_pedido', '=', $record->id_pedido)
            ->delete();

            $contador++;

            //----------------------------------------------------------------------------------------------------------
            // Control de Stock llamamiento function
            //----------------------------------------------------------------------------------------------------------
            $this->controlStock($record);
        }

        $updateOrder = \App\Pedidos::find($id_order);
        $updateOrder->num_art = $contador;

        $updateOrder->save();

        //--------------------------------------------------------------------------------------------------------------
        // En caso de que esté activo la solicitud del catálogo
        //--------------------------------------------------------------------------------------------------------------
        if($_ENV['CATALOGO_ACTIVO'] == 'true'){
            $catalogo = \App\Modules\Catalogo\Http\Controllers\CatalogoController::compruebaCatalogoCliente($codigo);

            if ($catalogo == 0){
                $catalogo = new Pedidosoks();

                $catalogo->id_producto  = 29;
                $catalogo->cantidad     = 1;
                $catalogo->id_color     = 0;
                $catalogo->tetina       = '-';
                $catalogo->talla        = '-';
                $catalogo->nombre       = '-';
                $catalogo->id_cliente   = $codigo;
                $catalogo->fecha        = $now;
                $catalogo->id_pedidook  = $id_order;
                $catalogo->save();

                $guardaCatalogo = \App\Modules\Catalogo\Http\Controllers\CatalogoController::guardaCatalogoCliente($codigo);
            }
        }
        // fin catálogo activo------------------------------------------------------------------------------------------

        //Session::push('id_order', $id_order);
        session()->put('id_order', $id_order);

        // Cliente dropshipping
        if (session()->has('codigoClienteDrop')){
            $drop = new dropshipping();
            $drop->idCliente = Session::get('codigoClienteDrop');
            $drop->idPedido = $id_order;
            $drop->idTienda =  Auth::user()->codigo_cliente;

            $drop->save();

            session()->forget('codigoClienteDrop');
        }

        return Redirect::to('/sendmail');
    }

    /**
     * Function control de stock
     *
     * @param $record
     */
    public function controlStock($record){
        $id = $record->id_producto;

        //--------------------------------------------------------------------------------------------------------------
        // $id -> 0  = chupete color
        // $id -> 1  = chupete normal
        // $id -> 32 = caja regalo 01
        // $id -> 33 = caja regalo 02
        // $id -> 10 = pack premium personalizado
        // $id -> 11 = conjunto chupete y cadena
        //--------------------------------------------------------------------------------------------------------------

        if ($id == 1 || $id == 0 || $id == 32 || $id == 33 || $id == 11 || $id == 10){
            $color = Color::find($record->id_color);
            
            $stockTable = Stock::where('color', 'like', '%' . $color->descripcion_color)
                ->where('tetina', '=', $record->tetina . ' ' . $record->talla)
                ->first();

            // TODO envio mail control stock y telegram
            if ($stockTable){
                $idStock = $stockTable->id;

                $stock = Stock::find($idStock);

                $stock->salida = $stock->salida + intval($record->cantidad);
                $stock->stock = $stock->stock - intval($record->cantidad);
                $stock->save();
            }
        }
    }
}
