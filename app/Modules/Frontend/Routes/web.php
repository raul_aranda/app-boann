<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'frontend', 'midleware' =>'auth'], function () {
    Route::get('/', function () {
        dd('This is the Frontend module index page. Build something great!');
    });

    Route::get('/pedidos', 'PedidosController@pedidoTemporal');
    Route::get('/createSelect', 'CreateSelectController@index')->name('createSelectColor');
    Route::get('/selectTalla', 'CreateSelectController@selectTalla')->name('selectTalla');
    Route::get('/addLinea', 'AddLineaController@index')->name('add.linea');

    //Contadores
    Route::get('/contlineas', 'ContadoresController@lineas')->name('lineas.contador');
    Route::get('/contpedidos', 'ContadoresController@pedidos')->name('pedidos.contador');

    Route::get('/addPedidos', 'AddPedidosController@addOrder');

    //Guardar datos cliente
    Route::get('/guardaComercio', 'SaveComercioController@index');




    Route::get('/createTalla', 'CreateSelectController@createTalla')->name('createTalla');
    Route::get('/createGrabacion', 'CreateSelectController@createGrabacion');

    Route::get('/gastosEnvio', 'GastosEnvioController@index');
});

