<?php

namespace App\Modules\Articulos\Models;

use Illuminate\Database\Eloquent\Model;

class Articulos extends Model
{
    protected $table = 'productos';
}
