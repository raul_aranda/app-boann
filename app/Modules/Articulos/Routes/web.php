<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'articulos', 'midleware' => 'auth'], function () {
    /*Route::get('/', function () {
        dd('This is the Articulos module index page. Build something great!');
    });*/
    Route::Resource('/articulos', 'ArticulosController');
    Route::get('/activate/{id}', 'ArticulosController@activate')->name('articulos.activate');
    Route::get('/deactivate/{id}', 'ArticulosController@deactivate')->name('articulos.deactivate');

    Route::get('/datatable', 'ArticulosTableController@index')->name('datatable.articulos');
});
