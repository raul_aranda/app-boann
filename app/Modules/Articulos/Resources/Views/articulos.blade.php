@extends('admin.layouts.back')

@section('after-styles')
    {{ Html::style("css/backend/plugin/datatables/jquery.dataTables.min.css") }}
    <style type="text/css">
        table.dataTable tbody th, table.dataTable tbody td {
            padding: 8px 4px;
        }
        .centrarTexto{
            text-align: center;
        }
        .caja{
            width: 50px;
            text-align: center;
        }
    </style>
@stop

@section('page-header')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../index.html">Home</a>
        </li>
        <li class="breadcrumb-item active">Artículos</li>
    </ol>
    <h1 class="page-title">
        Listado de Artículos
    </h1>
    <p class="page-description">Artículos</p>
    <div class="page-header-actions">

    </div>
@endsection

@section('content')
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Listado de Artículos</h3>
        </div><!-- /.box-header -->
        <div class="panel-body">
            <div class="table-responsive">
                <table id="articulos-table" class="table table-condensed table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Descripción</th>
                            <th>Código</th>
                            <th>Orden</th>
                            <th>Referencia</th>
                            <th>Activo</th>
                            <th>Precio</th>
                            <th style="text-align: center">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
    {{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}


    <script>
        $(function() {
            $('#articulos-table').DataTable({
                "language":
                    {
                        "sProcessing":     "Procesando...",
                        "sLengthMenu":     "Mostrar _MENU_ ",
                        "sZeroRecords":    "No se encontraron resultados",
                        "sEmptyTable":     "Ningún dato disponible en esta tabla",
                        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix":    "",
                        "sSearch":         "Buscar:",
                        "sUrl":            "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("datatable.articulos") }}',
                    type: 'get',
                    data: {status: 1, trashed: false}
                },
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'descripcion', name: 'descripcion' },
                    { data: 'codigo', name: 'codigo' },
                    { data: 'orden', name: 'orden',  searchabe: false, sClass: 'centrarTexto'},
                    { data: 'ref', name: 'ref' },
                    { data: 'active', name: 'active', sClass: 'centrarTexto' },
                    { data: 'precio', name: 'precio', sClass: 'centrarTexto'},
                    { data: 'action', name: 'action', orderable: false, searchable: false, sClass: 'centrarTexto'}
                ],
                order: [[3, "asc"]],
                searchDelay: 500
            });
        });
    </script>
@stop