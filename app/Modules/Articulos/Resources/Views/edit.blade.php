@extends ('admin.layouts.back')

@section('title', 'Administración de Artículos')
@section('after-styles')
@stop

@section('page-header')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../index.html">Home</a>
        </li>
        <li class="breadcrumb-item active">Artículos</li>
    </ol>
    <h1 class="page-title">
        Modificación de Artículo
    </h1>
    <div class="page-header-actions">

    </div>
@endsection

@section('content')
    {{ Form::model($articulo, ['route' => ['articulos.update', $articulo], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id'=>'form_user']) }}
    <div class="panel">
        <div class="panel heading">
            <h3 class="panel-title">Modificar Artículo</h3>
        </div><!-- /.box-header -->

        <div class="panel-body">
            <div class="row">
                <div class="form-group">
                    {{ Form::label('descripcion', 'Descripción', ['class' => 'col-md-2 control-label']) }}

                    <div class="col-md-10">
                        {{ Form::text('descripcion', $articulo->descripcion, ['class' => 'form-control', 'placeholder' => 'Descripción']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div>

            <div class="row">
                <div class="form-group">
                    {{ Form::label('codigo', 'Código', ['class' => 'col-md-2 control-label']) }}

                    <div class="col-md-2">
                        {{ Form::text('codigo', $articulo->codigo, ['class' => 'form-control', 'placeholder' => 'Código Artículo' ]) }}
                    </div><!--col-lg-10-->

                    {{ Form::label('ref', 'Referencia', ['class' => 'col-md-1 control-label']) }}

                    <div class="col-md-2">
                        {{ Form::text('ref', $articulo->ref,  ['class' => 'form-control', 'placeholder' => 'Referencia']) }}
                    </div>

                    {{ Form::label('precio', 'Precio', ['class' => 'col-md-1 control-label']) }}

                    <div class="col-md-2">
                        {{ Form::text('precio', $articulo->precio,  ['class' => 'form-control', 'placeholder' => 'Precio']) }}
                    </div>

                </div><!--form control-->

            </div>
        </div>
        <div class="panel-footer">
            <div class="pull-left">
                {{ link_to_route('articulos.index', 'Cancelar', [], ['class' => 'btn btn-danger']) }}
            </div><!--pull-left-->

            <div class="pull-right">
                {{ Form::submit('Actualizar', ['class' => 'btn btn-success']) }}
            </div><!--pull-right-->

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div>
    </form>
@stop

@section('after-scripts')
@stop