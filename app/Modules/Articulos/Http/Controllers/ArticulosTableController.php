<?php

namespace App\Modules\Articulos\Http\Controllers;

use App\Modules\Articulos\Models\Articulos;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;

class ArticulosTableController extends Controller
{
    public function index(){
        $articulos = DB::table('productos')
            ->get();

        return Datatables::of($articulos)
            ->addColumn('orden', function ($articulos){
                return '<input type="text" value="'. $articulos->orden.'"class="caja" disabled >';
            })
            ->addColumn('active', function ($articulos) {
                if ($articulos->active == 0){
                    return '<label class="label label-danger">No</label>';
                }else{
                    return '<label class="label label-success">Sí</label>';
                }
            })
            ->addColumn('action', function ($articulos){
                $buttons  =  '<a href="'.route('articulos.edit', $articulos->id).'" class="btn btn-xs btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Modificar"></i></a> ';

                if ($articulos->active == 0){
                    $buttons .=  '<a href="'.route('articulos.activate', $articulos->id).'" class="btn btn-xs btn-success"><i class="fa fa-play" data-toggle="tooltip" data-placement="top" title="Activar"></i></a> ' ;
                }else{
                    $buttons .=  '<a href="'.route('articulos.deactivate', $articulos->id).'" class="btn btn-xs btn-warning"><i class="fa fa-pause" data-toggle="tooltip" data-placement="top" title="Desactivar"></i></a> ';
                }

                return $buttons;
            })
            ->make(true);
    }
}
