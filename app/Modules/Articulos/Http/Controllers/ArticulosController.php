<?php

namespace App\Modules\Articulos\Http\Controllers;

use App\Modules\Articulos\Models\Articulos;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ArticulosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articulos = DB::table('productos')
            ->orderBy('orden', 'asc')
            ->paginate(10);
        return view('articulos::articulos', ['data' => $articulos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $articulo = Articulos::find($id);

        return view('articulos::edit', compact('articulo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $articulo = Articulos::find($id);

        $articulo->descripcion  = $request->descripcion;
        $articulo->codigo       = $request->codigo;
        $articulo->orden        = $request->orden;
        $articulo->ref          = $request->ref;
        $articulo->precio       = $request->precio;

        $articulo->save();

        return redirect()->route('articulos.index')->withFlashSuccess('Artículo modificado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $id
     * @return mixed
     */
    public function activate($id){
        $articulo = Articulos::find($id);

        $articulo->active = 1;
        $articulo->save();

        return redirect()->route('articulos.index')->withFlashSuccess('Artículo activado');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deactivate($id){
        $articulo = Articulos::find($id);

        $articulo->active = 0;
        $articulo->save();

        return redirect()->route('articulos.index')->withFlashSuccess('Artículo desactivado');
    }
}
