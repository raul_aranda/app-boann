<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStocks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stock', function (Blueprint $table) {
            $table->increments('id');
            $table->string('articulo', 60);
            $table->string('color', 60);
            $table->string('tetina');
            $table->integer('stock')->default(0);
            $table->integer('entrada')->default(0);
            $table->integer('salida')->default(0);
            $table->integer('minimo')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stock', function (Blueprint $table) {
            Schema::dropIfExists('stock');
        });
    }
}
