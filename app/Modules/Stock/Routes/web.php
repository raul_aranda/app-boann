<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'stock'], function () {
    Route::get('/', function () {
        dd('This is the stock module index page. Build something great!');
    });

    Route::resource('/stock', 'StockController');

    // Filtro de stock
    Route::post('/filtro', 'StockController@index')->name('filtro.stock');

    // Filtro excel
    Route::post('/excel', 'StockController@createExcel')->name('create.excel');

    Route::get('/vue', 'StockController@vue')->name('view.vue');
    Route::get('/stockage', 'StockTableController@index')->name('stock.controller');

    Route::get('/almacen', 'StockController@almacen')->name('control.almacen');
    Route::get('/actualizaAlmacen', 'StockController@actualizaAlmacen');
});
