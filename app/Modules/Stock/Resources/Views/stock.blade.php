@extends('admin.layouts.back')

@section('after-styles')
    {{ Html::style("/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css") }}
@stop

@section('page-header')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../index.html">Home</a>
        </li>
        <li class="breadcrumb-item active">Stock</li>
    </ol>
    <h1 class="page-title">
        Stockage Chupetes
    </h1>
    <p class="page-description">Stock</p>
    <div class="page-header-actions">
        {{ link_to_route('view.vue', 'Control Almacen', [], ['class' => 'btn btn-primary btn-xs']) }}
    </div>
@endsection

@section('content')
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Stockage Chupetes</h3>
        </div><!-- /.box-header -->
        <div class="panel-toolbar">
            <div class="filtros">
                {!! Form::open(['route' => 'filtro.stock', 'method' => 'post']) !!}
                <div class="container">
                    <div class='col-lg-3'>
                        <div class="form-group">
                            <div class='input-group date' id='datetimepicker6'>
                                {{ Form::text('date1','' , ['class' => 'form-control',  'id' => 'date1' ]) }}
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                            </div>
                        </div>
                    </div>
                    <div class='col-lg-3'>
                        <div class="form-group">
                            <div class='input-group date' id='datetimepicker7'>
                                {{ Form::text('date2', '', ['class' => 'form-control',  'id' => 'date2' ]) }}
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    {{ Form::submit('Filtro', ['class' => 'btn btn-success']) }}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="panel-body">
            {!! Form::open(['url' => '', 'method' => 'post', 'id' => 'listado']) !!}
            <div class="table-responsive">
                @if($dataSet)
                    <table id="users-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>Artículo </th>
                            <th>Color</th>
                            <th>Tetina</th>
                            <th>Cantidades</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($contador = 1)

                        @foreach($dataSet as $row)
                            <tr>
                                <td>
                                </td>
                                <td>{{ $contador }}</td>
                                <td> {{ $row['articulo'] }} </td>
                                <td> <b> {{ $row['codigo_color'] }}</b> {{ $row['descripcion_color'] }}</td>
                                <td>{{ $row['tetina'] }}</td>
                                <td>{{ $row['cantidad'] }}</td>
                            </tr>
                            @php($contador ++)
                        @endforeach
                    </table>
                @endif
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
@section('after-scripts')
    {{ Html::script("/bower_components/moment/min/moment-with-locales.min.js") }}
    {{ Html::script("/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js") }}
    <script type="text/javascript">
        $(function () {
            var d = new Date();
            var month = d.getMonth() +1;
            var day = d.getDate();

            var startDate = '0' + 1 + '-' +
                (month < 10 ? '0' : '') + month + '-' +
                d.getFullYear();

            $('#datetimepicker6').datetimepicker({
                locale: 'es',
                format: 'DD-MM-YYYY'
            });
            $('#date1').val(startDate);

            var output = (day < 10 ? '0' : '') + day + '-' +
                (month < 10 ? '0' : '') + month + '-' +
                d.getFullYear();

            $('#datetimepicker7').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                locale: 'es',
                format: 'DD-MM-YYYY'
            });

            $('#date2').val(output);
        });
    </script>
@stop