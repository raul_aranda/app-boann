@extends('admin.layouts.back')

@section('after-styles')
    {{ Html::style("/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css") }}
    {{ Html::style("css/backend/plugin/datatables/jquery.dataTables.min.css") }}
    <style type="text/css">
        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content/Box */
        .modal-content {
            background-color: #fefefe;
            margin: 15% auto; /* 15% from the top and centered */
            /*padding: 20px;*/
            border: 1px solid #888;
            width: 500px; /* Could be more or less, depending on screen size */
        }

        /* The Close Button */
        .close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }

        .modal-header {
            background-color: #822193;
            border-radius: .286rem .286rem 0 0;
            padding: 15px 20px;
            border-bottom: none;
        }
        #articulo{
            font-weight: bold;
        }
        .label{
            display: inline-block;
            width: 50px;
        }
    </style>
@stop

@section('page-header')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../index.html">Home</a>
        </li>
        <li class="breadcrumb-item active">Stock</li>
    </ol>
    <h1 class="page-title">
        Control de Stock
    </h1>
    <p class="page-description">Almacén</p>
    <div class="page-header-actions">
        {{ link_to_route('stock.index', 'Filtros Stock', [], ['class' => 'btn btn-primary btn-xs']) }}
    </div>

    <div id="myModal" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <div class="modal-header">
                <span class="close" id="closeModal">&times;</span>
                <h5 style="color: white">Control de Stock</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group">
                        {{ Form::label('pwd', 'Artículo', ['class' => 'col-md-3 control-label']) }}

                        <div class="col-md-8">
                            {{ Form::text('articulo', null, ['class' => 'form-control', 'placeholder' => 'Artículo', 'id' => 'articulo', 'disabled' ]) }}
                        </div><!--col-lg-10-->
                    </div><!--form control-->

                </div>
                <div class="row">
                    <div class="form-group">
                        {{ Form::label('stock', 'U. Almacén', ['class' => 'col-md-3 control-label']) }}

                        <div class="col-md-5">
                            {{ Form::text('stock', null, ['class' => 'form-control', 'placeholder' => 'Stock', 'id' => 'stock', 'disabled' ]) }}
                        </div><!--col-lg-10-->
                    </div><!--form control-->

                </div>
                <div class="row">
                    <div class="form-group">
                        {{ Form::label('entrada', 'Entrada Stock', ['class' => 'col-md-3 control-label']) }}

                        <div class="col-md-5">
                            {{ Form::text('entrada', null, ['class' => 'form-control', 'placeholder' => 'Unidades Entrada', 'id' => 'entrada' ]) }}
                        </div><!--col-lg-10-->
                    </div><!--form control-->

                </div>
                <div class="row">
                    <div class="form-group">
                        {{ Form::label('minimo', 'Mínimo Stock', ['class' => 'col-md-3 control-label']) }}

                        <div class="col-md-5">
                            {{ Form::text('minimo', null, ['class' => 'form-control', 'placeholder' => 'Mínimo', 'id' => 'minimo' ]) }}
                        </div><!--col-lg-10-->
                    </div><!--form control-->

                </div>
                <input type="hidden" id="idArticulo" value="">
            </div>
            <div class="row"></div>

            <div class="modal-footer">
                <div class="col-sm-12">
                    <button type="button" id="cancelar" class="btn btn-default btn-pure waves-effect waves-classic" data-dismiss="modal">Cancelar</button>
                    <button type="button" id="guardar" class="btn btn-primary waves-effect waves-classic">Guardar cambios</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Listado Artículos</h3>
        </div><!-- /.box-header -->
        <div class="panel-body">
            <div class="table-responsive">

                    <table id="stock-table" class="table table-condensed table-hover" data-page-length='25'>
                        <thead>
                        <tr>
                            <th></th>
                            <th>Artículo</th>
                            <th>Color</th>
                            <th>Tetina</th>
                            <th>Almacén/Salida/Mínimo</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                    </table>

            </div>
        </div>
    </div>
@endsection
@section('after-scripts')

    {{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}
    <script>
        $(function() {
            $('#stock-table').DataTable({
                "language":
                    {
                        "sProcessing":     "Procesando...",
                        "sLengthMenu":     "Mostrar _MENU_ ",
                        "sZeroRecords":    "No se encontraron resultados",
                        "sEmptyTable":     "Ningún dato disponible en esta tabla",
                        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix":    "",
                        "sSearch":         "Buscar:",
                        "sUrl":            "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("stock.controller") }}',
                    type: 'get',
                    data: {status: 1, trashed: false}
                },
                columns: [
                    { data: 'id', name: 'id', orderable: false, searchable: false},
                    { data: 'articulo', name: 'articulo' },
                    { data: 'color', name: 'color' },
                    { data: 'tetina', name: 'active',  searchabe: false, sClass: 'centrarTexto' },
                    { data: 'stock', name: 'stock' },
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ],
                order: [[0, "asc"]],
                searchDelay: 500
            });
        });

        // Get the modal
        var modal = document.getElementById("myModal");

        // Get the button that opens the modal
        //var btn = document.getElementById("myBtn");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];
        var cierra = document.getElementById("closeModal");
        var cancela = document.getElementById("cancelar");

        // When the user clicks on the button, open the modal
        /*btn.onclick = function() {
            modal.style.display = "block";
        }*/

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        cierra.onclick = function(){
            modal.style.display = "none";
        }
        cancela.onclick = function(){
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }

        function mostrarVentana(id) {
            $("#idUsuario").val(id);
            $.ajax({
                type: 'get',
                url: 'almacen',
                data: { id:id },
                success:function (data) {
                    $("#articulo").val(data.articulo + ' ' + data.color + ' ' + data.tetina);
                    $("#idArticulo").val(data.id);
                    $("#stock").val(data.stock);
                    $("#minimo").val(data.minimo);
                },
                error: function (data) {
                    console.log('error');
                }
            });
            modal.style.display = "block";
        }

        $("#guardar").click(function(){
            var id = $("#idArticulo").val();
            var entrada = $("#entrada").val();
            var minimo = $("#minimo").val();


            if (entrada == ''){
                alert('No se ha introducido ninguna cantidad')
            }else{
                $.ajax({
                    type: 'get',
                    url: 'actualizaAlmacen',
                    data: { id:id, entrada:entrada, minimo:minimo },
                    success:function (data) {
                        $("#entrada").val('');
                        //$("#informa").show();
                        modal.style.display = "none";
                        $('#stock-table').DataTable().ajax.reload();

                        /*setTimeout(function() {
                            $('#informa').fadeOut('slow');
                        }, 3000); // <-- time in milliseconds*/
                    },
                    error: function (data) {
                        console.log('error');
                    }
                });
                //modal.style.display = "none";
            }
        });
    </script>
@stop