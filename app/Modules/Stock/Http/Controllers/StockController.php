<?php

namespace App\Modules\Stock\Http\Controllers;

use App\Modules\Stock\Models\Stock;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Excel;

class StockController extends Controller
{
    /**
     * @var Fecha campo 1
     */
    var $date1;

    /**
     * @var Fecha campo 2
     */
    var $date2;

    /**
     * StockController constructor.
     */
    public function __construct()
    {
        $this->date1 = Carbon::parse('first day of this Month');
        $this->date2 = Carbon::now();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if ($request->date1 != null && $request->date2 != null){
            $this->date1 = Carbon::createFromFormat('d-m-Y', $request->date1);
            $this->date2 = Carbon::createFromFormat('d-m-Y', $request->date2);
        }

        $chupeteSql = $this->consultaSqlChupete('001');

        $dataSet = [];
        foreach ($chupeteSql as $chupete) {

            //Te quiero mama 80, principe 21, princesa 79
            if ($chupete->id_color == 80 || $chupete->id_color == 21 || $chupete->id_color == 79){
                $dataSet[] = [
                    'id_color' => $chupete->id_color,
                    'articulo' => 'Chupete',
                    'descripcion_color' => $chupete->descripcion_color,
                    'codigo_color' => $chupete->codigo_color,
                    'codigo_producto' => $chupete->codigo_producto,
                    'tetina' => 'Silicona Grande',
                    'cantidad' => $this->chupete($chupete->id_color, $this->date1, $this->date2),
                ];
            }else{

                for($i = 0; $i < 4; $i ++){
                    if ($i == 0){
                        $tetina = 'Silicona';
                        $talla = 'Grande';
                    }
                    if ($i == 1){
                        $tetina = 'Silicona';
                        $talla = 'Pequeña';
                    }
                    if ($i == 2){
                        $tetina = 'Latex';
                        $talla = 'Grande';
                    }
                    if ($i == 3){
                        $tetina = 'Latex';
                        $talla = 'Pequeña';
                    }

                    $dataSet[] = [
                        'id_color' => $chupete->id_color,
                        'articulo' => 'Chupete',
                        'descripcion_color' => $chupete->descripcion_color,
                        'codigo_color' => $chupete->codigo_color,
                        'codigo_producto' => $chupete->codigo_producto,
                        'tetina' => $tetina . ' '. $talla,
                        'cantidad' => $this->chupetePedidoOk('001', $chupete->id_color, $tetina, $talla, $this->date1, $this->date2),
                    ];
                }
            }
        }

        //Pack premium
        $premiumSql = $this->consultaSqlChupete('010');

        foreach ($premiumSql as $premium){
            for($i = 0; $i < 4; $i ++) {
                if ($i == 0) {
                    $tetina = 'Silicona';
                    $talla = 'Grande';
                }
                if ($i == 1) {
                    $tetina = 'Silicona';
                    $talla = 'Pequeña';
                }
                if ($i == 2) {
                    $tetina = 'Latex';
                    $talla = 'Grande';
                }
                if ($i == 3) {
                    $tetina = 'Latex';
                    $talla = 'Pequeña';
                }

                $dataSet[] = [
                    'id_color' => $premium->id_color,
                    'articulo' => 'Pack Premium',
                    'descripcion_color' => $premium->descripcion_color,
                    'codigo_color' => $premium->codigo_color,
                    'codigo_producto' => $premium->codigo_producto,
                    'tetina' => $tetina . ' ' . $talla,
                    'cantidad' => $this->chupetePedidoOk('010', $premium->id_color, $tetina, $talla, $this->date1, $this->date2),
                ];
            }
        }

        //Conjunto chupete y cadena
        $chupeteCadenaSql = $this->consultaSqlChupete('011');

        foreach ($chupeteCadenaSql as $chuca){
            for($i = 0; $i < 4; $i ++) {
                if ($i == 0) {
                    $tetina = 'Silicona';
                    $talla = 'Grande';
                }
                if ($i == 1) {
                    $tetina = 'Silicona';
                    $talla = 'Pequeña';
                }
                if ($i == 2) {
                    $tetina = 'Latex';
                    $talla = 'Grande';
                }
                if ($i == 3) {
                    $tetina = 'Latex';
                    $talla = 'Pequeña';
                }

                $dataSet[] = [
                    'id_color' => $chuca->id_color,
                    'articulo' => 'Chupete & Cadena',
                    'descripcion_color' => $chuca->descripcion_color,
                    'codigo_color' => $chuca->codigo_color,
                    'codigo_producto' => $chuca->codigo_producto,
                    'tetina' => $tetina . ' ' . $talla,
                    'cantidad' => $this->chupetePedidoOk('011', $chuca->id_color, $tetina, $talla, $this->date1, $this->date2),
                ];
            }

        }

        return view('stock::stock', compact('dataSet'));
    }

    /**
     * Chupetes especiales
     *
     * @param $id_color
     * @return mixed
     */
    function chupete($id_color, $date1, $date2){
        $data = DB::table('pedidosoks')
            ->where('id_producto', '=', 1)
            ->where('id_color', '=', $id_color)
            ->whereBetween('fecha',[$date1, $date2])
            ->sum('cantidad');

        return $data;
    }

    /**
     * @param $id
     * @param $id_color
     * @param $tetina
     * @param $talla
     * @param $date1
     * @param $date2
     * @return mixed
     */
    function chupetePedidoOk($id, $id_color, $tetina, $talla, $date1, $date2){
        $data = DB::table('pedidosoks')
            ->where('id_producto', '=', $id)
            ->where('id_color', '=', $id_color)
            ->where('tetina', '=', $tetina)
            ->where('talla', '=', $talla)
            ->whereBetween('fecha',[$date1, $date2])
            ->sum('cantidad');
            //->toSql();

        return $data;
    }

    /**
     * Consulta SQL
     * @param $id
     * @return mixed
     */
    function consultaSqlChupete($id){
        $data = DB::table('color')
            ->where('codigo_producto', '=', $id)
            ->orderby('codigo_color', 'asc')
            ->get();

        return $data;
    }

    function createExcel(){
        Excel::create('Stock');

    }

    function vue(){
        return view('stock::dataTable');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    function almacen(Request $request){
        $id = $request->id;

        $almacen = Stock::find($id);
        $data = Stock::all()->toArray();

        //return $almacen;
        return array('articulo' => $almacen->articulo, 'id' => $almacen->id, 'color' => $almacen->color,
        'stock' => $almacen->stock, 'entrada' => $almacen->entrada, 'minimo' => $almacen->minimo,
            'tetina' => $almacen->tetina);
    }

    /**
     * @param Request $request
     * @return string
     */
    function actualizaAlmacen(Request $request){
        $id = $request->id;

        $almacen = Stock::find($id);
        $almacen->entrada = $request->entrada;
        $almacen->stock = intval($almacen->stock) + intval($almacen->entrada);
        $almacen->minimo = $request->minimo;

        $almacen->save();
        return 'hola';
    }
}
