<?php

namespace App\Modules\Stock\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;

class StockTableController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        //$chupeteSql = $this->consultaSqlChupete('001');

        $data = DB::table('stock')
            ->get();


        return Datatables::of($data)
            ->addColumn('id', function ($data){
                return $data->id;
            })
            ->addColumn('articulo', function ($data){
                return $data->articulo;
            })
            ->addColumn('color', function ($data){
                return $data->color;
            })
            ->addColumn('tetina', function ($data){
                return $data->tetina;
            })
            /*->addColumn('venta', function ($data){
                return $data->salida;
            })*/
            ->addColumn('stock', function ($data){
                if ($data->stock <= $data->minimo){
                    $class = 'label-danger';
                }else{
                    $class = 'label-info';
                }

                $label =  '<label class="label ' . $class . '">'. $data->stock . '</label>&nbsp;';
                $label .= '<label class="label label-default">'. $data->salida . '</label>&nbsp;';
                $label .= '<label class="label label-warning">'. $data->minimo . '</label>';

                return $label;
            })
            ->addColumn('action', function ($data){
                $buttons = '<a class="btn btn-xs btn-info" type="button" onclick="mostrarVentana('.$data->id.')"><i class="fa fa-refresh" data-toggle="tooltip" data-placement="top" title="Cambiar contraseña"></i></a> ';

                return $buttons;
            })

            ->make(true);
        //}

        //return view('admin.users.show');
    }

    /**
     * Consulta SQL
     * @param $id
     * @return mixed
     */
    function consultaSqlChupete($id){
        $data = DB::table('color')
            ->where('codigo_producto', '=', $id)
            ->orderby('codigo_color', 'asc')
            ->get();

        return $data;
    }

    /**
     * Chupetes especiales
     *
     * @param $id_color
     * @return mixed
     */
    function chupete($id_color, $date1, $date2){
        $data = DB::table('pedidosoks')
            ->where('id_producto', '=', 1)
            ->where('id_color', '=', $id_color)
            ->whereBetween('fecha',[$date1, $date2])
            ->sum('cantidad');

        return $data;
    }

    /**
     * @param $id
     * @param $id_color
     * @param $tetina
     * @param $talla
     * @param $date1
     * @param $date2
     * @return mixed
     */
    function chupetePedidoOk($id, $id_color, $tetina, $talla, $date1, $date2){
        $data = DB::table('pedidosoks')
            ->where('id_producto', '=', $id)
            ->where('id_color', '=', $id_color)
            ->where('tetina', '=', $tetina)
            ->where('talla', '=', $talla)
            ->whereBetween('fecha',[$date1, $date2])
            ->sum('cantidad');
        //->toSql();

        return $data;
    }
}
