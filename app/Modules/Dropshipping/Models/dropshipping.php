<?php

namespace App\Modules\Dropshipping\Models;

use Illuminate\Database\Eloquent\Model;

class dropshipping extends Model
{
    protected $table = 'dropshipping';
    public $timestamps = false;
}
