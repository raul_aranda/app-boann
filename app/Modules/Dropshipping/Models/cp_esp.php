<?php

namespace App\Modules\Dropshipping\Models;

use Illuminate\Database\Eloquent\Model;

class cp_esp extends Model
{
    protected $table = 'cp_esp';
    public $timestamps = false;
}
