<?php

namespace App\Modules\Dropshipping\Models;

use Illuminate\Database\Eloquent\Model;

class cpr_esp extends Model
{
    protected $table = 'cpr_esp';
    public $timestamps = false;
}
