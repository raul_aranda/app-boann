<?php

namespace App\Modules\Dropshipping\Models;

use Illuminate\Database\Eloquent\Model;

class clienteDrop extends Model
{
    protected $table = 'clienteDrop';
    public $timestamps = false;
}
