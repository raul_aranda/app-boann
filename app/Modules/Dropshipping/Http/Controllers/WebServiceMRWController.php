<?php

namespace App\Modules\Dropshipping\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

/**
 * Class WebServiceMRWController
 * @package App\Modules\Dropshipping\Http\Controllers
 *
 * Forma de request para enviar etiqueta
 *
 *  $request['DatosEntrega']['Direccion']['CodigoDireccion'] = '';
 *  $request['DatosEntrega']['Direccion']['CodigoTipoVia'] = '';
 *  $request['DatosEntrega']['Direccion']['Via'] = '';
 *  $request['DatosEntrega']['Direccion']['Numero'] = '';
 *  $request['DatosEntrega']['Direccion']['Resto'] = '';
 *  $request['DatosEntrega']['Direccion']['CodigoPostal'] = '';
 *  $request['DatosEntrega']['Direccion']['Poblacion'] = '';
 *  $request['DatosEntrega']['Direccion']['Provincia'] = '';
 *  $request['DatosEntrega']['Direccion']['CodigoPais'] = '';
 *  $request['DatosEntrega']['Nif'] = '';
 *  $request['DatosEntrega']['Nombre'] = '';
 *  $request['DatosEntrega']['Telefono'] = '';
 *  $request['DatosEntrega']['Contacto'] = '';
 *  $request['DatosEntrega']['ALaAtencionDe'] = '';
 *  $request['DatosEntrega']['Observaciones'] = '';
 *  $request['DatosServicio']['Peso'] = '';
 *  $request['DatosServicio']['Mail'] = '';
 *  $request['DatosServicio']['SMS'] = '';
 *
 *
 */
class WebServiceMRWController extends Controller
{
    private $dbh;
    private $FranquiciaMRW = 'xxxxx';
    private $AbonadoMRW = 'xxxxxx';
    private $DepartamentoMRW = '';
    private $usuarioMRW = 'xxxxxxxxxxxxx';
    private $PasswordMRW = 'xxxxxxxxxxxxx';

    /**
     * WebServiceMRWController constructor.
     */
    public function __construct(){
        // http://sagec-test.mrw.es/MRWEnvio.asmx?WSDL (PRE)
        // http://sagec.mrw.es/MRWEnvio.asmx?WSDL (PRO)
        $this->wsdl_url = "http://sagec-test.mrw.es/MRWEnvio.asmx?WSDL";

        $this->cabeceras = array(
            'CodigoFranquicia' => $this->FranquiciaMRW, //Obligatorio
            'CodigoAbonado' => $this->AbonadoMRW, //Obligatorio
            'CodigoDepartamento' => $this->DepartamentoMRW, //Opcional - Se puede omitir si no se han creado departamentos en sistemas de MRW.
            'UserName' => $this->usuarioMRW, //Obligatorio
            'Password' => $this->PasswordMRW //Obligatorio
        );

        $this->init();
    }

    /**
     * @return bool
     */
    private function init(){
        try {
            $this->clientMRW = new SoapClient(
                $this->wsdl_url,
                array(
                    'trace' => TRUE
                )
            );
        } catch (SoapFault $e) {
            printf("Error creando cliente SOAP: %s<br />\n", $e->__toString());
            return false;
        }
    }

    /**
     * @param $request
     * @return string
     */
    public function enviarEtiqueta($request){
        $responseCode = '';

        ## PARAMETROS DEL ENVIO ##
        // Datos de fecha actual
        date_default_timezone_set('CET');
        $hoy = date("d/m/Y");
        // Preparamos el array de parametros con los mismos datos del ejemplo que trae la documentacion

        $params = array(
            'request' => array(
                'DatosEntrega' => array(
                    ## DATOS DESTINATARIO ##
                    'Direccion' => array(
                        'CodigoDireccion' => $request['DatosEntrega']['Direccion']['CodigoDireccion'], //Opcional - Se puede omitir. Si se indica sustituira al resto de parametros
                        'CodigoTipoVia' => $request['DatosEntrega']['Direccion']['CodigoTipoVia'], //Opcional - Se puede omitir aunque es recomendable usarlo
                        'Via' => $request['DatosEntrega']['Direccion']['Via'], //Obligatorio
                        'Numero' => $request['DatosEntrega']['Direccion']['Numero'], //Obligatorio - Recomendable que sea el dato real. Si no se puede extraer el dato real se pondra 0 (cero)
                        'Resto' => $request['DatosEntrega']['Direccion']['Resto'], //Opcional - Se puede omitir.
                        'CodigoPostal' => $request['DatosEntrega']['Direccion']['CodigoPostal'], //Obligatorio
                        'Poblacion' => $request['DatosEntrega']['Direccion']['Poblacion'], //Obligatorio
                        'Provincia' => $request['DatosEntrega']['Direccion']['Provincia'], //Obligatorio
                        //'Estado' => '', //Opcional - Se debe omitir para envios nacionales.
                        'CodigoPais' => $request['DatosEntrega']['Direccion']['CodigoPais'] //Opcional - Se puede omitir para envios nacionales.
                    ),
                    'Nif' => $request['DatosEntrega']['Nif'], //Opcional - Se puede omitir.
                    'Nombre' => $request['DatosEntrega']['Nombre'], //Obligatorio
                    'Telefono' => $request['DatosEntrega']['Telefono'], //Obligatorio
                    'Contacto' => $request['DatosEntrega']['Contacto'], //Opcional - Muy recomendable
                    'ALaAtencionDe' => $request['DatosEntrega']['ALaAtencionDe'], //Opcional - Se puede omitir.
                    'Observaciones' => $request['DatosEntrega']['Observaciones'] //Opcional - Se puede omitir.
                ),
                ## DATOS DEL SERVICIO ##
                'DatosServicio' => array(
                    'Fecha' => $hoy,  //Obligatorio. Fecha >= Hoy()
                    'Referencia' => '',  //Obligatorio. ¿numero de pedido/albaran/factura?
                    'CodigoServicio' => '0800', //Obligatorio. Cada servicio deberá ser activado por la franquicia
                    //0800 = Ecommerce
                    ## Desglose de Bultos <--
                    'NumeroBultos' => '1', //Obligatorio. Solo puede haber un bulto por envio en eCommerce
                    'Peso' => $request['DatosServicio']['Peso'], //Obligatorio. Debe ser igual al valor Peso en BultoRequest si se ha usado
                    'Reembolso' => $request['DatosServicio']['Reembolso'], //Opcional - Se puede omitir. (coste adicional)
                    'ImporteReembolso' => $request['DatosServicio']['ImporteReembolso'], //Obligatorio si hay reembolso. Los decimales se indican con , (coma)
                    ## Notificaciones --> //Opcional - Se puede omitir todo el nodo y subnodos
                    'Notificaciones' => array(
                        'NotificacionRequest' => array(
                            0 => array(
                                'CanalNotificacion' => '2',    //Canal por el que se enviará la notificación
                                'TipoNotificacion' => '2',    //Tipo de la notificación
                                'MailSMS' => $request['DatosServicio']['Mail']    //Teléfono móvil o dirección email, según CanalNotificacion
                            ),
                            1 => array(
                                'CanalNotificacion' => '1',    //Canal por el que se enviará la notificación
                                'TipoNotificacion' => '4',    //Tipo de la notificación
                                'MailSMS' => $request['DatosServicio']['SMS']    //Teléfono móvil o dirección email, según CanalNotificacion
                            )
                        )
                    ),
                )
            )
        );


        // Cargamos los headers sobre el objeto cliente SOAP
        $header = new SoapHeader('http://www.mrw.es/', 'AuthInfo', $this->cabeceras);
        $this->clientMRW->__setSoapHeaders($header);
        // Invocamos el metodo TransmEnvio pasandole los parametros del envio
        try {
            $responseCode = $this->clientMRW->TransmEnvio($params);

            if ($responseCode->TransmEnvioResult->Estado == 1) {

                /*
                 *
                 * $responseCode => Nos devuelve un Object.
                 *
                 * $responseCode->TransmEnvioResult->Estado, $responseCode->TransmEnvioResult->Mensaje, $responseCode->TransmEnvioResult->NumeroSolicitud, $responseCode->TransmEnvioResult->NumeroEnvio, $responseCode->TransmEnvioResult->Url
                 *
                 * Con estos datos podemos introducirlos en BD y luego rescatarlos para obtener el número de envío.
                 *
                 */

            } else {

                // Error, introducir en un log.

            }
        } catch (SoapFault $exception) {
            // Error, mostramos la excepción del SOAP.
        }

        return $responseCode;
    }

    /**
     * @param $numero_envio
     * @return string
     */
    public function getEtiquetaMRW($numero_envio){
        $responseCode = '';

        $params = array(
            'request' => array(
                'NumeroEnvio' => $numero_envio,
                'SeparadorNumerosEnvio' => '',
                'FechaInicioEnvio' => '',
                'FechaFinEnvio' => '',
                'TipoEtiquetaEnvio' => '0',
                'ReportTopMargin' => '',
                'ReportLeftMargin' => '',
            )
        );

        $header = new SoapHeader('http://www.mrw.es/', 'AuthInfo', $this->cabeceras);
        $this->clientMRW->__setSoapHeaders($header);

        try {
            $responseCode = $this->clientMRW->EtiquetaEnvio($params);

            /*
             *
             *  $responseCode => Nos devuelve un Object.
             *
             *  $responseCode->GetEtiquetaEnvioResult->EtiquetaFile, Con esto obtenemos la etiqueta en base64.
             *
             */

        } catch (SoapFault $exception) {
            // Error, mostramos la excepción del SOAP.
        }

        return $responseCode;
    }
}
