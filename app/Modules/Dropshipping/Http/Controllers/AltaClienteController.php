<?php

namespace App\Modules\Dropshipping\Http\Controllers;

use App\Modules\Dropshipping\Models\clienteDrop;

use App\Modules\Dropshipping\Models\cp_esp;
use App\Modules\Dropshipping\Models\cpr_esp;
use http\Client\Response;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class AltaClienteController extends Controller
{
    public function index(Request $request){
        $nombre     = $request->nom;
        $apellido1  = $request->ape1;
        $apellido2  = $request->ape2;
        $telefono   = $request->tel;
        $cp         = $request->cpo;
        $provincia  = $request->pro;
        $localidad  = $request->loc;
        $via        = $request->via;
        $calle      = $request->cal;
        $numero     = $request->num;
        $resto      = $request->res;

        $clienteDrop = new clienteDrop();
        $clienteDrop->nombre = $nombre;
        $clienteDrop->apellido1 = $apellido1;
        $clienteDrop->apellido2 = $apellido2;
        $clienteDrop->telefono = $telefono;
        $clienteDrop->cp = $cp;
        $clienteDrop->provincia = $provincia;
        $clienteDrop->localidad = $localidad;
        $clienteDrop->via = $via;
        $clienteDrop->calle = $calle;
        $clienteDrop->numero = $numero;
        $clienteDrop->resto = $resto;

        $clienteDrop->save();

        session()->put('codigoClienteDrop', $clienteDrop->id);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function localidad(Request $request){
        $datoCp = $request->cp;

        $codigoPostal = cp_esp::where('cp', '=', $datoCp)
            ->get();

        return  $codigoPostal;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function provincia(Request $request){
        $datoCp = $request->cp;

        $codigProvincia = substr($datoCp, -5, 2);

        $dataProvincia = cpr_esp::where('codigo', '=', $codigProvincia)->first();

        return $dataProvincia->provincia;
    }
}
