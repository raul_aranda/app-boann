<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDropshipping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clienteDrop', function (Blueprint $table) {
            $table->increments('idCliente');
            $table->string('nombre', 60);
            $table->string('apellido1', 80);
            $table->string('apellido2',80);
            $table->string('telefono', 20);
            $table->string('cp', 5);
            $table->string('provincia', 20);
            $table->string('localidad', 40);
            $table->string('via', 10);
            $table->string('calle', 40);
            $table->integer('numero');
            $table->string('resto', 80)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clienteDrop', function (Blueprint $table) {
            Schema::dropIfExists('clienteDrop');
        });
    }
}
