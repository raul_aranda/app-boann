<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableClienteDrop extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dropshipping', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idCliente');
            $table->integer('idPedido');
            $table->string('idTienda');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dropsipping', function (Blueprint $table) {
            Schema::dropIfExists('dropshipping');
        });
    }
}
