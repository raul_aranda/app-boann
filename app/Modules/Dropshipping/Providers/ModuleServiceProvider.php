<?php

namespace App\Modules\Dropshipping\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'dropshipping');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'dropshipping');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'dropshipping');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
