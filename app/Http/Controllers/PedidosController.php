<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class PedidosController extends Controller
{
    function pedidos(){
        $codigo = Auth::user()->codigo_cliente;
        $pedidos = \App\Pedidos::where('id_cliente', '=', $codigo)
            ->orderBy('id', 'desc')
            ->paginate(10);

        return $pedidos;
    }
}
