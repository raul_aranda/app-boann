<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SelectController extends Controller
{
    /**
     * testFunction
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function testfunction(Request $request){
        if ($request->isMethod('post')){
            return response()->json(['response' => 'This is post method']);
        }

        return response()->json(['response' => 'This is get method']);
    }

    public function index(){
        //return response()->json(['response' => 'This is get method']);
    }

    /**
     * selectProductos
     *
     * @return string
     */
    public function selectProductos(){
        /*$datos = DB::table('productos')->get();

        $selectProducto = '<option selected="selected">Selecciona artículo</option>';

        foreach ($datos as $dato){
            $selectProducto .= '<option value='. $dato->id.'>'.$dato->descripcion.'</option>';
        }
        $selectProducto .= '</select>';*/
        //$datos = \App\Productos::all()

        $datos = DB::table('productos')
            ->where('active', 1)
            ->orderBy('orden', 'asc')
        ->get();

        return $datos;
    }


    /**
     * getColor
     *
     * @param Request $request
     * @return string
     */
    public function getColor(Request $request){
        $id = $request['id'];

        $datos = DB::table('color')
            ->where('codigo_producto', '=', $id)
            ->orderBy('codigo_color')
            ->get();

        $selectColor = '<option selected="selected">Color</option>';

        foreach ($datos as $dato){
            $selectColor .= '<option value='. $dato->id_color.'>'.$dato->codigo_color. ' '.$dato->descripcion_color.'</option>';
        }

        return $selectColor;
    }

    /**
     * getTalla
     *
     * @param Request $request
     * @return string
     */
    public function getTalla(Request $request){
        $id = $request['id'];

        switch ($id){
            case "ropa":
                $selectTalla  = '<option selected="selected">Talla</option>';
                $selectTalla .= '<option value="--- S-M">S-M (36-38)</option>';
                $selectTalla .= '<option value="--- M-L">M-L (40-42)</option>';
                $selectTalla .= '<option value="--- L-XL">L-XL (44-46)</option>';
                break;
            case "pegatina":
                $selectTalla  = '<option selected="selected">Opción</option>';
                $selectTalla .= '<option value="Interior">Interior</option>';
                $selectTalla .= '<option value="Exterior">Exterior</option>';
                break;
            case "silicona":
                $selectTalla  = '<option selected="selected">Tetina/Talla</option>';
                $selectTalla .= '<option value="Silicona Pequeña">Silicona pequeña</option>';
                $selectTalla .= '<option value="Silicona Grande">Silicona grande</option>';
                break;
            case "silicona_pequeña":
                $selectTalla  = '<option selected="selected">Tetina/Talla</option>';
                $selectTalla .= '<option value="Silicona Pequeña">Silicona pequeña</option>';
                break;
            case "body":
                $selectTalla  = '<option selected="selected">Opción</option>';
                $selectTalla .= '<option value="--- Perso">Personalizado</option>';
                $selectTalla .= '<option value="--- Frase">Frase</option>';
                break;
            default :
                $selectTalla  = '<option selected="selected">Tetina/Talla</option>';
                $selectTalla .= '<option value="Latex Pequeña">Latex pequeña</option>';
                $selectTalla .= '<option value="Latex Grande">Latex grande</option>';
                $selectTalla .= '<option value="Silicona Pequeña">Silicona pequeña</option>';
                $selectTalla .= '<option value="Silicona Grande">Silicona grande</option>';
                break;
        }

        return $selectTalla;
    }

    /**
     * @return string
     */
    public function getFrase(){
        $selectFrase  = '<option selected="selected">Frase</option>';
        $selectFrase .= '<option value="Aquí empieza todo">Aquí empieza todo</option>';
        $selectFrase .= '<option value="Hecho con mogollón de amor">Hecho con mogollón de amor</option>';
        $selectFrase .= '<option value="Hecha con mogollón de amor">Hecha con mogollón de amor</option>';
        $selectFrase .= '<option value="Prometo ser buena">Prometo ser buena</option>';
        $selectFrase .= '<option value="Prometo ser bueno">Prometo ser bueno</option>';
        $selectFrase .= '<option value="Tengo dos corazones">Tengo dos corazones</option>';

        return $selectFrase;
    }

    /**
     * @return string
     */
    public function getOpcional(){
        $selectOpcional  = '<option selected="selected">Dibujo</option>';
        $selectOpcional .= '<option value="Niño">Niño</option>';
        $selectOpcional .= '<option value="Niña">Niña</option>';
        $selectOpcional .= '<option value="Niño/Niña">Niño/Niña</option>';
        $selectOpcional .= '<option value="Niño/Niño">Niño/Niño</option>';
        $selectOpcional .= '<option value="Niña/Niña">Niña/Niña</option>';

        return $selectOpcional;
    }

    /**
     * Frases body
     *
     * @return string
     */
    public function getFraseBody(){
        $selectFraseBody  = '<option selected="selected">Frase</option>';
        $selectFraseBody .= '<option value="Prometo ser buena">Prometo ser buena</option>';
        $selectFraseBody .= '<option value="Prometo ser bueno">Prometo ser bueno</option>';
        $selectFraseBody .= '<option value="Hecho con mogollón de amor">Hecho con mogollón de amor</option>';
        $selectFraseBody .= '<option value="Hecha con mogollón de amor">Hecha con mogollón de amor</option>';
        $selectFraseBody .= '<option value="Mi tía mola mucho">Mi tía mola mucho</option>';
        $selectFraseBody .= '<option value="Te reto a no sonreir cuando me mires">Te reto a no sonreir cuando me mires</option>';
        $selectFraseBody .= '<option value="Yo no digo ni pío">Yo no digo ni pío</option>';

        return $selectFraseBody;
    }
}
