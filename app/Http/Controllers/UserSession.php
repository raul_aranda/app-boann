<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class UserSession extends Controller
{
    public function UserSession(){
        if (Auth::check() == false){
            $user = Auth::user();

            $user->last_logged_in_at = Carbon::now();
            $user->save();

            Session::flush();
            URL::redirect('login');
        }
    }
}
