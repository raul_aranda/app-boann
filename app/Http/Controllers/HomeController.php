<?php

namespace App\Http\Controllers;

use App\Productos;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $readRecordsController = new ReadRecordsController();
        $records = $readRecordsController->readOrders();

        $readOrderController = new PedidosController();
        $orders = $readOrderController->pedidos();

        $productos = new SelectController();
        $producto = $productos->selectProductos();

        return view('inicio',['data' => $records, 'order' => $orders, 'producto' => $producto]);
    }
}
