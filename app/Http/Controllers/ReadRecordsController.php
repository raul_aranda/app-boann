<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class ReadRecordsController extends Controller
{
    public function __construct(){
        $login = new \App\Http\Controllers\UserSession();
        //$login->UserSession();
    }

    public function readRecords(Request $request){
        $codigo = Auth::user()->codigo_cliente;

        //$login = new \App\Http\Controllers\UserSession();
        //$login->UserSession();

        $records = DB::table('pedidostmps')
            ->join('productos', 'productos.id', '=', 'pedidostmps.id_producto')
            ->leftjoin('color', 'pedidostmps.id_color', '=', 'color.id_color')
            ->where('pedidostmps.id_cliente','=', $codigo)
            ->get();

        return $records;
    }

    public function readOrders(){
        $codigo = Auth::user()->codigo_cliente;

        $orders = DB::table('pedidos')
            ->where('id_cliente','=', $codigo)
            ->get();


        $data = '<table class="table table-condensed table-bordered table-striped" style="margin-bottom: 10px;">
				<tr>
					<th width="2%">No.</th>
					<th width="20%">Fecha</th>
					<th width="10%">Líneas productos</th>
					<th width="10%">Albarán</th>
					<th width="3%">Pdf</th>
				</tr>';

        //$sqlQuery = "SELECT * FROM pedidos";

        if (count($orders) != 0){
            $number = 1;

            foreach ($orders as $item) {
                $data .= '<tr>
				<td>'.$number.'</td>
				<td>'.$item->fecha.'</td>
				<td>'.$item->num_art.'</td>
				<td>'.$item->num_art.'</td>
	
				<td>
					<a href="/pdf" class="btn btn-default" onclick="createPdf('.$item->id.')"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>
				</td>
    		</tr>';
                $number++;
            }
        }else{
            $data .= '<tr><td colspan="5">Records not found!</td></tr>';
        }
        $data .= '</table>';

        //echo $data;
        //return $orders;
        //return view('partials.order', ['order' => DB::table('pedidos')->paginate(20)]);
    }

}
