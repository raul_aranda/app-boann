<?php

namespace App\Http\Controllers\Backend\Pedidos;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class PedidosAlbaranController
 * @package App\Http\Controllers\Backend\Pedidos
 */
class PedidosAlbaranController extends Controller
{
    /**
     * Generación cabecera excel albaran
     *
     * @param Request $request
     */
    public function cabecera(Request $request){
        $miId = $request->input('pedido');

        for ($i = 0; $i < count($miId); $i++) {
            $albaranes = $this->consultaAlbaran($miId[$i]);

            // Textil
            foreach ($albaranes as $albaran){
                $fecha = Carbon::createFromFormat('Y-m-d H:i:s', $albaran->fecha);
                $fecha = $fecha->format('d-m-Y');
                $dataAlbaran[] = array(
                    'Cod Cliente' => $albaran->id_cliente,
                    'Su Pedido' => $albaran->id,
                    'Fecha Pedido' => $fecha,
                    'Lugar Entrega' => '',
                    'Domicilio de Entrega' => '',
                    'Localidad' => '',
                );
            }
        }

        Excel::create('cabecera albaran', function ($excel) use($dataAlbaran){
            // Título
            $excel->setTitle('Pedidos Taller');

            $excel->setDescription('Ejemplo excel laravel');

            $excel->sheet('Nombre', function ($sheet) use($dataAlbaran){
                $sheet->fromArray($dataAlbaran, null, 'A1', false, true);
            });
        })->download('xlsx');
    }

    /**
     * Consulta pedidos
     *
     * @param $id
     * @return mixed
     */
    public function consultaAlbaran($id){
        $albaran = DB::table('pedidos')
            ->select('pedidos.*')
            ->where('pedidos.id', '=', $id)
            ->get();

        return $albaran;
    }

    /**
     * Generación excel detalles albaran
     *
     * @param Request $request
     */
    public function detalle(Request $request){
        $miId = $request->input('pedido');

        for ($i = 0; $i < count($miId); $i++) {
            $detalles = $this->consultaDetalles($miId[$i]);

            $cantidad = 0;

            foreach ($detalles as $detalle){
                // Comprobación de artículos con cambio referencia
                $referencia = $this->compruebaReferencia($detalle->talla, $detalle->ref, $detalle->id_color);

                if ($detalle->id_producto == 1 && $detalle->modelo == 'color'){
                    $referencia = '0042CHUPEDECORADO';
                }

                if ($detalle->id_producto == 1 && $detalle->modelo == 'coco'){
                    $referencia = 'CHUPETECOCO';
                }

                if ($detalle->id_producto != 0){
                    $dataDetalle[] = array(
                        'Su Pedido' => $detalle->id_pedidook,
                        'Referencia' => $referencia,
                        'Cantidad' => $detalle->cantidad,
                        'Precios' => '',
                    );
                }

                // En caso de que el sujetachupete tela sea personalizado añadir una nueva referencia
                if ($detalle->id_producto == 16 && $detalle->talla == 'per'){
                    $dataDetalle[] = array(
                        'Su Pedido' => $detalle->id_pedidook,
                        'Referencia' => '0001PERSSUJETCHTEL',
                        'Cantidad' => $detalle->cantidad,
                        'Precios' => '',
                    );
                }

                //Promo Navidad ----------------------------------------------------------------------------------------
                /*if ($detalle->id_producto == 0){
                    $promo = $detalle->cantidad;

                    if ($detalle->id_color == 102){
                        $referencia = '0001CHUPETEDECORAD';
                    }else{
                        $referencia = '0001BAB+CHUPNAVIDA';
                    }

                    if ($detalle->talla == '5+2'){
                        $promo = $promo * 5;
                    }elseif ($detalle->talla == '10+5'){
                        $promo = $promo * 10;
                    }

                    $dataDetalle[] = array(
                        'Su Pedido' => $detalle->id_pedidook,
                        'Referencia' => $referencia,
                        'Cantidad' => $promo,
                        'Precios' => '',
                    );
                }*/
                // Fin promo Navidad -----------------------------------------------------------------------------------

                //Promo mundial
                if ($detalle->id_producto == 0){
                    $dataDetalle[] = array(
                        'Su Pedido' => $detalle->id_pedidook,
                        'Referencia' => $referencia,
                        'Cantidad' => $detalle->cantidad,
                        'Precios' => '',
                    );
                }
                //fin promo mundial



                $codigo_cliente = $detalle->id_cliente;
            }


            // Consulta sql portes
            //$portes = $this->consultaPortes($codigo_cliente);

            $portes = $this->muestraPortes($miId[$i]);


            // Añadir los portes
            //foreach ($portes as $porte){
                $dataDetalle[] = array(
                    'Su Pedido' => $detalle->id_pedidook,
                    'Referencia' => $portes->codportes,
                    'Cantidad' => 1,
                    'Precios' => '',
                );
           // }

            // Se añade array en blanco para luego hacer count-1 y que no llegue al límite del array
            $dataDetalle[] = array(
                'Su Pedido' => '',
                'Referencia' => '',
                'Cantidad' => '',
                'Precios' => '',
            );
        }

        for ($i = 0; $i < count($dataDetalle)-1; $i ++){
            $cantidad0 = $dataDetalle[$i]['Cantidad'];

            if ($dataDetalle[$i]['Referencia'] == $dataDetalle[$i+1]['Referencia']){
                $cantidad = $cantidad + $dataDetalle[$i]['Cantidad'];
            }else{
                if ($dataDetalle[$i]['Referencia'] <> ''){
                    $dataDetalleArray[] = array(
                        'Su Pedido' => $dataDetalle[$i]['Su Pedido'],
                        'Referencia' => $dataDetalle[$i]['Referencia'],
                        'Cantidad' => $cantidad + $cantidad0,
                        'Precios' => '',
                    );
                }
                $cantidad = 0;
            }
        }

        Excel::create('detalles_albaran', function ($excel) use($dataDetalleArray){
            // Título
            $excel->setTitle('Detalle Albaran');

            $excel->setDescription('Ejemplo excel laravel');

            $excel->sheet('Detalle', function ($sheet) use($dataDetalleArray){
                $sheet->fromArray($dataDetalleArray, null, 'A1', false, true);
            });
        })->download('xlsx');
    }


    /**
     * Consulta detalle pedidos
     *
     * @param $id
     * @return mixed
     */
    public function consultaDetalles($id){
        $detalles = DB::table('pedidosoks')
            ->select('pedidosoks.*', 'productos.ref')
            ->leftJoin('productos', 'productos.id', 'pedidosoks.id_producto')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->orderBy('id_producto', 'ASC')
            ->get();

        return $detalles;
    }

    /**
     * Consulta portes para el usuario y los añade al albarán
     *
     * @param $user
     * @return mixed
     */
    /*public function consultaPortes($user){
        $portes = DB::table('portes')
            ->select('portes.*')
            ->leftJoin('users', 'users.id_portes', 'portes.id')
            ->where('users.codigo_cliente', '=', $user)
            ->get();

        return $portes;
    }*/

    public function muestraPortes($id){
        $portes = DB::table('pedidos')
            ->select('codportes')
            ->where('id', '=', $id)
            ->first();

        return $portes;
    }

    /**
     * Dependiendo si las etiquetas es mixta o muiusos que aplique otra referencia
     *
     * @param $talla
     * @param $ref
     * @param $idColor
     * @return string
     */
    public function compruebaReferencia($talla, $ref, $idColor){
        $referencia = $ref;

        if ($talla == 'Mixto'){
            $referencia = '0001ETIQUETAMIXTA';
        }elseif ($talla == 'Multiuso'){
            $referencia = '0001ETIQUETAMULTIU';
        }

        if ($idColor == 82 || $idColor == 83 || $idColor == 84 || $idColor == 101){
            $referencia = '0001CADENAPREMIUM';
        }



        return $referencia;
    }
}
