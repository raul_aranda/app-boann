<?php

namespace App\Http\Controllers\Backend\Pedidos;

use App\User;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class PedidosTallerController extends Controller
{
    public function index(Request $request){
        $miId = $request->input('pedido');

        for ($i = 0; $i < count($miId); $i++){
            $textil = $this->consultaEtiquetas($miId[$i], 'Textil');
            $multiusos = $this->consultaEtiquetas($miId[$i], 'Multiuso');
            $mixtos = $this->consultaEtiquetas($miId[$i], 'Mixto');
            $pulseras = $this->consultaPulsera($miId[$i]);
            $zapatillas = $this->consultaZapatillas($miId[$i]);
            $chapas = $this->consultaChapas($miId[$i]);
            $pegatinas = $this->consultaPegatinas($miId[$i]);
            $camisetas = $this->consultaCamisetas($miId[$i]);
            $bolsas = $this->consultaBolsas($miId[$i]);
            $bodys = $this->consultaBody($miId[$i]);
            $navidades = $this->consultaNavidad($miId[$i]);
            $vajillas = $this->consultaVajilla($miId[$i]);
            $vasos = $this->consultaVasos($miId[$i]);
            $cubiertos = $this->consultaCubiertos($miId[$i]);
            $baberos = $this->consultaBaberos($miId[$i]);
            $caja01 = $this->consultaCaja($miId[$i], 32);
            $caja02 = $this->consultaCaja($miId[$i], 33);
            $sujetaTela = $this->consultaSujetaChupetesTela($miId[$i]);
            $ambientadores = $this->consultaAmbientador($miId[$i]);
            $mochilas = $this->consultaMochilas($miId[$i]);
            $talegas = $this->consultaTalegas($miId[$i]);
            $talegasMini = $this->consultaTalegasMini($miId[$i]);
            $aguaColonias = $this->consultaAguaColonia($miId[$i]);
            $mascarillaInfantil = $this->consultaMascarillaInfantil($miId[$i]);
            $mascarillaAdulto = $this->consultaMascarillaAdulto($miId[$i]);
            $bolsasPortaObjetos = $this->consultaBolsaPortaObjetos($miId[$i]);
            $cojines = $this->consultaCojin($miId[$i]);
            $dientes = $this->consultaCepilloDientes($miId[$i]);
            $cadenasSilicona = $this->consultaCadenaSilicona($miId[$i]);
            $chupetesColores = $this->consultaChupetesSoloColor($miId[$i]);
            $bolasMadera = $this->consultaBolasMadera($miId[$i]);
            $bolasPlastico = $this->consultaBolasPlastico($miId[$i]);

            // Textil
            foreach ($textil as $pTextil){
                //if ($pTextil->id_color);
                $dataTextil[] = array(
                    'textil' => 'TEXTIL',
                    'cantidad' => $pTextil->cantidad,
                    'niño' => $pTextil->descripcion_color,
                    'opcion' => '',
                    'Nombre' => $pTextil->nombre,
                    'Id' => $pTextil->id_cliente,
                    'Pedido' => $pTextil->id_pedidook,
                );
            }

            //Multiuso
            foreach ($multiusos as $multiuso){
                //if ($pTextil->id_color);
                $dataMultiuso[] = array(
                    'textil' => 'MULTIUSOS',
                    'cantidad' => $multiuso->cantidad,
                    'niño' => $multiuso->descripcion_color,
                    'opcion' => '',
                    'Nombre' => $multiuso->nombre,
                    'Id' => $multiuso->id_cliente,
                    'Pedido' => $multiuso->id_pedidook,
                );
            }

            // Mixto
            foreach ($mixtos as $mixto){
                //if ($pTextil->id_color);
                $dataMixto[] = array(
                    'textil' => 'MIXTO',
                    'cantidad' => $mixto->cantidad,
                    'niño' => $mixto->descripcion_color,
                    'opcion' => '',
                    'Nombre' => $mixto->nombre,
                    'Id' => $mixto->id_cliente,
                    'Pedido' => $mixto->id_pedidook,
                );
            }

            // Pegatinas
            foreach ($pegatinas as $pegatina){
                //if ($pTextil->id_color);
                $colorPegatina = '';

                $dataPegatina[] = array(
                    'textil' => 'BB A BORDO',
                    'cantidad' => $pegatina->cantidad,
                    'niño' => $pegatina->descripcion_color,
                    'opcion' => $pegatina->tetina,
                    'Nombre' => $pegatina->nombre,
                    'Id' => $pegatina->id_cliente,
                    'Pedido' => $pegatina->id_pedidook,
                    'Talla' => $pegatina->talla,
                );
            }

            // Zapatillas
            foreach ($zapatillas as $zapatilla) {
                //if ($pTextil->id_color);
                $dataZapatilla[] = array(
                    'textil' => 'ZAPAS',
                    'cantidad' => $zapatilla->cantidad,
                    'niño' => $zapatilla->descripcion_color,
                    'opcion' => $zapatilla->talla,
                    'Nombre' => $zapatilla->nombre,
                    'Id' => $zapatilla->id_cliente,
                    'Pedido' => $zapatilla->id_pedidook,
                );
            }

            // Chapas
            foreach ($chapas as $chapa) {
                //if ($pTextil->id_color);
                $dataChapas[] = array(
                    'textil' => 'CHAPAS',
                    'cantidad' => $chapa->cantidad,
                    'niño' => $chapa->descripcion_color,
                    'opcion' => '',
                    'Nombre' => $chapa->nombre,
                    'Id' => $chapa->id_cliente,
                    'Pedido' => $chapa->id_pedidook,
                );
            }

            // Pulseras
            foreach ($pulseras as $pulsera) {
                //if ($pTextil->id_color);
                $dataPulsera[] = array(
                    'textil' => 'PULSERA',
                    'cantidad' => $pulsera->cantidad,
                    'niño' => $pulsera->descripcion_color,
                    'opcion' => '',
                    'Nombre' => $pulsera->nombre,
                    'Id' => $pulsera->id_cliente,
                    'Pedido' => $pulsera->id_pedidook,
                );
            }

            // Camisetas premamá
            foreach ($camisetas as $camiseta) {
                //if ($pTextil->id_color);
                $dataCamiseta[] = array(
                    'textil' => 'CAMISETA',
                    'cantidad' => $camiseta->cantidad,
                    'niño' => $camiseta->descipcion_color,
                    'opcion' => $camiseta->talla,
                    'Nombre' => $camiseta->nombre,
                    'Id' => $camiseta->id_cliente,
                    'Pedido' => $camiseta->id_pedidook,
                );
            }

            // Bolsas de cambio
            foreach ($bolsas as $bolsa) {
                $dataBolsa[] = array(
                    'textil' => 'BOLSA CAMBIO',
                    'cantidad' => $bolsa->cantidad,
                    'niño' => $bolsa->descripcion_color,
                    'opcion' => '',
                    'Nombre' => $bolsa->nombre,
                    'Id' => $bolsa->id_cliente,
                    'Pedido' => $bolsa->id_pedidook,
                );
            }

            // Body personalizado
            foreach ($bodys as $body){
                $dataBody[] = array(
                    'textil' => 'BODY',
                    'cantidad' => $body->cantidad,
                    'niño' => $body->descripcion_color,
                    'opcion' => $body->talla,
                    'Nombre' => $body->nombre,
                    'Id' => $body->id_cliente,
                    'Pedido' => $body->id_pedidook,
                );
            }

            // Campaña Navidad
            foreach ($navidades as $navidad){
                $dataNavidad[] = array(
                    'textil' => 'CHUPETE COLOR',
                    'cantidad' => $navidad->cantidad,
                    'niño' => $navidad->descripcion_color,
                    'opcion' => $navidad->tetina . '/' .$navidad->talla,
                    'Nombre' => $navidad->nombre,
                    'Id' => $navidad->id_cliente,
                    'Pedido' => $navidad->id_pedidook,
                );
            }

            // Vajilla personalizada
            foreach ($vajillas as $vajilla){
                $dataVajilla[] = array(
                    'textil' => 'VAJILLA',
                    'cantidad' => $vajilla->cantidad,
                    'niño' => $vajilla->talla,
                    'opcion' => $vajilla->descripcion_color,
                    'Nombre' => $vajilla->nombre,
                    'Id' => $vajilla->id_cliente,
                    'Pedido' => $vajilla->id_pedidook,
                );
            }

            // Vasos personalizados
            foreach ($vasos as $vaso){
                $dataVaso[] = array(
                    'textil' => 'VASO',
                    'cantidad' => $vaso->cantidad,
                    'niño' => $vaso->talla,
                    'opcion' => $vaso->descripcion_color,
                    'Nombre' => $vaso->nombre,
                    'Id' => $vaso->id_cliente,
                    'Pedido' => $vaso->id_pedidook,
                );
            }

            // Cubiertos personalizados
            foreach ($cubiertos as $cubierto){
                $dataCubierto[] = array(
                    'textil' => 'CUBIERTO',
                    'cantidad' => $cubierto->cantidad,
                    'niño' => $cubierto->talla,
                    'opcion' => $cubierto->descripcion_color,
                    'Nombre' => $cubierto->nombre,
                    'Id' => $cubierto->id_cliente,
                    'Pedido' => $cubierto->id_pedidook,
                );
            }

            // Baberos bordados o con frase
            foreach ($baberos as $babero){
                $dataBabero[] = array(
                    'textil' => 'BABERO',
                    'cantidad' => $babero->cantidad,
                    'niño' => $babero->talla,
                    'opcion' => $babero->descripcion_color,
                    'Nombre' => $babero->nombre,
                    'Id' => $babero->id_cliente,
                    'Pedido' => $babero->id_pedidook,
                );
            }

            // Cajas regalo 01
            foreach ($caja01 as $caja1){
                $dataCaja01[] = array(
                    'textil' => 'CAJA01',
                    'cantidad' => $caja1->cantidad,
                    'niño' => $caja1->talla,
                    'opcion' => $caja1->descripcion_color,
                    'Nombre' => $caja1->nombre,
                    'Id' => $caja1->id_cliente,
                    'Pedido' => $caja1->id_pedidook,
                );
            }

            // Cajas regalo 01
            foreach ($caja02 as $caja2){
                $dataCaja01[] = array(
                    'textil' => 'CAJA02',
                    'cantidad' => $caja2->cantidad,
                    'niño' => $caja2->talla,
                    'opcion' => $caja2->descripcion_color,
                    'Nombre' => $caja2->nombre,
                    'Id' => $caja2->id_cliente,
                    'Pedido' => $caja2->id_pedidook,
                );
            }

            // Sujeta chueptes tela
            foreach ($sujetaTela as $sujetaChupete){
                $dataSujeta[] = array(
                    'textil' => 'SUJETA TELA',
                    'cantidad' => $sujetaChupete->cantidad,
                    'niño' => $sujetaChupete->talla,
                    'opcion' => $sujetaChupete->descripcion_color,
                    'Nombre' => $sujetaChupete->nombre,
                    'Id' => $sujetaChupete->id_cliente,
                    'Pedido' => $sujetaChupete->id_pedidook,
                );
            }

            // Ambientador personalizado
            foreach ($ambientadores as $ambientador){
                $dataAmbientador[] = array(
                    'textil' => 'AMBIENTADOR',
                    'cantidad' => $ambientador->cantidad,
                    'niño' => $ambientador->descripcion_color,
                    'opcion' => $ambientador->descripcion_color,
                    'Nombre' => $ambientador->nombre,
                    'Id' => $ambientador->id_cliente,
                    'Pedido' => $ambientador->id_pedidook,
                );
            }

            // Mochila personalizada
            foreach ($mochilas as $mochila){
                $dataMochila[] = array(
                    'textil' => 'MOCHILA',
                    'cantidad' => $mochila->cantidad,
                    'niño' => $mochila->descripcion_color,
                    'opcion' => $mochila->talla,
                    'Nombre' => $mochila->nombre,
                    'Id' => $mochila->id_cliente,
                    'Pedido' => $mochila->id_pedidook,
                );
            }

            // Talegas personalizadas
            foreach ($talegas as $talega){
                $dataTalega[] = array(
                    'textil' => 'TALEGA',
                    'cantidad' => $talega->cantidad,
                    'niño' => '',
                    'opcion' => $talega->talla,
                    'Nombre' => $talega->nombre,
                    'Id' => $talega->id_cliente,
                    'Pedido' => $talega->id_pedidook,
                );
            }

            // Talegas personalizadas pequeñas
            foreach ($talegasMini as $talegaMini){
                $dataTalegaMini[] = array(
                    'textil' => 'TALEGAPEQUENA',
                    'cantidad' => $talegaMini->cantidad,
                    'niño' => $talegaMini->descripcion_color,
                    'opcion' => $talegaMini->talla,
                    'Nombre' => $talegaMini->nombre,
                    'Id' => $talegaMini->id_cliente,
                    'Pedido' => $talegaMini->id_pedidook,
                );
            }

            // Agua de colonia 100ml
            foreach ($aguaColonias as $aguaColonia){
                $dataAguaColonia[] = array(
                    'textil' => 'AGUACOLONIA',
                    'cantidad' => $aguaColonia->cantidad,
                    'opcion' => $aguaColonia->talla,
                    'Nombre' => $aguaColonia->nombre,
                    'Id' => $aguaColonia->id_cliente,
                    'Pedido' => $aguaColonia->id_pedidook,
                );
            }

            // Mascarilla infantil
            foreach ($mascarillaInfantil as $maskInfantil){
                $dataMascarillaInfantil[] = array(
                    'textil' => 'MASCARILLAINFANTIL',
                    'cantidad' => $maskInfantil->cantidad,
                    'niño' => $maskInfantil->descripcion_color,
                    'opcion' => $maskInfantil->talla,
                    'Nombre' => $maskInfantil->nombre,
                    'Id' => $maskInfantil->id_cliente,
                    'Pedido' => $maskInfantil->id_pedidook,
                );
            }

            // Mascarilla adulto
            foreach ($mascarillaAdulto as $maskAdulto){
                $dataMascarillaAdulto[] = array(
                    'textil' => 'MASCARILLAADULTO',
                    'cantidad' => $maskAdulto->cantidad,
                    'niño' => $maskAdulto->descripcion_color,
                    'opcion' => $maskAdulto->talla,
                    'Nombre' => $maskAdulto->nombre,
                    'Id' => $maskAdulto->id_cliente,
                    'Pedido' => $maskAdulto->id_pedidook,
                );
            }

            // Bolsa porta objetos
            foreach ($bolsasPortaObjetos as $bolsaPortaObjeto){
                $dataBolsaPortaObjetos[] = array(
                    'textil' => 'BOLSAPORTAOBJETOS',
                    'cantidad' => $bolsaPortaObjeto->cantidad,
                    'niño' => $bolsaPortaObjeto->descripcion_color,
                    'opcion' => $bolsaPortaObjeto->talla,
                    'Nombre' => $bolsaPortaObjeto->nombre,
                    'Id' => $bolsaPortaObjeto->id_cliente,
                    'Pedido' => $bolsaPortaObjeto->id_pedidook,
                );
            }

            // Cojin personalizado
            foreach ($cojines as $cojin){
                $dataCojines[] = array(
                    'textil' => 'COJIN',
                    'cantidad' => $cojin->cantidad,
                    'niño' => $cojin->descripcion_color,
                    'opcion' => $cojin->talla,
                    'Nombre' => $cojin->nombre,
                    'Id' => $cojin->id_cliente,
                    'Pedido' => $cojin->id_pedidook,
                );
            }

            // Cepillo de dientes Bambú
            foreach ($dientes as $cepillo){
                $dataCepilloDientes[] = array(
                    'textil' => 'CEPILLO DIENTES',
                    'cantidad' => $cepillo->cantidad,
                    'niño' => $cepillo->descripcion_color,
                    'opcion' => $cepillo->talla,
                    'Nombre' => $cepillo->nombre,
                    'Id' => $cepillo->id_cliente,
                    'Pedido' => $cepillo->id_pedidook,
                );
            }

            // Cadena Silicona
            foreach ($cadenasSilicona as $cadenaSilicona){
                $dataCadenaSilicona[] = array(
                    'textil' => 'CADENA SILICONA',
                    'cantidad' => $cadenaSilicona->cantidad,
                    'niño' => $cadenaSilicona->descripcion_color,
                    'opcion' => $cadenaSilicona->talla,
                    'Nombre' => $cadenaSilicona->nombre,
                    'Id' => $cadenaSilicona->id_cliente,
                    'Pedido' => $cadenaSilicona->id_pedidook,
                );
            }

            // Chupete color
            foreach ($chupetesColores as $chupeteColor){
                $dataChupeteColor[] = array(
                    'textil' => 'CHUPETE COLOR',
                    'cantidad' => $chupeteColor->cantidad,
                    'niño' => $chupeteColor->descripcion_color,
                    'opcion' => $chupeteColor->talla . ' ' . $chupeteColor->tetina,
                    'Nombre' => $chupeteColor->nombre,
                    'Id' => $chupeteColor->id_cliente,
                    'Pedido' => $chupeteColor->id_pedidook,
                );
            }

            // Bolas Madera
            foreach ($bolasMadera as $bolaMadera){
                $dataBolaMadera[] = array(
                    'textil' => 'BOLAS MADERA',
                    'cantidad' => $bolaMadera->cantidad,
                    'niño' => $bolaMadera->descripcion_color,
                    'opcion' => $bolaMadera->talla,
                    'Nombre' => $bolaMadera->nombre,
                    'Id' => $bolaMadera->id_cliente,
                    'Pedido' => $bolaMadera->id_pedidook,
                );
            }

            // Bolas Plastico
            foreach ($bolasPlastico as $bolaPlastico){
                $dataBolaPlastico[] = array(
                    'textil' => 'BOLAS PLÁSTICO',
                    'cantidad' => $bolaPlastico->cantidad,
                    'niño' => $bolaPlastico->descripcion_color,
                    'opcion' => $bolaPlastico->talla,
                    'Nombre' => $bolaPlastico->nombre,
                    'Id' => $bolaPlastico->id_cliente,
                    'Pedido' => $bolaPlastico->id_pedidook,
                );
            }
        }

        if (!isset($dataTextil))        $dataTextil[] = array();
        if (!isset($dataMultiuso))      $dataMultiuso[] = array();
        if (!isset($dataCamiseta))      $dataCamiseta[] = array();
        if (!isset($dataMixto))         $dataMixto[] = array();
        if (!isset($dataPulsera))       $dataPulsera[]  = array();
        if (!isset($dataZapatilla))     $dataZapatilla[] = array();
        if (!isset($dataChapas))        $dataChapas[] = array();
        if (!isset($dataPegatina))      $dataPegatina[]  = array();
        if (!isset($dataBolsa))         $dataBolsa[] = array();
        if (!isset($dataBody))          $dataBody[] = array();
        if (!isset($dataNavidad))       $dataNavidad[] = array();
        if (!isset($dataVajilla))       $dataVajilla[] = array();
        if (!isset($dataVaso))          $dataVaso[] = array();
        if (!isset($dataCubierto))      $dataCubierto[] = array();
        if (!isset($dataBabero))        $dataBabero[] = array();
        if (!isset($dataCaja01))        $dataCaja01[] = array();
        if (!isset($dataCaja02))        $dataCaja02[] = array();
        if (!isset($dataSujeta))        $dataSujeta[] = array();
        if (!isset($dataAmbientador))   $dataAmbientador[] = array();
        if (!isset($dataMochila))       $dataMochila[] = array();
        if (!isset($dataTalega))        $dataTalega[] = array();
        if (!isset($dataTalegaMini))    $dataTalegaMini[] = array();
        if (!isset($dataAguaColonia))   $dataAguaColonia[] = array();
        if (!isset($dataMascarillaInfantil))   $dataMascarillaInfantil[] = array();
        if (!isset($dataMascarillaAdulto))   $dataMascarillaAdulto[] = array();
        if (!isset($dataBolsaPortaObjetos))   $dataBolsaPortaObjetos[] = array();
        if (!isset($dataCojines))   $dataCojines[] = array();
        if (!isset($dataCepilloDientes))   $dataCepilloDientes[] = array();
        if (!isset($dataCadenaSilicona))   $dataCadenaSilicona[] = array();
        if (!isset($dataChupeteColor))   $dataChupeteColor[] = array();
        if (!isset($dataBolaMadera))   $dataBolaMadera[] = array();
        if (!isset($dataBolaPlastico))   $dataBolaPlastico[] = array();

        /**/

        Excel::create('Taller', function ($excel) use($dataTextil, $dataMultiuso, $dataCamiseta, $dataMixto,
            $dataPulsera, $dataZapatilla, $dataChapas, $dataPegatina, $dataBolsa, $dataBody, $dataNavidad,
            $dataVajilla, $dataVaso, $dataCubierto, $dataBabero, $dataCaja01, $dataCaja02, $dataSujeta, $dataAmbientador,
            $dataMochila, $dataTalega, $dataTalegaMini, $dataAguaColonia, $dataMascarillaInfantil, $dataMascarillaAdulto,
            $dataBolsaPortaObjetos, $dataCojines, $dataCepilloDientes, $dataCadenaSilicona, $dataChupeteColor,
            $dataBolaMadera, $dataBolaPlastico){

            $excel->sheet('New Sheet', function ($sheet) use($dataTextil, $dataMultiuso, $dataCamiseta, $dataMixto,
                $dataPulsera, $dataZapatilla, $dataChapas, $dataPegatina, $dataBolsa, $dataBody, $dataNavidad,
                $dataVajilla, $dataVaso, $dataCubierto, $dataBabero, $dataCaja01, $dataCaja02, $dataSujeta, $dataAmbientador,
                $dataMochila, $dataTalega, $dataTalegaMini, $dataAguaColonia, $dataMascarillaInfantil, $dataMascarillaAdulto,
                $dataBolsaPortaObjetos, $dataCojines, $dataCepilloDientes, $dataCadenaSilicona, $dataChupeteColor,
                $dataBolaMadera, $dataBolaPlastico){

                $sheet->loadView('admin.users.pedidos.pedidosTaller',
                    ['textiles' => $dataTextil, 'multiusos' => $dataMultiuso, 'mixtos' => $dataMixto,
                        'pegatinas' => $dataPegatina, 'zapatillas' => $dataZapatilla, 'chapas' => $dataChapas,
                        'pulseras' => $dataPulsera, 'camisetas' => $dataCamiseta, 'bolsas' => $dataBolsa,
                        'bodys' => $dataBody, 'navidad' => $dataNavidad, 'vajillas' => $dataVajilla,
                        'vasos' => $dataVaso, 'cubiertos' => $dataCubierto, 'baberos' => $dataBabero,
                        'caja01' => $dataCaja01, 'caja02' => $dataCaja02, 'sujeta' => $dataSujeta, 'ambient' => $dataAmbientador,
                        'mochilas' => $dataMochila, 'talegas' => $dataTalega, 'talegasMini' => $dataTalegaMini,
                        'aguaColonias' => $dataAguaColonia, 'mascarillaInfantil' => $dataMascarillaInfantil,
                        'mascarillaAdulto' => $dataMascarillaAdulto, 'bolsasPortaObjetos' => $dataBolsaPortaObjetos,
                        'cojines' => $dataCojines, 'cepilloDientes' => $dataCepilloDientes, 'cadenasSilicona' => $dataCadenaSilicona,
                        'chupetesColores' => $dataChupeteColor, 'bolasMadera' => $dataBolaMadera, 'bolasPlastico' => $dataBolaPlastico]);
            });


            /*Excel::create('Taller', function ($excel)
            use($dataTextil, $dataMultiuso, $dataCamiseta, $dataMixto, $dataBlanco, $dataPulsera,
                $dataZapatilla, $dataChapas, $dataPegatina, $dataBolsa){
            // Título
                $excel->setTitle('Pedidos Taller');

                $excel->setDescription('Ejemplo excel laravel');

                $excel->sheet('Nombre', function ($sheet)
                use($dataTextil, $dataMultiuso, $dataCamiseta, $dataMixto, $dataBlanco, $dataPulsera,
                    $dataZapatilla, $dataChapas, $dataPegatina, $dataBolsa){
                    $sheet->cell('B2', function ($cell){
                        $cell->setValue('Pedido');
                        $cell->setFont(array(
                            'family' => 'Calibri',
                            'size'  => '16',
                            'bold'  => true
                        ));
                    });
                    //$celda = count($dataTextil);
                    $sheet->fromArray($dataTextil, null, 'B4', false, false);

                    $sheet->fromArray($dataMultiuso, null, 'B4', false, false);
                    $sheet->fromArray($dataMixto, null, 'B4', false, false);

                    $sheet->fromArray($dataPegatina, null, 'B4', false, false);

                    $sheet->fromArray($dataZapatilla, null, 'B4', false, false);

                    $sheet->fromArray($dataChapas, null, 'B4', false, false);

                    $sheet->fromArray($dataPulsera, null, 'B4', false, false);

                    $sheet->fromArray($dataCamiseta, null, 'B4', false, false);

                    $sheet->fromArray($dataBolsa, null, 'B4', false, false);
                });*/
        })->download('xlsx');

    }



    /**
     * Consulta pedidos etiquetas
     *
     * @param $id
     * @param $etiqueta
     * @return mixed
     */
    public function consultaEtiquetas($id, $etiqueta){
        /*$pedidos = DB::table('pedidos')
            ->leftJoin('users', 'pedidos.id_cliente', '=', 'users.codigo_cliente')
            ->leftJoin('pedidosoks', 'pedidosoks.id_pedidook', '=', 'pedidos.id')
            ->leftJoin('productos', 'productos.id', '=', 'pedidosoks.id_producto')
            ->select('pedidos.*', 'users.name', 'pedidosoks.id_producto', 'productos.descripcion', 'pedidosoks.nombre',
                'pedidosoks.id_color')
            ->where('pedidos.id', '=', $id)
            ->where('pedidosoks.id_producto', '=', 17)
            //->where('pedidosoks.talla', '=', $etiqueta)
            ->get();    */
        $pedidos = DB::table('pedidosoks')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 17)
            ->where('pedidosoks.talla', '=', $etiqueta)
            ->get();

        return $pedidos;
    }

    /**
     * @param $id
     * @param $etiqueta
     * @param $color
     * @return mixed
     */
    public function consultaEtiquetasColor($id, $etiqueta, $color){
        $pedidos = DB::table('pedidosoks')
            ->select('pedidosoks.*')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 17)
            ->where('pedidosoks.talla', '=', $etiqueta)
            ->where('pedidosoks.id_color', '=', $color)
            ->get();

        return $pedidos;
    }

    /**
     * Consulta pedidos pegatinas bebé a bordo
     *
     * @param $id
     * @return mixed
     */
    public function consultaPegatinas($id){
        $pegatina = DB::table('pedidosoks')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 22)
            ->orderBy('pedidosoks.id_color', 'DESC')
            ->get();

        return $pegatina;
    }

    /**
     * Consulta pedidos zapatillas
     *
     * @param $id
     * @return mixed
     */
    public function consultaZapatillas($id){
        $zapatilla = DB::table('pedidosoks')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 25)
            ->orderBy('pedidosoks.id_color', 'DESC')
            ->get();

        return $zapatilla;
    }

    /**
     * Consulta pedidos chapas
     * @param $id
     * @return mixed
     */
    public function consultaChapas($id){
        $chapas = DB::table('pedidosoks')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 24)
            ->get();

        return $chapas;
    }

    /**
     * Consulta pedidos pulseras identificativas
     *
     * @param $id
     * @return mixed
     */
    public function consultaPulsera($id){
        $pulsera = DB::table('pedidosoks')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 26)
            ->get();

        return $pulsera;
    }

    /**
     * Consulta pedidos camisetas
     *
     * @param $id
     * @return mixed
     */
    public function consultaCamisetas($id){
        $camiseta = DB::table('pedidosoks')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 21)
            ->get();

        return $camiseta;
    }

    /**
     * Consulta pedidos bolsa de cambio
     *
     * @param $id
     * @return mixed
     */
    public function consultaBolsas($id){
        $bolsa = DB::table('pedidosoks')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 28)
            ->get();

        return $bolsa;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function consultaBody($id){
        $body = DB::table('pedidosoks')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 27)
            ->get();

        return $body;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function consultaNavidad($id){
        $navidad = DB::table('pedidosoks')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 0)
            ->get();

        return $navidad;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function consultaVajilla($id){
        $vajilla = DB::table('pedidosoks')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 34)
            ->get();

        return $vajilla;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function consultaVasos($id){
        $vaso = DB::table('pedidosoks')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 35)
            ->get();

        return $vaso;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function consultaCubiertos($id){
        $cubierto = DB::table('pedidosoks')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 36)
            ->get();

        return $cubierto;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function consultaBaberos($id){
        $babero = DB::table('pedidosoks')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 12)
            ->get();

        return $babero;
    }

    /**
     * Cajas regalo tanto 01 como 02
     *
     * @param $id
     * @param $idProducto
     * @return mixed
     */
    public function consultaCaja($id, $idProducto){
        $caja = DB::table('pedidosoks')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', $idProducto)
            ->get();

        return $caja;
    }

    /**
     * Sujeta chupetes tela
     *
     * @param $id
     * @return mixed
     */
    public function consultaSujetaChupetesTela($id){
        $sujeta = DB::table('pedidosoks')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 16)
            ->get();

        return $sujeta;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function consultaAmbientador($id){
        $ambientador = DB::table('pedidosoks')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 43)
            ->get();

        return $ambientador;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function consultaMochilas($id){
        $mohila = DB::table('pedidosoks')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 44)
            ->get();

        return $mohila;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function consultaTalegas($id){
        $talega = DB::table('pedidosoks')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 45)
            ->get();

        return $talega;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function consultaTalegasMini($id){
        $talega = DB::table('pedidosoks')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 46)
            ->get();

        return $talega;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function consultaAguaColonia($id){
        $aguaColonia = DB::table('pedidosoks')
            ->select('pedidosoks.*')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 50)
            ->get();

        return $aguaColonia;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function consultaMascarillaInfantil($id){
        $mascaAdulto = DB::table('pedidosoks')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 57)
            ->get();

        return $mascaAdulto;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function consultaMascarillaAdulto($id){
        $mascaInfantil = DB::table('pedidosoks', 'color.descripcion_color')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 58)
            ->get();

        return $mascaInfantil;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function consultaBolsaPortaObjetos($id){
        $bolsaPortaObjetos = DB::table('pedidosoks', 'color.descripcion_color')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 59)
            ->get();

        return $bolsaPortaObjetos;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function consultaCojin($id){
        $cojines = DB::table('pedidosoks', 'color.descripcion_color')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 60)
            ->get();

        return $cojines;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function consultaCepilloDientes($id){
        $dientes = DB::table('pedidosoks', 'color.descripcion_color')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 5)
            ->get();

        return $dientes;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function consultaCadenaSilicona($id){
        $cadenaSilicona = DB::table('pedidosoks', 'color.descripcion_color')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 63)
            ->get();

        return $cadenaSilicona;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function consultaChupetesSoloColor($id){
        $chupetesColores = DB::table('pedidosoks', 'color.descripcion_color')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 1)
            ->where('pedidosoks.modelo', '=', 'color')
            ->get();

        return $chupetesColores;
    }

    /**
     * 
     */
    public function consultaBolasMadera($id){
        $bolasMadera = DB::table('pedidosoks', 'color.descripcion_color')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 66)
            ->get();

        return $bolasMadera;
    }


    /**
     * 
     */
    public function consultaBolasPlastico($id){
        $bolasPlastico= DB::table('pedidosoks', 'color.descripcion_color')
            ->select('pedidosoks.*', 'color.descripcion_color')
            ->leftJoin('color', 'color.id_color', '=' ,'pedidosoks.id_color')
            ->where('pedidosoks.id_pedidook', '=', $id)
            ->where('pedidosoks.id_producto', '=', 67)
            ->get();

        return $bolasPlastico;
    }



    /**
     * @param Request $request
     */
    public function csvMultiusos(Request $request){
        $miId = $request->input('pedido');

        //Bucle color Azul
        for ($i = 0; $i < count($miId); $i++) {
            $multiusos = $this->consultaEtiquetasColor($miId[$i], 'Multiuso', '99');
            $mixtos = $this->consultaEtiquetasColor($miId[$i], 'Mixto', '99');

            //Multiuso Azul
            foreach ($multiusos as $multiuso) {
                $nombres = $this->getNombreApellidos($multiuso->nombre);
                $cantidad = $multiuso->cantidad;

                for ($j = 0; $j < $cantidad; $j++) {
                    $dataMultiuso[] = array(
                        'Nombre' => $nombres[0],
                        'Apellidos' => $nombres[1],
                        'Completo' => $nombres[0] . ' ' . $nombres[1],
                        'textil' => 'MULTIUSO',
                        'niño' => $color = ($multiuso->id_color == 56 || $multiuso->id_color == 100) ? 'ROSA' : 'AZUL',
                        'cantidad' => $multiuso->cantidad,
                    );
                }
            }

            // Mixto Azul
            foreach ($mixtos as $mixto) {
                $nombres = $this->getNombreApellidos($mixto->nombre);
                $cantidad = $mixto->cantidad;

                for ($j = 0; $j < $cantidad; $j++) {
                    $dataMixto[] = array(
                        'Nombre' => $nombres[0],
                        'Apellidos' => $nombres[1],
                        'Completo' => $nombres[0] . ' ' . $nombres[1],
                        'textil' => 'MIXTO',
                        'niño' => $color = ($mixto->id_color == 56 || $mixto->id_color == 100) ? 'ROSA' : 'AZUL',
                        'cantidad' => $mixto->cantidad,
                    );
                }
            }
        }

        //Bucle color Rosa
        for ($i = 0; $i < count($miId); $i++) {
            $multiusos = $this->consultaEtiquetasColor($miId[$i], 'Multiuso', '100');
            $mixtos = $this->consultaEtiquetasColor($miId[$i], 'Mixto', '100');

            //Multiuso Rosa
            foreach ($multiusos as $multiuso) {
                $nombres = $this->getNombreApellidos($multiuso->nombre);
                $cantidad = $multiuso->cantidad;

                for ($j = 0; $j < $cantidad; $j++) {
                    $dataMultiuso[] = array(
                        'Nombre' => $nombres[0],
                        'Apellidos' => $nombres[1],
                        'Completo' => $nombres[0] . ' ' . $nombres[1],
                        'textil' => 'MULTIUSO',
                        'niño' => $color = ($multiuso->id_color == 56 || $multiuso->id_color == 100) ? 'ROSA' : 'AZUL',
                        'cantidad' => $multiuso->cantidad,
                    );
                }
            }

            // Mixto Rosa
            foreach ($mixtos as $mixto) {
                $nombres = $this->getNombreApellidos($mixto->nombre);
                $cantidad = $mixto->cantidad;

                for ($j = 0; $j < $cantidad; $j++) {
                    $dataMixto[] = array(
                        'Nombre' => $nombres[0],
                        'Apellidos' => $nombres[1],
                        'Completo' => $nombres[0] . ' ' . $nombres[1],
                        'textil' => 'MIXTO',
                        'niño' => $color = ($mixto->id_color == 56 || $mixto->id_color == 100) ? 'ROSA' : 'AZUL',
                        'cantidad' => $mixto->cantidad,
                    );
                }
            }
        }

        if (!isset($dataMultiuso)) $dataMultiuso[] = array();
        if (!isset($dataMixto))         $dataMixto[] = array();

        $data = array_merge($dataMultiuso, $dataMixto);

        $this->generaCSV($data);
    }

    /**
     * @param Request $request
     */
    public function csvTextil(Request $request){
        $miId = $request->input('pedido');

        //Bucle color azul
        for ($i = 0; $i < count($miId); $i++) {
            $textil = $this->consultaEtiquetasColor($miId[$i], 'Textil', '99');
            $mixtos = $this->consultaEtiquetasColor($miId[$i], 'Mixto', '99');

            // Textil Azul
            foreach ($textil as $pTextil) {
                $nombres = $this->getNombreApellidos($pTextil->nombre);
                $cantidad = $pTextil->cantidad;

                //Si existe más de uno lo añade por separado
                for ($j = 0; $j < $cantidad; $j++){
                    $dataTextil[] = array(
                        'Nombre' =>  $nombres[0],
                        'Apellidos' => $nombres[1],
                        'Completo' => $nombres[0] . ' ' . $nombres[1],
                        'textil' => 'TEXTIL',
                        'niño' => $color = ($pTextil->id_color == 56 || $pTextil->id_color == 100) ? 'ROSA' : 'AZUL',
                        'cantidad' => $pTextil->cantidad,
                    );
                }
            }

            // Mixto Azul
            foreach ($mixtos as $mixto) {
                $nombres = $this->getNombreApellidos($mixto->nombre);
                $cantidad = $mixto->cantidad;

                for ($j = 0; $j < $cantidad; $j++) {
                    $dataMixto[] = array(
                        'Nombre' => $nombres[0],
                        'Apellidos' => $nombres[1],
                        'Completo' => $nombres[0] . ' ' . $nombres[1],
                        'textil' => 'MIXTO',
                        'niño' => $color = ($mixto->id_color == 56 || $mixto->id_color == 100) ? 'ROSA' : 'AZUL',
                        'cantidad' => $mixto->cantidad,
                    );
                }
            }
        }

        //Bucle color rosa
        for ($i = 0; $i < count($miId); $i++) {
            $textil = $this->consultaEtiquetasColor($miId[$i], 'Textil', '100');
            $mixtos = $this->consultaEtiquetasColor($miId[$i], 'Mixto', '100');

            // Textil
            foreach ($textil as $pTextil) {
                $nombres = $this->getNombreApellidos($pTextil->nombre);

                $cantidad = $pTextil->cantidad;

                for ($j = 0; $j < $cantidad; $j++){
                    $dataTextil[] = array(
                        'Nombre' =>  $nombres[0],
                        'Apellidos' => $nombres[1],
                        'Completo' => $nombres[0] . ' ' . $nombres[1],
                        'textil' => 'TEXTIL',
                        'niño' => $color = ($pTextil->id_color == 56 || $pTextil->id_color == 100) ? 'ROSA' : 'AZUL',
                        'cantidad' => $pTextil->cantidad,
                    );
                }
            }

            // Mixto Rosa
            foreach ($mixtos as $mixto) {
                $nombres = $this->getNombreApellidos($mixto->nombre);
                $cantidad = $mixto->cantidad;

                for ($j = 0; $j < $cantidad; $j++) {
                    $dataMixto[] = array(
                        'Nombre' => $nombres[0],
                        'Apellidos' => $nombres[1],
                        'Completo' => $nombres[0] . ' ' . $nombres[1],
                        'textil' => 'MIXTO',
                        'niño' => $color = ($mixto->id_color == 56 || $mixto->id_color == 100) ? 'ROSA' : 'AZUL',
                        'cantidad' => $mixto->cantidad,
                    );
                }
            }
        }

        if (!isset($dataTextil))        $dataTextil[] = array();
        if (!isset($dataMixto))         $dataMixto[] = array();

        /*Excel::create('Csv', function ($excel) use ($dataTextil, $dataMultiuso, $dataMixto){
            $excel->sheet('Csv', function ($sheet) use ($dataTextil, $dataMultiuso, $dataMixto) {
                $sheet->loadView('admin.users.pedidos.csvTaller',
                    ['textiles' => $dataTextil, 'multiusos' => $dataMultiuso, 'mixtos' => $dataMixto]);
            });
        })->download('xlsx');*/

        $data = array_merge($dataTextil, $dataMixto);

        $this->generaCSV($data);

    }

    /**
     *
     * Tratando de separar nombres y apellidos de un campo. Se separa el nombre completo por palabras, se procesa
     * cada palabra, si es una palabra que forma parte de un nombre o apellido compuesto se guarda para anexarla
     * hasta el siguiente ciclo. Al final se tiene que tomar una decisión sobre que hacer para
     * separar los nombres si son más de dos. Si son …
     *
     * @param $nombre
     * @return mixed|string
     */
    public function getNombreApellidos($nombre){
        /* separar el nombre completo en espacios */
        $tokens = explode(' ', trim($nombre));
        /* arreglo donde se guardan las "palabras" del nombre */
        $names = array();
        /* palabras de apellidos (y nombres) compuetos */
        $special_tokens = array('da', 'de', 'del', 'la', 'las', 'los', 'mac', 'mc', 'van', 'von', 'y', 'i', 'san', 'santa');

        $prev = "";
        foreach($tokens as $token) {
            $_token = strtolower($token);
            if(in_array($_token, $special_tokens)) {
                $prev .= "$token ";
            } else {
                $names[] = $prev. $token;
                $prev = "";
            }
        }

        $num_nombres = count($names);

        $nombres = $apellidos = "";
        switch ($num_nombres) {
            case 0:
                $nombres = '';
                break;
            case 1:
                $nombres = $names[0];
                break;
            case 2:
                $nombres    = $names[0];
                $apellidos  = $names[1];
                break;
            case 3:
                $nombres   = $names[0];
                $apellidos = $names[1] . ' ' . $names[2];
                break;
            default:
                $apellidos = $names[0] . ' '. $names[1];
                unset($names[0]);
                unset($names[1]);

                $nombres = implode(' ', $names);
                break;
        }

        //$nombres    = mb_convert_case($nombres, MB_CASE_TITLE, 'UTF-8');
        //$apellidos  = mb_convert_case($apellidos, MB_CASE_TITLE, 'UTF-8');

        return array($nombres , $apellidos);
    }

    /**
     * @param $data
     */
    public function generaCSV($data){
        // output headers so that the file is downloaded rather than displayed
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename="demo.csv"');

        // do not cache the file
        header('Pragma: no-cache');
        header('Expires: 0');

        // create a file pointer connected to the output stream
        $file = fopen('php://output', 'w');

        // send the column headers
        //fputcsv($file, array('NOMBRE', 'APELLIDO', 'Column 3', 'Column 4', 'Column 5'));
        fputcsv($file, array('NOMBRE', 'APELLIDO', 'NOMBRE_COMPLETO', 'TIPO', 'COLOR', 'CANTIDAD'));

        // Sample data. This can be fetched from mysql too
        /*$data = array(
            array('Data 11', 'Data 12', 'Data 13', 'Data 14', 'Data 15'),
            array('Data 21', 'Data 22', 'Data 23', 'Data 24', 'Data 25'),
            array('Data 31', 'Data 32', 'Data 33', 'Data 34', 'Data 35'),
            array('Data 41', 'Data 42', 'Data 43', 'Data 44', 'Data 45'),
            array('Data 51', 'Data 52', 'Data 53', 'Data 54', 'Data 55')
        );*/

        /*$data = array(

            array('Data 11', 'Data 12'),
            array('Data 21', 'Data 22'),
            array('Data 31', 'Data 32'),
            array('Data 41', 'Data 42'),
            array('Data 51', 'Data 52')
        );*/


        // output each row of the data
        foreach ($data as $row)
        {
            fputcsv($file, $row);
        }


        exit();
    }
}
