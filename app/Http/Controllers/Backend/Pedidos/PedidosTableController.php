<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use App\User;

class PedidosTableController extends Controller
{
    public function index()
    {
        //if ($request()->ajax()) {
        /*$users = DB::table('pedidos')->select(['id', 'fecha', 'num_art', 'id_cliente']);
        return Datatables::of($users)
            ->addColumn('action', function ($user){
                $buttons  =  '<a href="#" class="btn btn-xs btn-info"><i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="Visualizar"></i></a> ';

                return $buttons;
            })
            ->make(true);*/
        //}

        //return view('admin.users.pedidos');
        return Datatables::of(User::query())
            ->addColumn('active', function ($user) {
                if ($user->active == 0){
                    return '<label class="label label-danger">No</label>';
                }else{
                    return '<label class="label label-success">Sí</label>';
                }
            })
            ->addColumn('action', function ($user){
                $buttons  =  '<a href="'.route('users.show', $user->id).'" class="btn btn-xs btn-info"><i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="Visualizar"></i></a> ';
                $buttons .=  '<a href="'.route('users.edit', $user->id).'" class="btn btn-xs btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Modificar"></i></a> ';
                $buttons .=  '<a href="'.route('users.create').'" class="btn btn-xs btn-info"><i class="fa fa-refresh" data-toggle="tooltip" data-placement="top" title="Cambiar contraseña"></i></a> ';

                if ($user->active == 0){
                    $buttons .=  '<a href="'.route('user.activate', $user->id).'" class="btn btn-xs btn-success"><i class="fa fa-play" data-toggle="tooltip" data-placement="top" title="Activar"></i></a> ' ;
                }else{
                    $buttons .=  '<a href="'.route('user.deactivate', $user->id).'" class="btn btn-xs btn-warning"><i class="fa fa-pause" data-toggle="tooltip" data-placement="top" title="Desactivar"></i></a> ';
                }

                $buttons .=  '<a href="'.route('users.create').'" class="btn btn-xs btn-danger"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Eliminar"></i></a>';

                return $buttons;
            })
            ->make(true);
    }
}
