<?php

namespace App\Http\Controllers\Backend\Pedidos;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class PedidosBackendController extends Controller
{
    /**
     * Listado de todos los pedidos con paginación
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $pedidos = DB::table('pedidos')
            ->leftJoin('users', 'pedidos.id_cliente', '=', 'users.codigo_cliente')
            ->select('pedidos.*', 'users.name')
            ->orderBy('id', 'desc')
            ->paginate(10);

        Session::forget('fecha1');
        Session::forget('fecha2');

        return view('admin.users.pedidos.pedidos', ['data' => $pedidos, 'filtro' => 'false',
            'fecha1'=>'', 'fecha2'=>'']);
    }

    /**
     * Activación filtro por fechas
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function filtro(Request $request){
        $dt1 = Carbon::createFromFormat('d-m-Y H:i:s', $request->date1);
        $dt2 = Carbon::createFromFormat('d-m-Y H:i:s', $request->date2);

        Session::put('fecha1', $request->date1);
        Session::put('fecha2', $request->date2);

        $pedidos = DB::table('pedidos')
            ->leftJoin('users', 'pedidos.id_cliente', '=', 'users.codigo_cliente')
            ->select('pedidos.*', 'users.name')
            ->whereBetween('fecha',[$dt1, $dt2])
            ->orderBy('id', 'desc')
            ->get();

        return view('admin.users.pedidos.pedidos', ['data' => $pedidos, 'filtro' => 'true',
            'fecha1' => $request->date1, 'fecha2'=>$request->date2]);

    }

    /**
     * Ordenación por código cliente
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function orden(Request $request){
        if (isset($request->filtro)) {
            if ($request->filtro == 'cliente'){
                $pedidos = $this->orderBy('users.codigo_cliente');
            }else if($request->filtro == 'nombre') {
                $pedidos = $this->orderBy('users.name');
            }else{
                $pedidos = $this->orderBy('pedidos.fecha');
            }

        }


        /*if (Session::has('fecha1')){
            $dt1 = Carbon::createFromFormat('d-m-Y H:i:s', Session::get('fecha1'));
            $dt2 = Carbon::createFromFormat('d-m-Y H:i:s', Session::get('fecha2'));

            $pedidos = DB::table('pedidos')
                ->leftJoin('users', 'pedidos.id_cliente', '=', 'users.codigo_cliente')
                ->select('pedidos.*', 'users.name')
                ->whereBetween('fecha',[$dt1, $dt2])
                ->orderBy('users.codigo_cliente', 'desc')
                ->get();
        }else{
            $pedidos = DB::table('pedidos')
                ->leftJoin('users', 'pedidos.id_cliente', '=', 'users.codigo_cliente')
                ->select('pedidos.*', 'users.name')
                ->orderBy('users.codigo_cliente', 'desc')
                ->get();
        }*/
        if (Session::has('fecha1')) {
            $dt1 = Carbon::createFromFormat('d-m-Y H:i:s', Session::get('fecha1'));
            $dt2 = Carbon::createFromFormat('d-m-Y H:i:s', Session::get('fecha2'));
        }else{
            $dt1 = '';
            $dt2 = '';
        }

        return view('admin.users.pedidos.pedidos', ['data' => $pedidos, 'filtro' => 'true',
            'fecha1' => $dt1, 'fecha2' => $dt2]);

    }


    public function orderBy($filtro){

        if (Session::has('fecha1')){
            $dt1 = Carbon::createFromFormat('d-m-Y H:i:s', Session::get('fecha1'));
            $dt2 = Carbon::createFromFormat('d-m-Y H:i:s', Session::get('fecha2'));

            $pedidos = DB::table('pedidos')
                ->leftJoin('users', 'pedidos.id_cliente', '=', 'users.codigo_cliente')
                ->select('pedidos.*', 'users.name')
                ->whereBetween('fecha',[$dt1, $dt2])
                ->orderBy($filtro, 'desc')
                ->get();
        }else{
            $pedidos = DB::table('pedidos')
                ->leftJoin('users', 'pedidos.id_cliente', '=', 'users.codigo_cliente')
                ->select('pedidos.*', 'users.name')
                ->orderBy($filtro, 'desc')
                ->paginate(10);
        }

        return $pedidos;
    }

}
