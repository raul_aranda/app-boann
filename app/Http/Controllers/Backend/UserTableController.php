<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;

class UserTableController extends Controller
{

    /**
     * Displays datatables front end view
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        //if ($request()->ajax()) {
            return Datatables::of(User::query())
                ->addColumn('name', function ($user){
                    $catalogo = \App\Modules\Catalogo\Http\Controllers\CatalogoController::getCatalogoCliente($user->codigo_cliente);

                    if ($catalogo == 0){
                        $circle = '<i class="icon md-circle grey-600" aria-hidden="true" style="font-size: 8px;"></i> ';
                    }else{
                        $circle = '<i class="icon md-circle red-600" aria-hidden="true" style="font-size: 8px;"></i> ';
                    }

                    return $circle . ' ' . $user->name;
                })
                ->addColumn('active', function ($user) {
                    if ($user->active == 0){
                        return '<label class="label label-danger">No</label>';
                    }else{
                        return '<label class="label label-success">Sí</label>';
                    }
                })
                ->addColumn('action', function ($user){
                    $buttons  =  '<a href="'.route('users.show', $user->id).'" class="btn btn-xs btn-info"><i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="Visualizar"></i></a> ';
                    $buttons .=  '<a href="'.route('users.edit', $user->id).'" class="btn btn-xs btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Modificar"></i></a> ';
                    //$buttons .=  '<a href="'.route('user.pwd', $user->id).'" class="btn btn-xs btn-info"><i class="fa fa-refresh" data-toggle="tooltip" data-placement="top" title="Cambiar contraseña"></i></a> ';
                    $buttons .= '<a class="btn btn-xs btn-info" type="button" onclick="mostrarVentana('.$user->id.')"><i class="fa fa-refresh" data-toggle="tooltip" data-placement="top" title="Cambiar contraseña"></i></a> ';

                    if ($user->active == 0){
                        $buttons .=  '<a href="'.route('user.activate', $user->id).'" class="btn btn-xs btn-success"><i class="fa fa-play" data-toggle="tooltip" data-placement="top" title="Activar"></i></a> ' ;
                    }else{
                        $buttons .=  '<a href="'.route('user.deactivate', $user->id).'" class="btn btn-xs btn-warning"><i class="fa fa-pause" data-toggle="tooltip" data-placement="top" title="Desactivar"></i></a> ';
                    }

                    $buttons .=  '<a href="'.route('user.borrar', $user->id).'" class="btn btn-xs btn-danger"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Eliminar"></i></a>';

                    return $buttons;
                })
                ->make(true);
        //}

        //return view('admin.users.show');
    }
}
