<?php

namespace App\Http\Controllers\Backend;

use App\Modules\Catalogo\Models\Catalogo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use \App\User;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    private $path = 'admin.users';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = User::all();
        //$data = User::paginate(15);

        return view($this->path.'.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $gastosEnvio = DB::table('envios')
            ->get();

        return view($this->path.'.create',['envios' => $gastosEnvio]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       try{
           $user = new User();
           $user->name             = Input::get('name');
           $user->email            = Input::get('email');
           $user->codigo_cliente   = Input::get('cliente');
           if (!empty(Input::get('pwdtxt'))){
               $user->encrypt_password = Crypt::encrypt(Input::get('pwdtxt'));
               $user->password = bcrypt(Input::get('pwdtxt'));
               $user->remember_token = md5(uniqid(mt_rand(), true));
           }
           $user->direccion        = Input::get('direccion');
           $user->poblacion        = Input::get('poblacion');
           $user->provincia        = Input::get('provincia');
           $user->cp               = Input::get('cp');
           $user->tlf              = Input::get('telefono');
           $user->geoaddress       = Input::get('geoadd');
           $user->lat              = Input::get('latitud');
           $user->lon              = Input::get('longitud');
           $user->active           = (Input::has('status')) ? '1' : '0';
           $user->agente           = Input::get('agente');

           $user->save();

           //Catálogo
           $catalogoEnviado = (Input::has('send')) ? '1' : '0';

           if ($catalogoEnviado == '1'){
               $catalogo = Catalogo::where('idCliente', '=', $user->codigo_cliente)->get();

               if (count($catalogo) == 0){
                   $cat = new Catalogo();

                   $cat->idCliente = $user->codigo_cliente;
                   $cat->fecha    = Carbon::now();

                   $cat->save();
               }
           }

           return redirect()->route('users.index')->withFlashSuccess('Usuario añadido correctamente');

       }catch (Exception $e){
           return "Fatal error - " .$e->getMessage();
       }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function obtener(Request $request){
        $id = $request->id;

        $user = User::find($id);

        return array('nombre' => $user->name);
    }

    public function reset(Request $request){
        $id = $request->id;
        $pwd = $request->pwd;

        $user = User::find($id);
        $user->password = Hash::make($pwd);
        $user->encrypt_password = Crypt::encrypt($pwd);
        $user->remember_token = md5(uniqid(mt_rand(), true));

        $user->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        $tienda = $user->name;

        $pedidos = DB::table('pedidos')
            ->where('id_cliente', '=', $user->codigo_cliente)
            ->orderBy('id', 'desc')
            ->get();

        $catalogo = Catalogo::where('idCliente', '=', $user->codigo_cliente)->first();

        return view($this->path.'.show', ['user' => $user, 'catalogo' => $catalogo, 'pedidos' => $pedidos, 'tienda' => $tienda]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        /*$catalogo = DB::table('catalogos')
            ->where('idCliente', '=', $user->codigo_cliente)
            ->get();*/

        $gastosEnvio = DB::table('envios')
            ->get();

        return view($this->path.'.edit', ['user' => $user, 'envios' => $gastosEnvio]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name'      => 'required',
            'email'     => 'required|email',
            'direccion' => 'required',
            'direccion' => 'required',
            'poblacion' => 'required',
            'provincia' => 'required',
            'cp'        => 'required',
            'telefono'  => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()){
            return Redirect::to('users/'.$id.'/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }else{
            $user = User::find($id);

            $user->name             = Input::get('name');
            $user->email            = Input::get('email');
            if (!empty(Input::get('pwdtxt'))){
                $user->encrypt_password = Crypt::encrypt(Input::get('pwdtxt'));
                $user->password = bcrypt(Input::get('pwdtxt'));
                $user->remember_token = md5(uniqid(mt_rand(), true));
            }
            $user->direccion        = Input::get('direccion');
            $user->poblacion        = Input::get('poblacion');
            $user->provincia        = Input::get('provincia');
            $user->cp               = Input::get('cp');
            $user->tlf              = Input::get('telefono');
            $user->geoaddress       = Input::get('geoadd');
            $user->lat              = Input::get('latitud');
            $user->lon              = Input::get('longitud');
            $user->active           = (Input::has('status')) ? '1' : '0';
            $user->agente           = Input::get('agente');
            $user->envio            = Input::get('envio');

            $user->save();
            //$request->session()->flash('flash_success', 'Usuario modificado correctamente');
            //Session::flash('message','Usuario modificado correctamente');

            //Comprobación de si ya se le ha enviado el catálogo
            /*$catalogoEnviado = (Input::has('send')) ? '1' : '0';

            if ($catalogoEnviado == '1'){
                $catalogo = Catalogo::where('idCliente', '=', $user->codigo_cliente)->get();

                if (count($catalogo) == 0){
                    $cat = new Catalogo();

                    $cat->idCliente = $user->codigo_cliente;
                    $cat->fecha    = Carbon::now();

                    $cat->save();
                }
            }*/

            return Redirect::to('users')->withFlashSuccess('Usuario Actualizado');
        }
        //return redirect()->route($this->path.'.index')->withFlashSucess('Usuario Actualizado');
    }

    /**
     * Activa el usuario
     *
     * @param $id
     * @return mixed
     */
    public function activate($id){
        $user = User::find($id);

        $user->active = 1;
        $user->save();
        return Redirect::to('users')->withFlashInfo('Usuario Activado');
    }

    /**
     * Desactiva el usuario
     *
     * @param $id
     * @return mixed
     */
    public function deactivate($id){
        $user = User::find($id);

        $user->active = 0;
        $user->save();
        return Redirect::to('users')->withFlashInfo('Usuario Desactivado');
    }

    public function borraUsuario($id){
       $user = User::find($id);

       $user->delete();
       return Redirect::to('users')->withFlashInfo('Usuario con id ' .$id . ' eliminado');
    }

}
