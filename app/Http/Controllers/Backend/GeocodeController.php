<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Auth\Access\Response;
use Illuminate\Http\Request;
//use Jcf\Geocode\Geocode;
use App\Http\Controllers\Controller;
use Jcf\Geocode\Facades\Geocode;

class GeocodeController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Jcf\Geocode\Exceptions\EmptyArgumentsException
     */
    public function index(Request $request){

        $geocode = Geocode::make()->address($request->address);
        //$geocode = Geocode::make()->address("C/ Almez, 6 , Archidona, Málaga, 29300");


        /*if ($response) {
            echo $response->latitude();
            echo $response->longitude();
            echo $response->formattedAddress();
            echo $response->locationType();

            return Response::json($response);
        }*/
        //return $response->latitude().",".$response->latitude();
        //return $response;
       return response()->json(array(
           'lat' => $geocode->latitude(),
           'lon' => $geocode->longitude(),
           'for' => $geocode->formattedAddress(),
       ));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Jcf\Geocode\Exceptions\EmptyArgumentsException
     */
    public function add(Request $request){
        $geocode = Geocode::make()->address($request->address);
        return response()->json(array(
            'lat' => $geocode->latitude(),
            'lon' => $geocode->longitude(),
            'for' => $geocode->formattedAddress(),
        ));
    }
}

