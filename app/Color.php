<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $primaryKey = 'id_color';
    protected $table = 'color';
}
