<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Notifications\Notifiable;


Route::get('/', function () {
    //return view('welcome');
    return Redirect::to('login');
});

Auth::routes();

Route::get('/home', function (){
    return Redirect::to('/inicio');
});
Route::get('/inicio', 'HomeController@index');


//Select
Route::get('/productos', 'SelectController@selectProductos');
Route::get('/colores', 'SelectController@getColor');
Route::get('/talla', 'SelectController@getTalla');
Route::get('/frase', 'SelectController@getFrase');
Route::get('/opcional', 'SelectController@getOpcional');
Route::get('/frase_body', 'SelectController@getFraseBody');

//Read Records
Route::get('/readRecords', 'ReadRecordsController@readRecords');
Route::get('/readOrders', 'ReadRecordsController@readOrders');


//Add Record
//Route::get('/addRecord', 'AddRecordsController@addRecords');
//Route::get('/addOrder', 'AddRecordsController@addOrder');

//Delete Record
Route::get('/deleteRecord', function (Request $request){
    \App\Pedidostmps::where('id_pedido', '=', $request->id)
        ->delete();

    return \App\Pedidostmps::all();
});


//Contadores
Route::get('/countOrders', function (){
    $codigo = Auth::user()->codigo_cliente;

    $count = DB::table('pedidos')
        ->where('id_cliente', '=', $codigo)
        ->count();

    return $count;
});

Route::get('/countTmp', function (){
    $codigo = Auth::user()->codigo_cliente;

    $count = DB::table('pedidostmps')
        ->where('id_cliente', '=', $codigo)
        ->count();

    return $count;
});

Route::get('/test-mail', function (){
    Mail::send('emails.send', ['title' => 'hoola', 'content'=> 'mas hola'], function ($message){
        $message->to('marketing@maxibebe.org', 'Raul')
            ->subject('Bienvenido');
    }) ;
});

// Al procesar el pedido
Route::get('/sendmail', function (){
    $codigo = Auth::user()->codigo_cliente;
    $nombre = Auth::user()->name;
    $mail   = Auth::user()->email;

    $id_order = Session::get('id_order');

    $get = DB::table('pedidosoks')
        ->join('productos', 'productos.id', '=', 'pedidosoks.id_producto')
        ->leftjoin('color', 'pedidosoks.id_color', '=', 'color.id_color')
        ->orderBy('productos.orden', 'asc')
        ->where([
            ['id_cliente', '=', $codigo],
            ['id_pedidook', '=', $id_order],
        ])
        ->get();

    $pedido = DB::table('pedidos')
        ->where([
            ['id_cliente', '=', $codigo],
            ['id', '=', $id_order],
        ])
        ->get();

    $clienteDropShipping = DB::table('dropshipping')
        ->select('*')
        ->leftjoin('clienteDrop', 'clienteDrop.id', '=', 'dropshipping.idCliente')
        ->where('dropshipping.idPedido', '=', $id_order)
        ->get();

    Session::forget('id_order');

    //return view('emails.mail', ['data' => $get, 'pedido' => $pedido]);NOOOO
    //return view('emails.pdf', ['data' => $get, 'pedido' => $pedido, 'dropShipping' => $clienteDropShipping]);

    $pdf = PDF::loadView('emails.pdf', ['data' => $get, 'pedido' => $pedido, 'dropShipping' => $clienteDropShipping]);

    //return $pdf->stream();

    Mail::send('emails.mail', ['html' => 'view', 'data' => $get, 'pedido' => $pedido, 'dropShipping' => $clienteDropShipping], function ($message)
    use($pdf, $codigo, $nombre)
    {
        $codigoCliente = substr($codigo, -4);
        $message->from('app@chupetesboann.com', 'App Boann');
        $message->to('raul@maxibebe.org')->subject('Pedido '.$codigoCliente. ' '.$nombre);
        $message->attachData($pdf->output(), "pedido.pdf");
    });

    Mail::send('emails.mailCliente', ['html' => 'view', 'data' => $get, 'pedido' => $pedido], function ($message)
    use($codigo, $nombre, $mail)
    {
        $message->from('app@chupetesboann.com', 'App Boann');
        $message->to($mail)->subject('Pedido Chupetes Boann');
    });
});

// Visualización pdf pedido
Route::get('/pedido', function(){
    $codigo = Auth::user()->codigo_cliente;
    $id_order = Session::get('id_order');

    $get = DB::table('pedidosoks')
        ->join('productos', 'productos.id', '=', 'pedidosoks.id_producto')
        ->leftjoin('color', 'pedidosoks.id_color', '=', 'color.id_color')
        ->where([
            ['id_cliente', '=', $codigo],
            ['id_pedidook', '=', $id_order],
            ['id_producto', '<>', 29],
        ])
        ->get();

    $pedido = DB::table('pedidos')
        ->where([
            ['id_cliente', '=', $codigo],
            ['id', '=', $id_order],
        ])
        ->get();

    return view('emails.prueba', ['data' => $get, 'pedido' => $pedido]);
});

Route::get('/createPdf', 'CreatePdfController@Pdf')->name('create.pdf');

// Pedidos temporales
Route::get('/pedidos', function (){
    $codigo = Auth::user()->codigo_cliente;

    $result = DB::table('pedidostmps')
        ->join('productos', 'productos.id', '= ', 'pedidostmps.id_producto')
        ->leftjoin('color', 'pedidostmps.id_color', '=', 'color.id_color')
        ->where('pedidostmps.id_cliente','=', $codigo)
        ->where('')
        ->get();

    return view('partials.ordertmp',['data' => $result]);
});

// Pedidos definitivos
Route::get('/orders', function (){
    //($orders = new \App\Http\Controllers\PedidosController();
    $codigo = Auth::user()->codigo_cliente;
    $orders = DB::table('pedidos')
        ->where('id_cliente','=', $codigo)
        ->orderBy('id', 'desc')
        ->get();

    return view('partials.order', ['order' => $orders]);
});


Route::get('/pdf', function (){

    $codigo = Auth::user()->codigo_cliente;
	//$codigo = '43000000924';
    //$id_order = Session::get('id_order');
    $id_order = 161;

    $get = DB::table('pedidosoks')
        ->join('productos', 'productos.id', '=', 'pedidosoks.id_producto')
        ->leftjoin('color', 'pedidosoks.id_color', '=', 'color.id_color')
        ->orderBy('productos.orden', 'desc')
        ->where([
            ['id_cliente', '=', $codigo],
            ['id_pedidook', '=', $id_order],
        ])
        ->get();

    $pedido = DB::table('pedidos')
        ->where([
            ['id_cliente', '=', $codigo],
            ['id', '=', $id_order],
        ])
        ->get();


    Session::forget('id_order');

    $pdf = PDF::loadView('emails.pdf', ['data' => $get, 'pedido' => $pedido]);
    return $pdf->stream();
});






Route::get('/clave', function (){
    $codigo = Auth::user()->codigo_cliente;
    $nombre = Auth::user()->name;
    $mail   = Auth::user()->email;

    $id_order = 2282;//Session::get('id_order');


    $get = DB::table('pedidosoks')
        ->join('productos', 'productos.id', '=', 'pedidosoks.id_producto')
        ->leftjoin('color', 'pedidosoks.id_color', '=', 'color.id_color')
        ->orderBy('productos.orden', 'asc')
        ->where([
            ['id_cliente', '=', $codigo],
            ['id_pedidook', '=', $id_order],
        ])
        ->get();

    $pedido = DB::table('pedidos')
        ->where([
            ['id_cliente', '=', $codigo],
            ['id', '=', $id_order],
        ])
        ->get();

    //Session::forget('id_order');

    //return view('emails.mail', ['data' => $get, 'pedido' => $pedido]);

    $pdf = PDF::loadView('emails.pdf', ['data' => $get, 'pedido' => $pedido]);
    //return $pdf->stream();

    Mail::send('emails.mail', ['html' => 'view', 'data' => $get, 'pedido' => $pedido], function ($message)
    use($pdf, $codigo, $nombre)
    {
        $codigoCliente = substr($codigo, -1, 4);

        $message->from('app@chupetesboann.com', 'App Boann');
        $message->to('pedidos@chupetesboann.com')->subject('Pedido '.$codigoCliente. ' '.$nombre);
        $message->attachData($pdf->output(), "pedido.pdf");
    });


});








Route::get('/telegram', function (){
    $user = \App\Pedidos::find(84);
    $user->notify(new \App\Notifications\BotTelegram());
});

Route::get('/contra', function (){
    $hola =\Illuminate\Support\Facades\Crypt::decrypt('$2y$10$BwfKutL1EGSuqmbgqfZP9OmT4izSN9RCNrCbKJ8JjRSVa23Z/tTCW');
    echo $hola;
});

Route::group(['before' => 'auth'], function (){
    Route::get('test', 'Backend\TestController@index');
});

/**
 * Acceso Backend
 */
Route::group(['middleware' => ['role:admin']],function(){
    Route::get('test', 'Backend\TestController@index');
    Route::get('gps', 'Backend\GetUserGpsController@gps')->name('users.gps');

    Route::resource('users', 'Backend\UserController');

    Route::get('datatable', 'Backend\UserTableController@index')->name('user.get');

    Route::get('users/{id}/geocode', 'Backend\GeocodeController@index');
    Route::get('users/add/geocodeAdd', 'Backend\GeocodeController@add')->name('geocodeAdd');

    Route::get('gpsdatatable', 'Backend\GpsTableController@index')->name('user.get.gps');
    Route::get('front', 'HomeController@index')->name('user.frontend');

    //Activate/deactivate user
    Route::get('users/{id}/activate', 'Backend\UserController@activate')->name('user.activate');
    Route::get('users/{id}/deactivate', 'Backend\UserController@deactivate')->name('user.deactivate');
    Route::get('user/reset', 'Backend\UserController@reset')->name('user.reset');
    Route::get('user/obtener', 'Backend\UserController@obtener')->name('user.obtener');
    Route::get('user/{id}/borrar', 'Backend\UserController@borraUsuario')->name('user.borrar');

    //Pedidos
    Route::get('listado-pedidos', 'Backend\Pedidos\PedidosBackendController@index')->name('users.pedidos');
    Route::post('listado-pedido', 'Backend\Pedidos\PedidosBackendController@filtro')->name('filtro.pedidos');
    Route::post('excel-taller', 'Backend\Pedidos\PedidosTallerController@index')->name('pedidos.taller');
    Route::post('textil', 'Backend\Pedidos\PedidosTallerController@csvTextil')->name('csv.taller');
    Route::post('multiusos', 'Backend\Pedidos\PedidosTallerController@csvMultiusos')->name('csv.multiusos');

    //Albaranes
    Route::post('excel-cabecera', 'Backend\Pedidos\PedidosAlbaranController@cabecera')->name('pedidos.cabecera');
    Route::post('excel-detalle', 'Backend\Pedidos\PedidosAlbaranController@detalle')->name('pedidos.detalle');

    //Ordenación por codigo cliente
    Route::get('oren-cliente', 'Backend\Pedidos\PedidosBackendController@orden')->name('order.by');
});

Route::get ('/permisos', 'PermisosController@index');


Route::get('/desencriptar', 'LuisController@desencriptarluis');





