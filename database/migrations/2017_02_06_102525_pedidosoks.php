<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pedidosoks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidosoks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_producto');
            $table->smallInteger('cantidad');
            $table->smallInteger('id_color');
            $table->string('tetina', 11);
            $table->string('talla', 11);
            $table->string('nombre', 200);
            $table->string('id_cliente', 11);
            $table->dateTime('fecha');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidosoks');
    }
}
