<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AnadeCodigoClienteTablaPedidostmps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pedidostmps', function (Blueprint $table) {
            $table->string('id_cliente', 20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pedidostmps', function (Blueprint $table){
           $table->dropColumn('id_cliente');
        });
    }
}
