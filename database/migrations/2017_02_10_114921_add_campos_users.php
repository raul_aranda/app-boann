<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('cp', 11)->nulleable(false);
            $table->string('poblacion', 255)->nulleable(false);
            $table->string('provincia', 100)->nulleable(false);
            $table->string('tlf', 20)->nulleable(false);
            $table->string('barras', 100)->nulleable(false);
            $table->string('agente', 11)->nulleable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table){
            $table->dropColumn('cp');
            $table->dropColumn('poblacion');
            $table->dropColumn('provincia');
            $table->dropColumn('tlf');
            $table->dropColumn('barras');
            $table->dropColumn('agente');
        });
    }
}
