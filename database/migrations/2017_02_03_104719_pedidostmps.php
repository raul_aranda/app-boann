<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pedidostmps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidostmps', function (Blueprint $table) {
            $table->increments('id_pedido');
            $table->integer('id_producto');
            $table->smallInteger('cantidad');
            $table->smallInteger('id_color');
            $table->string('tetina', 11);
            $table->string('talla', 11);
            $table->string('nombre', 200);
            $table->dateTime('fecha');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidostmps');
    }
}
