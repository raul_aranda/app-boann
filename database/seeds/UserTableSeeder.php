<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Add users to the table demo

        $demo = \App\Demo::all();

        foreach($demo as $item) {
            $users = [
                'name' => $item->nombre,
                'email' => $item->mail,
                'password' => bcrypt(''),
                'remember_token' => md5(uniqid(mt_rand(), true)),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'codigo_cliente' => $item->codigo,
                'direccion' => $item->direccion,
                'cp' => $item->cp,
                'poblacion' => $item->poblacion,
                'provincia' => $item->provincia,
                'tlf' => $item->tlf,
                'barras' => '',
                'agente' => '',
                'active' => '0',
            ];

            DB::table('users')->insert($users);
        }
    }
}
