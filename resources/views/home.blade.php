@extends('layouts.app')

@section('after-styles')
    {{ Html::style("bower_components/sweetalert/dist/sweetalert.css") }}
@stop
@section('content')
    {!! Html::script('/js/operations.js') !!}
    {!! Html::script('/js/bootbox.min.js') !!}
    <div id="content-section">
        <!-- Content section -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @if(Auth::user()->provincia == 'Asturias')
                        <img class="img-responsive sombraImagen" src="{{ url('img/banners/BANNER_asturias.jpg') }}">
                    @else
                        <img class="img-responsive sombraImagen" src="{{ url('img/nuevo_banner.jpg') }}">
                    @endif
                    <br>
                    <blockquote>
                        @php
                            $user = Auth::user()->name;
                            $user .= ' - '. Auth::user()->codigo_cliente;
                        @endphp
                        <p>Tienda: <span id="tienda">{{ $user }}</span></p>
                    </blockquote>
                </div>
            </div>
            <div class="oaerror info">
                <strong><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></strong> Si quieres cualquier producto sin nombre, incluir "sin personalizar" en el texto
            </div>
            <p></p>
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_a" data-toggle="tab">Líneas producto <span class="badge badge-important"><div id="orders_tmp"></div> </span></a></li>
                        <li><a href="#tab_b" data-toggle="tab">Pedidos <span class="badge"><div id="count_pedidos"></div> </span></a></li>
                        <li><a href="#tab_c" data-toggle="tab">Mensajes </a></li>
                        <li><a href="#tab_d" data-toggle="tab">Mis datos</a></li>
                    </ul>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_a">
                    <div class="row">
                        <div class="col-md-12">
                            <!--<h3>Artículos:</h3>-->


                            <!--<div class="records_content"></div>-->
                            <div class="table-container">
                                @include('partials.ordertmp')
                            </div>

                        </div>
                        <div class="col-md-12">
                            @include('partials.select')
                        </div>
                        <div class="col-md-12" style="padding-top: 20px;">
                            <div class="oaerror danger" id="msg">
                            </div>
                        </div>
                        <input type="hidden" id="txtTetina">
                        <input type="hidden" id="txtTalla">
                    </div>
                    <div class="form-group" style="padding-top: 10px;">
                        <label for="nombre" class="col-sm-2 control-label">Comentarios</label>
                        <div class>
                            <input type="text" id="comentario" placeholder="Comentarios sobre el pedido" class="form-control"/>
                        </div>
                    </div>
                    <div class="modal-footer" id="procesar">
                        <button type="button" class="btn btn-primary" onclick="addOrder()">Procesar pedido</button>
                    </div>
                </div>

                <div class="tab-pane" id="tab_b" >
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-order">
                                @include('partials.order')
                            </div>

                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab_c">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="pdf_content">hola</div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab_d">
                    @include('partials.datos')
                </div>
            </div><!-- tab content -->

        </div>

        <div id="result">

        </div>
        <!-- /Content section -->
    </div>
@endsection

@section('after-scripts')
    {{ Html::script("bower_components/sweetalert/dist/sweetalert.min.js") }}

    <script type="text/javascript">
       /* swal({
                    title: "¿Desea finalizar el pedido?",
                    text: "En caso de finalizar, no podrá realizar aumentos sobre este pedido.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sí, aceptar",
                    cancelButtonText: "No, cancelar",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){
                    if (isConfirm) {
                        swal("Procesado", "Su pedido ha sido procesaddo", "success");
                    } else {
                        swal("Cancelado", "Su pedido no ha sido procesado :)", "error");
                    }
                });*/
    </script>

@stop
