<html>
<head></head>
<body>
<h1>{{Auth::user()->codigo_cliente}}</h1>
<p>contenido</p>
<?php
?>
@foreach($data as $row)
    {{$row->id_pedidook}}
    {{$row->descripcion}}
    {{$row->cantidad}}
    {{$row->descripcion_color}}
    {{$row->tetina}}
    {{$row->talla}}
    {{$row->nombre}}
@endforeach
<table class="table table-mail" style="width: 100%; margin-top: 10px;">
    <tbody>
    <tr>
        <td class="space" style="width: 20px; padding: 7px 0;"> </td>
        <td style="padding: 7px 0;" align="center">
            <table class="table" style="width: 100%;" bgcolor="#ffffff">
                <tbody>
                <tr>
                    <td class="logo" style="border-bottom: 4px solid #333333; padding: 7px 0;" align="center"><a title="{shop_name}" href="{shop_url}" style="color: #337ff1;"> <img src="{shop_logo}" alt="{shop_name}" /></a></td>
                </tr>
                <tr>
                    <td class="titleblock" style="padding: 7px 0;" align="center"><span style="color: #555454; font-family: 'Open-sans', sans-serif; font-size: small;"> <span class="title" style="font-weight: 500; font-size: 28px; text-transform: uppercase; line-height: 33px;">Hoja de pedido</span><br /> </span></td>

                </tr>
                <tr>
                    <td class="space_footer" style="padding: 0;"> </td>
                </tr>
                <tr>
                    <td class="box" style="border: 1px solid #D6D4D4; background-color: #f8f8f8; padding: 7px 0;">
                        <table class="table" style="width: 100%;">
                            <tbody>
                            <tr>
                                <td style="padding: 7px 0;" width="10"> </td>
                                <td style="padding: 7px 0;"><span style="color: #555454; font-family: 'Open-sans', sans-serif; font-size: small;"> </span>
                                    <p style="border-bottom: 1px solid #D6D4D4; margin: 3px 0 7px; text-transform: uppercase; font-weight: 500; font-size: 18px; padding-bottom: 10px;">Detalles del pedido</p>
                                    <span style="color: #777;"> <span style="color: #333;"><strong>Pedido:</strong></span> {order_name} Situado en {date}<br /><br /><span style="color: #333;"><strong>Pago:</strong></span> {payment} </span></td>
                                <td style="padding: 7px 0;" width="10"> </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 7px 0;"><span style="color: #555454; font-family: 'Open-sans', sans-serif; font-size: small;"> </span>
                        <table class="table table-recap" style="width: 100%; border-collapse: collapse;" bgcolor="#ffffff"><!-- Title -->
                            <tbody>
                            <tr><th style="border: 1px solid #D6D4D4; background-color: #fbfbfb; color: #333; font-family: Arial; font-size: 13px; padding: 10px;" bgcolor="#f8f8f8">Referencia</th><th style="border: 1px solid #D6D4D4; background-color: #fbfbfb; color: #333; font-family: Arial; font-size: 13px; padding: 10px;" bgcolor="#f8f8f8">Producto</th><th style="border: 1px solid #D6D4D4; background-color: #fbfbfb; color: #333; font-family: Arial; font-size: 13px; padding: 10px;" width="17%" bgcolor="#f8f8f8">Precio unitario</th><th style="border: 1px solid #D6D4D4; background-color: #fbfbfb; color: #333; font-family: Arial; font-size: 13px; padding: 10px;" bgcolor="#f8f8f8">Cantidad</th><th style="border: 1px solid #D6D4D4; background-color: #fbfbfb; color: #333; font-family: Arial; font-size: 13px; padding: 10px;" width="17%" bgcolor="#f8f8f8">Precio total</th></tr>
                            <tr>
                                <td colspan="5" style="border: 1px solid #D6D4D4; text-align: center; color: #777; padding: 7px 0;">  {products}</td>
                            </tr>
                            <tr>
                                <td colspan="5" style="border: 1px solid #D6D4D4; text-align: center; color: #777; padding: 7px 0;">  {discounts}</td>
                            </tr>
                            <tr class="conf_body">
                                <td colspan="4" style="border: 1px solid #D6D4D4; color: #333; padding: 7px 0;" bgcolor="#f8f8f8">
                                    <table class="table" style="width: 100%; border-collapse: collapse;">
                                        <tbody>
                                        <tr>
                                            <td style="color: #333; padding: 0;" width="10"> </td>
                                            <td style="color: #333; padding: 0;" align="right"><span style="color: #555454; font-family: 'Open-sans', sans-serif; font-size: small;"> <strong>Productos</strong> </span></td>
                                            <td style="color: #333; padding: 0;" width="10"> </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td colspan="4" style="border: 1px solid #D6D4D4; color: #333; padding: 7px 0;" align="right" bgcolor="#f8f8f8">
                                    <table class="table" style="width: 100%; border-collapse: collapse;">
                                        <tbody>
                                        <tr>
                                            <td style="color: #333; padding: 0;" width="10"> </td>
                                            <td style="color: #333; padding: 0;" align="right"><span style="color: #555454; font-family: 'Open-sans', sans-serif; font-size: small;"> {total_products} </span></td>
                                            <td style="color: #333; padding: 0;" width="10"> </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr class="conf_body">
                                <td colspan="4" style="border: 1px solid #D6D4D4; color: #333; padding: 7px 0;" bgcolor="#f8f8f8">
                                    <table class="table" style="width: 100%; border-collapse: collapse;">
                                        <tbody>
                                        <tr>
                                            <td style="color: #333; padding: 0;" width="10"> </td>
                                            <td style="color: #333; padding: 0;" align="right"><span style="color: #555454; font-family: 'Open-sans', sans-serif; font-size: small;"> <strong>Descuentos</strong> </span></td>
                                            <td style="color: #333; padding: 0;" width="10"> </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td colspan="4" style="border: 1px solid #D6D4D4; color: #333; padding: 7px 0;" bgcolor="#f8f8f8">
                                    <table class="table" style="width: 100%; border-collapse: collapse;">
                                        <tbody>
                                        <tr>
                                            <td style="color: #333; padding: 0;" width="10"> </td>
                                            <td style="color: #333; padding: 0;" align="right"><span style="color: #555454; font-family: 'Open-sans', sans-serif; font-size: small;"> {total_discounts} </span></td>
                                            <td style="color: #333; padding: 0;" width="10"> </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr class="conf_body">
                                <td colspan="4" style="border: 1px solid #D6D4D4; color: #333; padding: 7px 0;" bgcolor="#f8f8f8">
                                    <table class="table" style="width: 100%; border-collapse: collapse;">
                                        <tbody>
                                        <tr>
                                            <td style="color: #333; padding: 0;" width="10"> </td>
                                            <td style="color: #333; padding: 0;" align="right"><span style="color: #555454; font-family: 'Open-sans', sans-serif; font-size: small;"> <strong>Papel de regalo</strong> </span></td>
                                            <td style="color: #333; padding: 0;" width="10"> </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td colspan="4" style="border: 1px solid #D6D4D4; color: #333; padding: 7px 0;" bgcolor="#f8f8f8">
                                    <table class="table" style="width: 100%; border-collapse: collapse;">
                                        <tbody>
                                        <tr>
                                            <td style="color: #333; padding: 0;" width="10"> </td>
                                            <td style="color: #333; padding: 0;" align="right"><span style="color: #555454; font-family: 'Open-sans', sans-serif; font-size: small;"> {total_wrapping} </span></td>
                                            <td style="color: #333; padding: 0;" width="10"> </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr class="conf_body">
                                <td colspan="4" style="border: 1px solid #D6D4D4; color: #333; padding: 7px 0;" bgcolor="#f8f8f8">
                                    <table class="table" style="width: 100%; border-collapse: collapse;">
                                        <tbody>
                                        <tr>
                                            <td style="color: #333; padding: 0;" width="10"> </td>
                                            <td style="color: #333; padding: 0;" align="right"><span style="color: #555454; font-family: 'Open-sans', sans-serif; font-size: small;"> <strong>Transporte</strong> </span></td>
                                            <td style="color: #333; padding: 0;" width="10"> </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td colspan="4" style="border: 1px solid #D6D4D4; color: #333; padding: 7px 0;" bgcolor="#f8f8f8">
                                    <table class="table" style="width: 100%; border-collapse: collapse;">
                                        <tbody>
                                        <tr>
                                            <td style="color: #333; padding: 0;" width="10"> </td>
                                            <td style="color: #333; padding: 0;" align="right"><span style="color: #555454; font-family: 'Open-sans', sans-serif; font-size: small;"> {total_shipping} </span></td>
                                            <td style="color: #333; padding: 0;" width="10"> </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr class="conf_body">
                                <td colspan="4" style="border: 1px solid #D6D4D4; color: #333; padding: 7px 0;" bgcolor="#f8f8f8">
                                    <table class="table" style="width: 100%; border-collapse: collapse;">
                                        <tbody>
                                        <tr>
                                            <td style="color: #333; padding: 0;" width="10"> </td>
                                            <td style="color: #333; padding: 0;" align="right"><span style="color: #555454; font-family: 'Open-sans', sans-serif; font-size: small;"> <strong>Impuesto total</strong> </span></td>
                                            <td style="color: #333; padding: 0;" width="10"> </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td colspan="4" style="border: 1px solid #D6D4D4; color: #333; padding: 7px 0;" bgcolor="#f8f8f8">
                                    <table class="table" style="width: 100%; border-collapse: collapse;">
                                        <tbody>
                                        <tr>
                                            <td style="color: #333; padding: 0;" width="10"> </td>
                                            <td style="color: #333; padding: 0;" align="right"><span style="color: #555454; font-family: 'Open-sans', sans-serif; font-size: small;"> {total_tax_paid} </span></td>
                                            <td style="color: #333; padding: 0;" width="10"> </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr class="conf_body">
                                <td colspan="4" style="border: 1px solid #D6D4D4; color: #333; padding: 7px 0;" bgcolor="#f8f8f8">
                                    <table class="table" style="width: 100%; border-collapse: collapse;">
                                        <tbody>
                                        <tr>
                                            <td style="color: #333; padding: 0;" width="10"> </td>
                                            <td style="color: #333; padding: 0;" align="right"><span style="color: #555454; font-family: 'Open-sans', sans-serif; font-size: small;"> <strong>Pago total</strong> </span></td>
                                            <td style="color: #333; padding: 0;" width="10"> </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td colspan="4" style="border: 1px solid #D6D4D4; color: #333; padding: 7px 0;" bgcolor="#f8f8f8">
                                    <table class="table" style="width: 100%; border-collapse: collapse;">
                                        <tbody>
                                        <tr>
                                            <td style="color: #333; padding: 0;" width="10"> </td>
                                            <td style="color: #333; padding: 0;" align="right"><span style="color: #555454; font-family: 'Open-sans', sans-serif; font-size: large;"> {total_paid} </span></td>
                                            <td style="color: #333; padding: 0;" width="10"> </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td class="space_footer" style="padding: 0;"> </td>
                </tr>
                <tr>
                    <td style="padding: 7px 0;">
                        <table class="table" style="width: 100%;">
                            <tbody>
                            <tr>
                                <td class="box address" style="border: 1px solid #D6D4D4; background-color: #f8f8f8; padding: 7px 0;" width="310">
                                    <table class="table" style="width: 100%;">
                                        <tbody>
                                        <tr>
                                            <td style="padding: 7px 0;" width="10"> </td>
                                            <td style="padding: 7px 0;"><span style="color: #555454; font-family: 'Open-sans', sans-serif; font-size: small;"> </span>
                                                <p style="border-bottom: 1px solid #D6D4D4; margin: 3px 0 7px; text-transform: uppercase; font-weight: 500; font-size: 18px; padding-bottom: 10px;">Dirección de entrega</p>
                                                <span style="color: #777;"> {delivery_block_html} </span></td>
                                            <td style="padding: 7px 0;" width="10"> </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td class="space_address" style="padding: 7px 0;" width="20"> </td>
                                <td class="box address" style="border: 1px solid #D6D4D4; background-color: #f8f8f8; padding: 7px 0;" width="310">
                                    <table class="table" style="width: 100%;">
                                        <tbody>
                                        <tr>
                                            <td style="padding: 7px 0;" width="10"> </td>
                                            <td style="padding: 7px 0;"><span style="color: #555454; font-family: 'Open-sans', sans-serif; font-size: small;"> </span>
                                                <p style="border-bottom: 1px solid #D6D4D4; margin: 3px 0 7px; text-transform: uppercase; font-weight: 500; font-size: 18px; padding-bottom: 10px;">Dirección de facturación</p>
                                                <span style="color: #777;"> {invoice_block_html} </span></td>
                                            <td style="padding: 7px 0;" width="10"> </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="footer" style="border-top: 4px solid #333333; padding: 7px 0;"><span><a href="{shop_url}" style="color: #337ff1;">distribucion@chupetesboann.com</a></span></td>
                </tr>
                </tbody>
            </table>
        </td>
        <td class="space" style="width: 20px; padding: 7px 0;"> </td>
    </tr>
    </tbody>
</table>
</body>
</html>