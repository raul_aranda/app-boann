<!doctype html>
<html>
<head>
  <meta name="viewport" content="width=device-width" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

  <style>
    #details {
      /*border-collapse: collapse;*/
      margin-bottom: 0.8em;
      border: 1px solid #000000;
    }

    #details th {
      font-size: 13px;
      font-weight: normal;
      padding-left: 3px;
      margin-top: 5px;
      margin-bottom: 5px;
      border: 1px solid #000000;
    }

    #details td {
      /*border-bottom: 1px solid #eee;
      border-top: 1px solid transparent;*/
      padding-left: 3px;
      padding-right: 15px;
      /*box-sizing: border-box;*/
      font-size: 13px;
      border: 1px solid #000000;
    }

    /*#details tr:nth-child(odd) td {
      background-color: #f6f6f6;
    }

    #details tr:nth-child(even) td {
      background-color: #ffffff;
    }*/
    .margen{
      margin-top: 2px;
      margin-bottom: 2px;
    }
    .datos{
      font-size: 13px;
    }
    @page {
      margin-top: 0.6em;
      /*margin-left: 0.6em;*/
    }
    .color{
      color: red;
      font-weight: bolder;
    }

    .footer {
      position: fixed;
      left: 0;
      bottom: 0;
      width: 100%;
      text-align: center;
      padding-bottom: 25px;
    }

  </style>
  <!-- Latest compiled and minified CSS -->
{!! Html::style('/bootstrap-3.3.7-dist/css/bootstrap.css') !!}


<!-- Styles -->
  {!! Html::style('/css/mycss.css') !!}
</head>

@foreach($pedido as $row)

@endforeach
@php
  //$valueNIF = str_limit(Auth::user()->barras, 9);
  $valueNIF = substr(Auth::user()->barras, 3, 9);
  /*$numPedido  = substr($row->fecha, 0, 4);
  $numPedido .= "-0" . $row->num_art;
  $numPedido .= "0" . $row->id;
  $numPedido .= "-" . substr($row->fecha,9, 2);*/

  $numPedido  = substr($row->fecha, 0, 4);
  $numPedido .= "-". $row->id;
@endphp
<body>
<table width="100%">
  <tbody>
  <tr>
    <td width="50%"><h3><strong>Datos del cliente</strong></h3><br></td>
    <td colspan="2"><h3>HOJA DE PEDIDO</h3><br>
    </td>
  </tr>
  <tr>
    <td width="50%"><strong>Nombre: </strong> {{Auth::user()->name}}</td>
    <td colspan="2"><h3 style="display: inline-block; margin-top: -10px;"># {{ str_pad($numPedido,12,"0", STR_PAD_LEFT) }}</h3></td>
  </tr>
  <tr class="datos">
    <td width="50%">
      <strong>Código Cliente: </strong> {{Auth::user()->codigo_cliente}} </strong><br>
      <h4>
      </h4></td>
    <td colspan="2">
      <strong>Fecha del pedido</strong>
      {{ $row->fecha }}<br>
    </td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">
      <strong>Detalles del pedido</strong>
    </td>
  </tr>
  <tr>
    <td colspan="3"><table width="100%" id="details">
        <tbody>
        <tr class="margen">
          <th width="2%" align="center"><strong>ID</strong></th>
          <th width="30%"><strong>Artículo</strong></th>
          <th width="5%" align="center"><strong>Uds</strong></th>
          <th width="20%"><strong>Color</strong></th>
          <th width="10%"><strong>Tetina</strong></th>
          <th width="10%"><strong>Talla</strong></th>
          <th width="23%"><strong>Nombre</strong></th>
        </tr>
        @php
          $count = 0
        @endphp
        @foreach($data as $line)
          @php
            $count = $count + $line->cantidad;
            if (!$line->modelo ) $modelo = '';
            else $modelo = $line->modelo;
          @endphp
          <tr>
            <td width="2%" align="center"> <div class="margen">{{$count}} </div></td>
            <td width="30%"> <div class="margen">{{ $line->descripcion . ' ' .$modelo }} </div></td>
            <td width="5%" align="center">
              @if ($line->cantidad > 1)
                <div class="margen color">{{ $line->cantidad }} </div>
              @else
                <div class="margen">{{ $line->cantidad }} </div>
              @endif
            </td>
            <td width="20%"> <div class="margen">{{ $line->codigo_color . ' ' . $line->descripcion_color }}</div> </td>
            <td width="10%"> <div class="margen">{{ $line->tetina }} </div></td>
            <td width="10%"> <div class="margen">{{ $line->talla }}</div></td>
            <td width="23%"> <div class="margen">{{ $line->nombre }} </div></td>
          </tr>

        @endforeach
      </table></td>
  </tr>
  <tr>
    <td colspan="3"></td>
  </tr>
  <tr class="datos">
    <td colspan="3" align="right"><strong>Total: <strong> {{ $count . " Artículos" }}</strong></strong></td>
  </tr>
  <tr class="datos">
    <td colspan="3"><strong>Comentarios: </strong>@if($row->comentarios == '')

        Ningún comentario

      @else

        {{ $row->comentarios }}

      @endif</td>
  </tr>

  </tbody>
</table>
<footer class="footer">
  <div class="container-fluid">
    @foreach($dropShipping as $drop)
      <strong>Nombre: </strong> {{$drop->nombre}}
      <strong>Apellidos: </strong> {{$drop->apellido1. ' ' . $drop->apellido2}}
      <strong>Teléfono: </strong> {{$drop->telefono}}
      <strong>Cp: </strong> {{$drop->cp}} <br>
      <strong>Provincia: </strong> {{$drop->provincia}}
      <strong>Localidad: </strong> {{$drop->localidad}}
      <strong>Dirección: </strong> {{$drop->via . ' '.$drop->calle.' '.$drop->numero}}
      <strong>Resto: </strong>{{$drop->resto}}
    @endforeach
  </div>
</footer>
<br>

</body>
</html>