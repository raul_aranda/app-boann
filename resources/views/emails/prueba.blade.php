@extends('layouts.app')

@section('content')

<style>
    .invoice-title h2, .invoice-title h3 {
        display: inline-block;
    }

    .table > tbody > tr > .no-line {
        border-top: none;
    }

    .table > thead > tr > .no-line {
        border-bottom: none;
    }

    .btmth {
        border-bottom: 2px solid #ddd;
    }

    .btm {
        border-bottom: 1px solid #ddd;
    }

</style>
<!-- Styles -->
{!! Html::style('/css/mycss.css') !!}

@foreach($pedido as $row)

@endforeach
@php
    //$valueNIF = str_limit(Auth::user()->barras, 9);
    $valueNIF = substr(Auth::user()->barras, 3, 9);
    $numPedido  = substr($row->fecha, 0, 4);
    $numPedido .= "-0" . $row->num_art;
    $numPedido .= "0" . $row->id;
    $numPedido .= "-" . substr($row->fecha,9, 2);
@endphp

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-6">
                    <img src="{{url('/img/logo_facturas.jpg')}}" class="img-responsive" alt="">
                </div>

                <div class="col-xs-2">
                </div>

                <div class="col-xs-4">
                    <h2 class="pull-right" style="display: inline-block;">HOJA DE PEDIDO</h2>
                    <h2 class="pull-right" style="display: inline-block; margin-top: -10px;"># {{ str_pad($numPedido,12,"0", STR_PAD_LEFT) }}</h2>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-xs-6">
                    <address>
                        <h4><strong>Datos del cliente</strong></h4>
                        <strong>Nombre: </strong> {{Auth::user()->name}}<br/>
                        <strong>Dirección: </strong> {{ Auth::user()->direccion }} <br/>
                        <strong>Población: </strong> {{ Auth::user()->poblacion }} <br/>
                        <strong>Provincia: </strong> {{ Auth::user()->provincia }} <br/>
                        <strong>E-mail: </strong> {{ Auth::user()->email }}
                    </address>
                </div>
                <div class="col-xs-6 text-right">
                    <address>
                        <strong></strong><br>
                        <i class="fa fa-user" aria-hidden="true"> <strong>Nif/Cif: </strong> {{$valueNIF}} </i><br>
                        <i class="fa fa-envelope-o" aria-hidden="true"></i> <strong>Código Cliente: </strong> {{Auth::user()->codigo_cliente}} <br>
                        <i class="fa fa-mobile" aria-hidden="true"></i><strong>Teléfono: </strong> {{Auth::user()->tlf}}<br>
                    </address>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">

                    <address>
                        <strong>Fecha del pedido</strong><br>
                        {{ $row->fecha }}<br><br>
                    </address>
                </div>
                <div class="col-xs-6 text-right">
                    <address>
                        <strong>Datos Bancarios</strong><br>
                        {{--@if(count(1)>0)
                            {!! nl2br(1) !!}
                        @else
                            No bank details have been submitted.
                        @endif--}}
                    </address>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="background: #683584; color: #fff;">
                    <h3 class="panel-title"><strong>Detalles del pedido</strong></h3>
                </div>

                <div class="panel-body">

                    <div class="row btmth">
                        <div class="col-xs-1">
                            <strong>ID</strong>
                        </div>
                        <div class="col-xs-2">
                            <strong>Artículo</strong>
                        </div>
                        <div class="col-xs-1">
                            <strong>Uds</strong>
                        </div>
                        <div class="col-xs-2">
                            <strong>Color</strong>
                        </div>
                        <div class="col-xs-1">
                            <strong>Tetina</strong>
                        </div>
                        <div class="col-xs-2">
                            <strong>Talla</strong>
                        </div>
                        <div class="col-xs-3">
                            <strong>Nombre</strong>
                        </div>
                    </div>
                    @php
                        $count = 0
                    @endphp
                    @foreach($data as $line)
                        @php
                            $count++
                        @endphp
                        <div class="row btm">
                            <div class="col-xs-1">
                                {{$count}}
                            </div>
                            <div class="col-xs-2">
                                {{ $line->descripcion .' ' . $line->modelo}}
                            </div>
                            <div class="col-xs-1">
                                {{ $line->cantidad }}
                            </div>
                            <div class="col-xs-2">
                                {{ $line->codigo_color . ' ' .$line->descripcion_color }}
                            </div>
                            <div class="col-xs-1">
                                {{ $line->tetina }}
                            </div>
                            <div class="col-xs-2">
                                {{ $line->talla }}
                            </div>
                            <div class="col-xs-3">
                                {{ $line->nombre }}
                            </div>
                        </div>
                    @endforeach

                    <div class="row">
                        <div class="col-xs-1">

                        </div>
                        <div class="col-xs-9 text-right">
                            <strong>Total</strong>
                        </div>
                        <div class="col-xs-2 text-right">
                            <strong> {{ $count . " Artículos" }}</strong>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-xs-1">

                        </div>
                        <div class="col-xs-1">
                            <strong>Comentarios: </strong>
                        </div>
                        <div class="col-xs-9">
                            @if($row->comentarios == '')
                                <div class="col-xs-9">
                                    Ningún comentario
                                </div>
                            @else
                                <div class="col-xs-9">
                                    {{ $row->comentarios }}
                                </div>
                            @endif

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12" align="center" style="padding-bottom: 15px;">
            pedidos@chupetesboann.com - 952 702 267
        </div>
    </div>

</div>


@endsection