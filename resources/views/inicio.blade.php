@extends('layouts.home')

@section('after-styles')

    {{ Html::style("bower_components/sweetalert/dist/sweetalert.css") }}
    <style type="text/css">
       .modal-contentDrop{
           background-color: #fefefe;
           margin: 5% auto; /* 15% from the top and centered */
           /*padding: 20px;*/
           border: 1px solid #888;
           width: 800px
       }
       

        /* Botón cerrar */
        .close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }

        .modal-headerDrop {
            background-color: #822193;
            border-radius: .286rem .286rem 0 0;
            padding: 15px 20px;
            border-bottom: none;
        }
        #articulo{
            font-weight: bold;
        }
        .label{
            display: inline-block;
            width: 50px;
        }
        .control-label{
            text-align: right;
            font-weight: bold;
            padding-top: 10px;
        }

        .formulario{
            background-color: #f1f4f5;
            padding: 5px;
            border: 1px solid #eee;
            border-radius: 3px;
            margin: 0 auto;
            font-size: 14px;
            color: #6d3682;
        }
        .col-md-2,
        .col-md-3,
        .col-md-4,
        .col-md-5,
        .col-md-7,
        .col-md-8,
        .col-md-6{
            padding-right: 0px;
        }

        form label.error {
            float: left;
            color: red;
            font-weight: lighter;
            padding-left: .5em;
            vertical-align: top;
            font-size: 12px;
        }

        #provincia, #localidad{font-weight: bold;}

       @media (min-width:1170px){
            .modelo{
                padding-left: 1px;
                padding-right: 1px;
            }

           #option1{width: 150px;}
           #contador{width: 70px;}
           #option2{width: 180px;}
           #select{padding-right: 5px;}
           #option3{width: 190px;}
           #option4{width: 180px; padding-left: 5px;}
           #optionButton{width: 30px;}
           #dibujo, #opcion{width: 170px;}
       }

    </style>
@stop
@section('content')

    @include('partials.modal')
    <div id="content-section">
        <!-- Content section -->
        <div class="container">
            <!--<div class="row">
                <div class="col-md-12">
                    <img class="img-responsive sombraImagen" src="{{ url('img/banner/dia_padre.jpg') }}">
                    <br>

                </div>
            </div>-->
            <div class="panel panel-bordered">
                <div class="panel-heading">
                    @php
                        $user = Auth::user()->name;
                        $user .= ' - '. Auth::user()->codigo_cliente;
                    @endphp
                    <input type="hidden" value="{{ Auth::user()->envio }}" id="txtTrans">
                    <div class="pull-right oaerror info">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        Si quieres cualquier producto sin nombre, incluir <span class="sin">"sin personalizar" </span>en el texto
                    </div>
                    <h3 class="panel-title">{{ $user }}</h3>
                </div><!-- /.box-header -->
                <div class="panel-toolbar">
                    {{--@if(($_ENV['CATALOGO_ACTIVO'] == 'true'))
                        @if(\App\Modules\Catalogo\Http\Controllers\CatalogoController::compruebaCatalogoCliente(Auth::user()->codigo_cliente) == 0)
                            <div class="opciones" style="margin-top: 10px">
                                <img class="img-responsive" src="{{ url('img/banner/catalogonuevo.jpg') }}">
                            </div>
                        @endif
                    @endif--}}
                </div>
                <div class="panel-body container-fluid">
                    <div class="nav-tabs-horizontal" role="tablist">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_a" data-toggle="tab">Líneas producto <span class="badge badge-important"><div id="orders_tmp"></div> </span></a>
                            </li>
                            <li>
                                <a href="#tab_b" data-toggle="tab">Pedidos <span class="badge"><div id="count_pedidos"></div> </span></a>
                            </li>
                            <li>
                                <a href="#tab_c" data-toggle="tab">Mensajes
                                    <span class="badge badge-danger">
                                        <div id="count_messages"></div>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#tab_d" data-toggle="tab">Mis datos</a>
                            </li>
                            <li>
                                <a href="#tab_e" data-toggle="tab">Catálogo</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_a" role="tabpanel">
                                <div class="table-container"></div>
                                @section('mensajes')
                                    <div id="msg" style="padding-bottom: 10px;">
                                    </div>
                                @show


                                <div class="opciones">
                                    <div class="form-group" style="padding-bottom: 15px;">
                                        @include('frontend::partials.opciones')
                                    </div>
                                </div>
                                <div class="opciones" style="margin-top: 10px; text-align: right;">
                                    <input type="hidden" id="txtEnvio">
                                    <input type="hidden" id="txtDescuento">
                                    <input type="hidden" id="txtMinDescuento">
                                    <input type="hidden" id="txtCodEnvio">
                                    <input type="hidden" id="idUser" value="{{ Auth::user()->codigo_cliente }}">
                                    <div id="gastosEnvio"></div>
                                    <div id="total"></div>

                                </div>
                                <div class="opciones right" style="margin-top: 10px">
                                    <div class="form-group" style="padding-top: 10px;">
                                        <label for="nombre">Comentarios</label>
                                        <div>
                                            <input type="text" id="comentario" placeholder="Comentarios sobre el pedido" class="form-control"/>
                                        </div>
                                    </div>
                                    <div style="text-align: right;">
                                        <!-- Button drop shipping modal -->
                                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#dropshippingModal">
                                            Drop shipping
                                        </button>
                                        <!--<div class="" id="procesar" style="margin-top: 20px;">
                                            <button type="button" class="btn btn-primary" onclick="addOrder()">Procesar pedido</button>
                                        </div>-->
                                        <button type="button" class="btn btn-primary" onclick="addOrder()" id="procesar">Procesar pedido</button>
                                    </div>

                                </div>
                            </div>

                            <div class="tab-pane" id="tab_b" >
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-order">
                                            @include('frontend::partials.pedidos')
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_c">
                                <div class="row">
                                    <div class="col-md-12">
                                        @section('mensajes-user')
                                            <div id="noticias"></div>
                                        @show
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_d">
                                @include('partials.datos')
                            </div>
                            <div class="tab-pane" id="tab_e">
                                <iframe src="https://onedrive.live.com/embed?cid=CC839941308D77D5&resid=CC839941308D77D5%214981&authkey=ACGXUnbOTr4M7t4" width="170" height="128" frameborder="0" scrolling="no"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="result">
            <input type="hidden" id="txtCount">
        </div>
        <!-- /Content section -->
    </div>
@endsection

@section('after-scripts')
    {!! Html::script('/js/operations.js?ver=2.6') !!}
    {!! Html::script('/js/bootbox.min.js') !!}
    {{ Html::script("bower_components/sweetalert/dist/sweetalert.min.js") }}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>

    <script type="text/javascript">
        $('#producto').select2();

        $(document).ready(function () {
            $('div.table-container').load('frontend/pedidos', function () {
                $('div.table-container').fadeIn();
            });
            $("#producto").prop('selectedIndex', 0);
            $('#producto').trigger('change'); //init first product

            ajaxRenderMensajes('mensajes/noti');
            countMessages();

            $("#formDropShipping").validate({
                rules: {
                    nombre: { required: true, minlength: 2 },
                    apellido1: { required:true, minlength: 4 },
                    apellido2: { required:true, minlength: 4 },
                    telefono: { required:true, minlength: 2, maxlength: 15 },
                    cp: { required:true, minlength: 5 },
                    localidad: { required:true, minlength: 2 },
                    via: { required:true},
                    calle: { required:true, minlength: 2 },
                    numero: { required:true, minlength: 1 }
                },
                messages: {
                    nombre: "El campo es obligatorio.",
                    apellido1: "Primer apellido obligatorio",
                    apellido2: "Segundo apellido obligatorio",
                    telefono : "El campo Teléfono no contiene un formato correcto.",
                    cp: "Código postal requerido",
                    localidad : "Localidad obligatoria",
                    via : "Requerido",
                    calle: "Requerido",
                    numero: "Requerido",
                },
                submitHandler: function(form){
                    var nom = $("#nombre").val();
                    var ape1 = $("#apellido1").val();
                    var ape2 = $("#apellido2").val();
                    var tel = $("#telefono").val();
                    var cpo = $("#cpo").val();
                    var pro = $("#provincia").val();
                    var loc = $("#localidad").val();
                    var via = $("#via").val();
                    var cal = $("#calle").val();
                    var num = $("#numero").val();
                    var res = $("#resto").val();

                    var lineas = $("#orders_tmp").text();
                    swal({
                            title: "¿Está segura/o que desea finalizar el pedido?",
                            text: "No se podrán hacer aumentos",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Sí, aceptar",
                            cancelButtonText: "No, cancelar",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        },
                        function(isConfirm){
                            if (isConfirm) {
                                if (lineas > 0){
                                    swal("Procesado", "Su pedido ha sido procesado", "success");
                                    $.ajax({
                                        type: "post",
                                        url: "dropshipping",
                                        contetnType:"application_json; charset=utf-8",
                                        data:
                                            { "_token": "{{ csrf_token() }}", nom:nom, ape1:ape1, ape2:ape2, tel:tel, cpo:cpo,
                                                pro:pro, loc:loc, via:via, cal:cal, num:num, res:res },
                                        success: function() {
                                            /*$("#nombre").val('');
                                            $("#ape1").val('');
                                            $("#ape2").val('');*/
                                            //modal.style.display = "none";
                                            confirmAddRecord();

                                        }
                                    });
                                }else{
                                    swal("Error", "No ha añadido ninguna línea de producto", "error");
                                }
                            } else {
                                swal("Cancelado", "Su pedido no ha sido procesado :)", "error");
                            }
                        });
                }
            });
        });
/*
        // Get the modal
        var modal = document.getElementById("myModal");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];
        var cierra = document.getElementById("closeModal");
        var cancela = document.getElementById("cancelar");

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // Close modal
        cierra.onclick = function(){
            modal.style.display = "none";
        }

        // Close modal button cancel
        cancela.onclick = function(){
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }

        // Show modal
        function dropShipping() {
            modal.style.display = "block";
        }*/

        $('#cpo').blur(function (){
            var cp = $(this).val();
            $('#localidad').empty();
            $('#provincia').empty();

            $.ajax({
                url:'dropshipping/localidad',
                type: 'post',
                data: {
                    "_token": "{{ csrf_token() }}",
                    'cp':cp
                },
                success: function (data){
                    $.each(data, function(key, value) {
                        $('#localidad').append($('<option>').text(value.poblacion).attr('value', value.poblacion));
                    });
                },
                error: function () {
                    alert ("Error en el Código Postal");
                }
            })

            $.ajax({
                url:'dropshipping/provincia',
                type: 'post',
                data: {
                    "_token": "{{ csrf_token() }}",
                    'cp':cp
                },
                success: function (data){
                    $('#provincia').append(new Option(data, data));
                },
                error: function () {
                    alert ("Error en la provincia");
                }
            })


            //$("#localidad").val(city);
        });
    </script>
@stop
