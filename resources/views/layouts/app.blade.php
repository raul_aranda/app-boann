<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Chupetes Boann') }}</title>


    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.1.1.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    {!! Html::style('/bootstrap-3.3.7-dist/css/bootstrap.css') !!}

    <!-- Latest compiled and minified JavaScript -->
    {!! Html::script('/bootstrap-3.3.7-dist/js/bootstrap.min.js') !!}


    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>

    <!-- Font Awesome -->
    {!! Html::style('/font-awesome-4.7.0/css/font-awesome.min.css') !!}

    <!-- Styles -->
    {!! Html::style('/css/mycss.css') !!}

    <!-- Styles -->
    @yield('before-styles')

    @yield('after-styles')

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top navbar-custom">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <!--<a class="navbar-brand" href="{{-- url('/') --}}">-->
                    <a  href="{{ url('/') }}">
                        {{-- config('app.name', 'Chupetes Boann') --}}
                        <img src="{{url('/img/logo.png')}}" alt="" width="200px">
                    </a>

                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        @role('admin')
                        <li class=""><a href="{{ url('/users') }}">Administración</a> </li>
                        @else
                            @endrole
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{-- url('/login') --}}"></a></li>
                            <li><a href="{{-- url('/register') --}}"></a></li>
                        @else
                            <li class="active"><a href="{{ url('/home') }}">Pedidos</a> </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Cerrar Sesión
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')

    </div>
    @yield('before-scripts')

    @yield('after-scripts')

</body>
</html>
