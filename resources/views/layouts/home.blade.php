<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Chupetes Boann') }}</title>


    <!-- Stylesheets -->
    {!! Html::style('/bootstrap-3.3.7-dist/css/bootstrap.css') !!}
    {!! Html::style('/remark/css/bootstrap-extend.min.css') !!}
    {!! Html::style('/remark/material/topicon/assets/css/site.min.css') !!}
    {!! Html::style('/remark/css/purple.min.css') !!}

    {!! Html::style('/remark/material/topicon/assets/css/taskboard.min.css') !!}

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <!-- Plugins -->
    {!! Html::style('/remark/global/vendor/animsition/animsition.min.css') !!}
    {!! Html::style('/remark/global/vendor/intro-js/introjs.min.css') !!}
    <link href="{{ asset("css/backend/backend.css")}}" rel="stylesheet" type="text/css" />

    <!-- Styles -->
    {!! Html::style('/css/mycss.css') !!}

    <!-- Fonts -->
    {!! Html::style('/remark/global/fonts/material-design/material-design.min.css') !!}
    {!! Html::style('/remark/global/fonts/brand-icons/brand-icons.min.css') !!}
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

    <!--[if lt IE 9]>
    <script src="../../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    <!--[if lt IE 10]>
    <script src="../../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    <!-- Scripts -->
    {!! Html::script('/remark/global/vendor/breakpoints/breakpoints.js') !!}

    {{ Html::style("css/backend/plugin/switchery/switchery.css") }}
    <script>
        Breakpoints();
    </script>

    <!-- Styles -->
@yield('before-styles')

@yield('after-styles')

<!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>

    <nav class="navbar navbar-default navbar-static-top navbar-custom">
        <div class="container">
            <div class="navbar-header" style="padding-top: 5px;">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
            <!--<a class="navbar-brand" href="{{-- url('/') --}}">-->
                <a  href="{{ url('/') }}">
                    {{-- config('app.name', 'Chupetes Boann') --}}
                    <img src="{{url('/img/logo.png')}}" alt="" width="150px">
                </a>

            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    @role('admin')
                    <li class=""><a href="{{ url('/users') }}">Administración</a> </li>
                    @else
                        @endrole
                    <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{-- url('/login') --}}"></a></li>
                            <li><a href="{{-- url('/register') --}}"></a></li>
                        @else
                            <li class="active"><a href="{{ url('/home') }}">Pedidos</a> </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Cerrar Sesión
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                </ul>
            </div>
        </div>
    </nav>
<div class="page">
    <div class="page-content">
        @yield('content')
    </div>
</div>

    @include('layouts.footer')


<!-- Core  -->
{!! Html::script('/remark/global/vendor/babel-external-helpers/babel-external-helpers.js') !!}
{!! Html::script('/remark/global/vendor/jquery/jquery.js') !!}
{!! Html::script('/remark/global/vendor/tether/tether.js') !!}
    {!! Html::script('/bootstrap-3.3.7-dist/js/bootstrap.min.js') !!}
{!! Html::script('/remark/global/vendor/animsition/animsition.js') !!}

{!! Html::script('/remark/global/vendor/ashoverscroll/jquery-asHoverScroll.js') !!}

<!-- Plugins -->
{!! Html::script('/remark/global/vendor/switchery/switchery.min.js') !!}
{!! Html::script('/remark/global/vendor/intro-js/intro.js') !!}

<!-- Scripts -->
{!! Html::script('/remark/global/js/State.js') !!}
{!! Html::script('/remark/global/js/Component.js') !!}
{!! Html::script('/remark/global/js/Plugin.js') !!}
{!! Html::script('/remark/global/js/Base.min.js') !!}
{!! Html::script('/remark/global/js/Config.js') !!}
{!! Html::script('/remark/material/components/Menubar.min.js') !!}
{!! Html::script('/remark/material/components/Sidebar.js') !!}

<!-- Config -->

<!-- Page -->
{!! Html::script('/remark/material/components/Site.min.js') !!}
    {{ Html::script("js/backend/plugins/switchery/switchery.js") }}

<script>
    (function(document, window, $) {
        'use strict';
        var Site = window.Site;
        $(document).ready(function() {
            Site.run();
        });
    })(document, window, jQuery);
</script>

<!-- REQUIRED JS SCRIPTS -->
<!-- JavaScripts -->
@yield('before-scripts')

@yield('after-scripts')
</body>
</html>
