<div class="mb-10 hidden-sm hidden-xs">
    {{ link_to_route('users.index', 'Todos los Usuarios', [], ['class' => 'btn btn-primary btn-xs']) }}
    {{ link_to_route('users.create', 'Nuevo Usuario', [], ['class' => 'btn btn-success btn-xs']) }}
    {{ link_to_route('users.index', 'Usuarios Desactivados', [], ['class' => 'btn btn-warning btn-xs']) }}
    {{ link_to_route('users.index', 'Usuarios Eliminados', [], ['class' => 'btn btn-danger btn-xs']) }}
    {{ link_to_route('users.gps', 'GPS', [], ['class' => 'btn btn-primary btn-xs']) }}
</div><!--pull right-->

<div class="clearfix"></div>