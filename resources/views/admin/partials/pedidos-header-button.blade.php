<div class="pull-right mb-10 hidden-sm hidden-xs">
    {{ link_to_route('users.pedidos', 'Todos los Pedidos', [], ['class' => 'btn btn-primary btn-xs']) }}
    {{ Form::button('Excel Taller', ['class' => 'btn btn-success btn-xs', 'id'=> 'btntaller']) }}
    {{ Form::button('Cabecera Albaranes', ['class' => 'btn btn-warning btn-xs', 'id'=>'btnalbaranes']) }}
    {{ Form::button('Detalles Albaranes', ['class' => 'btn btn-warning btn-xs', 'id'=>'btndetalles']) }}
    {{ link_to_route('users.index', 'Usuarios sin Pedidos', [], ['class' => 'btn btn-danger btn-xs']) }}
</div><!--pull right-->

<div class="clearfix"></div>