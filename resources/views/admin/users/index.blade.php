@extends('admin.layouts.back')

@section('after-styles')
    {{ Html::style("css/backend/plugin/datatables/jquery.dataTables.min.css") }}
    <style type="text/css">
        table.dataTable tbody th, table.dataTable tbody td {
            padding: 8px 4px;
        }
        .centrarTexto{
            text-align: center;
        }

        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content/Box */
        .modal-content {
            background-color: #fefefe;
            margin: 15% auto; /* 15% from the top and centered */
            /*padding: 20px;*/
            border: 1px solid #888;
            width: 700px; /* Could be more or less, depending on screen size */
        }

        /* The Close Button */
        .close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }

        .modal-header {
            background-color: #822193;
            border-radius: .286rem .286rem 0 0;
            padding: 15px 20px;
            border-bottom: none;
        }
        .control-label{
            padding-top: 7px;
        }

        .oaerror {
            margin: 0 auto;
            background-color: #FFFFFF;
            padding: 5px;
            border: 1px solid
            #eee;
            border-left-color: rgb(238, 238, 238);
            border-left-width: 1px;
            border-left-width: 2px;
            border-radius: 3px;
            margin: 0 auto;
            font-size: 14px;
            color:
                    #6d3682;
        }
        .info {
            border-left-color:
                    #5bc0de;
            background-color: rgba(91, 192, 222, 0.1);
            background-color:
                    #e9e0ed;
        }
        #informa{
            display: none;
        }
    </style>
@stop

@section('page-header')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../index.html">Home</a>
        </li>
        <li class="breadcrumb-item active">Basic UI</li>
    </ol>
    <h1 class="page-title">
        Administración de Usuarios
    </h1>
    <div class="page-header-actions">
        @include('admin.partials.user-header-button')
    </div>

    <!-- Trigger/Open The Modal -->
    <!--<button id="myBtn">Open Modal</button>-->

    <!-- The Modal -->
    <div id="myModal" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <div class="modal-header">
                <span class="close" id="closeModal">&times;</span>
                <h5 style="color: white">Cambio de contraseña de: <span id="dataNombre"></span></h5>
            </div>
            <div class="modal-body">
                <div class="oaerror info" id="informa">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    Contraseña reseteada correctamente
                </div>
                <div class="row">
                    <div class="form-group">
                        {{ Form::label('pwd', 'Nueva contraseña', ['class' => 'col-md-3 control-label']) }}

                        <div class="col-md-9">
                            {{ Form::text('pwd', null, ['class' => 'form-control', 'placeholder' => 'Contraseña', 'id' => 'pwd' ]) }}
                        </div><!--col-lg-10-->
                    </div><!--form control-->

                </div>
                <input type="hidden" id="idUsuario" value="">
            </div>
            <div class="row"></div>

            <div class="modal-footer">
                <div class="col-sm-12">
                    <button type="button" id="cancelar" class="btn btn-default btn-pure waves-effect waves-classic" data-dismiss="modal">Cancelar</button>
                    <button type="button" id="guardar" class="btn btn-primary waves-effect waves-classic">Guardar cambios</button>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('content')
<div class="panel">
    <div class="panel-heading">
        <div class="row">
            <div class='col-lg-3'>
            <h3 class="panel-title">Usuarios activos</h3>
            </div>
            <div class='col-lg-3'>
                <br>
                <label>Filtro pedidos</label>
                <select>
                    <option value="todos">Todos</option>
                    <option value="1mes">Último mes</option>
                    <option value="3meses">Últimos 3 meses</option>
                    <option value="6meses">Últimos 6 meses</option>
                    <option value="9meses">Últimos 9 meses</option>
                    <option value="12meses">Último año</option>
                </select>
            </div>
        </div>
    </div><!-- /.box-header -->

    <div class="panel-body">
        <div class="table-responsive">
            @if($data)
                <table id="users-table" class="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Nombre</th>
                        <th>E-mail</th>
                        <th>Activo</th>
                        <th>Población</th>
                        <th>Provincia</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                </table>
            @endif
        </div>
    </div>

</div>
@endsection
@section('after-scripts')
    {{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}


    <script>
        $(function() {
            $('#users-table').DataTable({
                "language":
                    {
                        "sProcessing":     "Procesando...",
                        "sLengthMenu":     "Mostrar _MENU_ ",
                        "sZeroRecords":    "No se encontraron resultados",
                        "sEmptyTable":     "Ningún dato disponible en esta tabla",
                        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix":    "",
                        "sSearch":         "Buscar:",
                        "sUrl":            "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("user.get") }}',
                    type: 'get',
                    data: {status: 1, trashed: false}
                },
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'email' },
                    { data: 'active', name: 'active',  searchabe: false, sClass: 'centrarTexto' },
                    { data: 'poblacion', name: 'poblacion' },
                    { data: 'provincia', name: 'provincia' },
                    { data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500
            });
        });

        // Get the modal
        var modal = document.getElementById("myModal");

        // Get the button that opens the modal
        //var btn = document.getElementById("myBtn");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];
        var cierra = document.getElementById("closeModal");
        var cancela = document.getElementById("cancelar");

        // When the user clicks on the button, open the modal
        /*btn.onclick = function() {
            modal.style.display = "block";
        }*/

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        cierra.onclick = function(){
            modal.style.display = "none";
        }
        cancela.onclick = function(){
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }

        function mostrarVentana(id) {
            $("#idUsuario").val(id);
            $.ajax({
                type: 'get',
                url: 'user/obtener',
                data: { id:id },
                success:function (data) {
                    $("#dataNombre").html(data.nombre);
                },
                error: function (data) {
                    console.log('error');
                }
            });
            modal.style.display = "block";
        }

        $("#guardar").click(function(){
            var id = $("#idUsuario").val();
            var pwd = $("#pwd").val();
            if (pwd == ''){
                alert('No se ha introducido ninguna contraeña')
            }else{
                $.ajax({
                    type: 'get',
                    url: 'user/reset',
                    data: { id:id, pwd:pwd },
                    success:function (data) {
                        $("#pwd").val('');
                        $("#informa").show();

                        setTimeout(function() {
                            $('#informa').fadeOut('slow');
                        }, 3000); // <-- time in milliseconds
                    },
                    error: function (data) {
                        console.log('error');
                    }
                });
                //modal.style.display = "none";
            }
        });
    </script>
@stop