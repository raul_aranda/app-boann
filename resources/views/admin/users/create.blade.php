@extends('admin.layouts.back')

@section('page-header')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../index.html">Home</a>
        </li>
        <li class="breadcrumb-item active">Basic UI</li>
    </ol>
    <h1 class="page-title">
        Administración de Usuarios
    </h1>
    <div class="page-header-actions">
        @include('admin.partials.user-header-button')
    </div>
@endsection

@section('content')
    {{ Form::open(['route' => 'users.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}

    <div class="panel">
        <div class="panel-toolbar-header with-border">
            <h3 class="panel-title">Nuevo Usuario</h3>
        </div><!-- /.box-header -->

        <div class="panel-body">

            <div class="row">
                <div class="form-group">
                    {{ Form::label('name', 'Nombre', ['class' => 'col-md-2 control-label']) }}

                    <div class="col-md-7">
                        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) }}
                    </div><!--col-lg-7-->
                    <div class="col-md-3">
                        {{ Form::label('status', 'Activo', ['class' => 'control-label']) }}
                        {{ Form::checkbox('status', '1') }}
                    </div>
                </div><!--form control-->
            </div>

            <div class="row">
                <div class="form-group">
                    {{ Form::label('email', 'Dirección de Correo', ['class' => 'col-md-2 control-label']) }}

                    <div class="col-md-7">
                        {{ Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email' ]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div>

            <div class="row">
                <div class="form-group">
                    {{ Form::label('codigo_cliente', 'Código Cliente', ['class' => 'col-md-2 control-label']) }}

                    <div class="col-md-3">
                        {{ Form::text('cliente', null, ['class' => 'form-control', 'placeholder' => 'Código Cliente' ]) }}
                    </div><!--col-lg-10-->

                    {{ Form::label('pwd', 'Contraseña', ['class' => 'col-md-1 control-label']) }}


                    <div class="col-md-3">
                        {{ Form::text('pwdtxt', null,  ['class' => 'form-control', 'placeholder' => 'Contraseña', 'id'=>'pwdtxt']) }}
                    </div>
                </div><!--form control-->

            </div>

            <div class="row">
                <div class="form-group">
                    {{ Form::label('direccion', 'Dirección', ['class' => 'col-md-2 control-label']) }}

                    <div class="col-md-10">
                        {{ Form::text('direccion', null, ['class' => 'form-control', 'placeholder' => 'Dirección', 'id' => 'direccion' ]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div>

            <div class="row">
                <div class="form-group">
                    {{ Form::label('poblacion', 'Población', ['class' => 'col-md-2 control-label']) }}

                    <div class="col-md-3">
                        {{ Form::text('poblacion', null, ['class' => 'form-control', 'placeholder' => 'Población' ]) }}
                    </div>

                    {{ Form::label('provincia', 'Provincia', ['class' => 'col-md-1 control-label']) }}
                    <div class="col-md-3">
                        {{ Form::text('provincia', null, ['class' => 'form-control', 'placeholder' => 'Provincia' ]) }}
                    </div>

                    {{ Form::label('cp', 'C.P.', ['class' => 'col-md-1 control-label']) }}
                    <div class="col-md-2">
                        {{ Form::text('cp', null, ['class' => 'form-control', 'placeholder' => 'CP' ]) }}
                    </div>

                    <!--col-lg-10-->
                </div><!--form control-->
            </div>

            <div class="row">
                <div class="form-group">
                    {{ Form::label('telefono', 'Teléfono', ['class' => 'col-md-2 control-label']) }}

                    <div class="col-md-2">
                        {{ Form::text('telefono', null, ['class' => 'form-control', 'placeholder' => 'Teléfono' ]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div>
            <div class="row">
                <div class="form-group">
                    {{ Form::label('latitud', 'Latitud', ['class' => 'col-md-2 control-label']) }}

                    <div class="col-md-2">
                        {{ Form::text('latitud', null, ['class' => 'form-control', 'placeholder' => 'Latitud', 'readonly']) }}
                    </div><!--col-lg-10-->

                    {{ Form::label('longitud', 'Longitud', ['class' => 'col-md-1 control-label']) }}

                    <div class="col-md-2">
                        {{ Form::text('longitud', null, ['class' => 'form-control', 'placeholder' => 'Longitud', 'readonly' ]) }}
                    </div><!--col-lg-10-->
                    {{-- link_to_route('geocode.user', 'Obtener Coordenadas', ['id' => 6], ['class' => 'btn btn-primary']) --}}
                    <button type="button" class="btn btn-primary" id="geocode">Obtener Coordenadas</button>
                </div><!--form control-->
            </div>

            <div class="row">
                <div class="form-group">
                    {{ Form::label('Dirección Geocode', 'Dirección Geocode', ['class' => 'col-md-2 control-label']) }}

                    <div class="col-md-8">
                        {{ Form::text('geoadd', null, ['class' => 'form-control', 'placeholder' => 'Dirección Geocode', 'readonly', 'id'=>'geoadd' ]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div>

            <div class="row">
                <div class="form-group">
                    {{ Form::label('Agente', 'Tu comercio online', ['class' => 'col-md-2 control-label']) }}

                    <div class="col-md-5">
                        {{ Form::text('agente', null, ['class' => 'form-control', 'placeholder' => 'Tu comercio online']) }}
                    </div><!--col-lg-10-->

                    {{ Form::label('envio', 'Envío', ['class' => 'col-md-1 control-label']) }}
                    <div class="col-md-2">
                        <select name="envio" class="form-control">
                            @foreach($envios as $envio)
                                <option value={{ $envio->nombre }}>{{ $envio->nombre }}</option>
                            @endforeach
                        </select>
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div>


        </div><!-- /.box-body -->
    </div><!--box-->

    <div class="box box-success">
        <div class="box-body">
            <div class="pull-left">
                {{ link_to_route('users.index', 'Cancelar', [], ['class' => 'btn btn-danger']) }}
            </div><!--pull-left-->

            <div class="pull-right">
                {{ Form::submit('Actualizar', ['class' => 'btn btn-success']) }}
            </div><!--pull-right-->

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->
    </form>



@stop

@section('after-scripts')

    <script>
        $("#geocode").click(function () {
            //var address = $("direccion").val()+','+$("poblacion").val()+','+$("provincia").val()+','+$("cp").val();
            var dir = $("#direccion").val();
            var pob = $("#poblacion").val();
            var pro = $("#provincia").val();
            var cp  = $("#cp").val();

            var address = dir +', '+ pob + ', ' + pro + ', ' + cp;

            $.ajax({
                url: 'add/geocodeAdd',
                data: {
                    address: address
                },
                success: function (data) {
                    $("#latitud").val(data.lat);
                    $("#longitud").val(data.lon);
                    $("#geoadd").val(data.for);
                },
                error: function (errorThrown) {
                    alert("Dirección no encontrada o inválida");
                }
            });
        });
    </script>
@stop
