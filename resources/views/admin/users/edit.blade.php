@extends ('admin.layouts.back')

@section ('title', 'Administración de Usuarios' . ' | ' . 'Modificar Usuario')

@section('after-styles')

@stop

@section('page-header')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../index.html">Inicio</a>
        </li>
        <li class="breadcrumb-item active">Usuarios</li>
    </ol>
    <h1 class="page-title">
        Administración de Usuarios
    </h1>
    <div class="page-header-actions">
        @include('admin.partials.user-header-button')
    </div>
@endsection

@section('content')
    {{ Form::model($user, ['route' => ['users.update', $user], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id'=>'form_user']) }}

    <div class="panel">
        <div class="panel heading">
            <h3 class="panel-title">Modificar Usuario</h3>
        </div><!-- /.box-header -->

        <div class="panel-body">

            <div class="row">
                <div class="form-group">
                    {{ Form::label('name', 'Nombre', ['class' => 'col-md-2 control-label']) }}

                    <div class="col-md-7">
                        {{ Form::text('name', $user->name, ['class' => 'form-control', 'placeholder' => 'Nombre']) }}
                    </div><!--col-lg-10-->
                    <div class="col-md-3">
                        {{ Form::label('status', 'Activo', ['class' => 'col-lg-2 control-label']) }}
                        {{ Form::checkbox('status', '1', $user->active == 1) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div>

            <div class="row">
                <div class="form-group">
                    {{ Form::label('email', 'Dirección de Correo', ['class' => 'col-md-2 control-label']) }}

                    <div class="col-md-7">
                        {{ Form::text('email', $user->email, ['class' => 'form-control', 'placeholder' => 'Email' ]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div>

            <div class="row">
                <div class="form-group">
                    {{ Form::label('codigo_cliente', 'Código Cliente', ['class' => 'col-md-2 control-label']) }}

                    <div class="col-md-3">
                        {{ Form::text('cliente', $user->codigo_cliente, ['class' => 'form-control', 'placeholder' => 'Código Cliente', 'readonly' ]) }}
                    </div><!--col-lg-10-->

                    {{ Form::label('pwd', 'Contraseña', ['class' => 'col-md-1 control-label']) }}

                    @php
                        if ($user->encrypt_password == ''){
                            $data = "Sin contraseña";
                            $clave = "";
                        }else{
                            $clave = \Illuminate\Support\Facades\Crypt::decrypt($user->encrypt_password);
                            $data = null;
                        }
                    @endphp
                    <div class="col-md-3">
                        {{ Form::text('pwdtxt', $data,  ['class' => 'form-control', 'placeholder' => 'Contraseña oculta', 'id'=>'pwdtxt']) }}
                        {{ Form::hidden('pwdhid', $clave,  ['class' => 'form-control', 'id'=>'pwdhid']) }}
                    </div>
                    <a href="#"><i class="fa fa-eye" aria-hidden="true" style="margin-top: 10px" id="pwdbtn"></i> </a>
                    <a href="#"><i class="fa fa-eye-slash" aria-hidden="true" style="margin-top: 10px" id="pwdclose"></i> </a>
                </div><!--form control-->

            </div>

            <div class="row">
                <div class="form-group">
                    {{ Form::label('direccion', 'Dirección', ['class' => 'col-md-2 control-label']) }}

                    <div class="col-md-10">
                        {{ Form::text('direccion', $user->direccion, ['class' => 'form-control', 'placeholder' => 'Dirección', 'id' => 'direccion' ]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div>

            <div class="row">
                <div class="form-group">
                    {{ Form::label('poblacion', 'Población', ['class' => 'col-md-2 control-label']) }}

                    <div class="col-md-3">
                        {{ Form::text('poblacion', $user->poblacion, ['class' => 'form-control', 'placeholder' => 'Población' ]) }}
                    </div>

                    {{ Form::label('provincia', 'Provincia', ['class' => 'col-md-1 control-label']) }}
                    <div class="col-md-3">
                        {{ Form::text('provincia', $user->provincia, ['class' => 'form-control', 'placeholder' => 'Provincia' ]) }}
                    </div>

                    {{ Form::label('cp', 'C.P.', ['class' => 'col-md-1 control-label']) }}
                    <div class="col-md-2">
                        {{ Form::text('cp', $user->cp, ['class' => 'form-control', 'placeholder' => 'CP' ]) }}
                    </div>

                    <!--col-lg-10-->
                </div><!--form control-->
            </div>

            <div class="row">
                <div class="form-group">
                    {{ Form::label('telefono', 'Teléfono', ['class' => 'col-md-2 control-label']) }}

                    <div class="col-md-2">
                        {{ Form::text('telefono', $user->tlf, ['class' => 'form-control', 'placeholder' => 'Teléfono' ]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div>
            <div class="row">
                <div class="form-group">
                    {{ Form::label('latitud', 'Latitud', ['class' => 'col-md-2 control-label']) }}

                    <div class="col-md-2">
                        {{ Form::text('latitud', $user->lat, ['class' => 'form-control', 'placeholder' => 'Latitud' ]) }}
                    </div><!--col-lg-10-->

                    {{ Form::label('longitud', 'Longitud', ['class' => 'col-md-1 control-label']) }}

                    <div class="col-md-2">
                        {{ Form::text('longitud', $user->lon, ['class' => 'form-control', 'placeholder' => 'Longitud' ]) }}
                    </div><!--col-lg-10-->
                    {{-- link_to_route('geocode.user', 'Obtener Coordenadas', ['id' => 6], ['class' => 'btn btn-primary']) --}}
                    <button type="button" class="btn btn-primary" id="geocode">Obtener Coordenadas</button>
                </div><!--form control-->
            </div>

            <div class="row">
                <div class="form-group">
                    {{ Form::label('Dirección Geocode', 'Dirección Geocode', ['class' => 'col-md-2 control-label']) }}

                    <div class="col-md-8">
                        {{ Form::text('geoadd', $user->geoaddress, ['class' => 'form-control', 'placeholder' => 'Dirección Geocode', 'readonly', 'id'=>'geoadd' ]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div>

            <div class="row">
                <div class="form-group">
                    {{ Form::label('Agente', 'Tu comercio online', ['class' => 'col-md-2 control-label']) }}

                    <div class="col-md-5">
                        {{ Form::text('agente', $user->agente, ['class' => 'form-control', 'placeholder' => 'Tu comercio online']) }}
                    </div><!--col-lg-10-->

                    {{ Form::label('envio', 'Envío', ['class' => 'col-md-1 control-label']) }}
                    <div class="col-md-2">
                        <select name="envio" class="form-control">
                            @foreach($envios as $envio)
                                @if($envio->nombre == $user->envio)
                                    <option value={{ $envio->nombre }} selected>{{ $envio->nombre }}</option>
                                @else
                                    <option value={{ $envio->nombre }}>{{ $envio->nombre }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div>

            {{--<div class="row">
                <div class="form-group">
                    {{ Form::label('send', 'Catálogo enviado', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-1">
                        {{ Form::checkbox('send', '1', (count($catalogo) == '1')?'checked':'') }}
                    </div><!--col-lg-1-->
                </div><!--form control-->
            </div>--}}
        </div><!-- /.box-body -->
    </div><!--box-->


        <div class="panel-footer">
            <div class="pull-left">
                {{ link_to_route('users.index', 'Cancelar', [], ['class' => 'btn btn-danger']) }}
            </div><!--pull-left-->

            <div class="pull-right">
                {{ Form::submit('Actualizar', ['class' => 'btn btn-success']) }}
            </div><!--pull-right-->

            <div class="clearfix"></div>
        </div><!-- /.box-body -->

    </form>



@stop

@section('after-scripts')

    <script>
        $(document).ready(function () {
            $("#pwdclose").hide();
            $("#pwdbtn").show();

            var elem = document.querySelector('.js-switch');
            var init = new Switchery(elem);
        });

        $("#geocode").click(function () {
            //var address = $("direccion").val()+','+$("poblacion").val()+','+$("provincia").val()+','+$("cp").val();
            var dir = $("#direccion").val();
            var pob = $("#poblacion").val();
            var pro = $("#provincia").val();
            var cp  = $("#cp").val();

            var address = dir +', '+ pob + ', ' + pro + ', ' + cp;

            $.ajax({
                url: 'geocode',
                data: {
                    address: address
                },
                success: function (data) {
                    $("#latitud").val(data.lat);
                    $("#longitud").val(data.lon);
                    $("#geoadd").val(data.for);
                },
                error: function (errorThrown) {
                    alert("Dirección no encontrada o inválida");
                }
            });
        });

        $("#pwdbtn").click(function () {
            var pwd =  $("#pwdhid").val();
            if(pwd == ''){

            }else{
                $("#pwdclose").show();
                $("#pwdbtn").hide();
                $("#pwdtxt").val(pwd);
            }
        });

        $("#pwdclose").click(function () {
            $("#pwdtxt").val('');
            $("#pwdclose").hide();
            $("#pwdbtn").show();
        });
    </script>
@stop
