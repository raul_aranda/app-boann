<html>
<style type="text/css">
    .azul{
        color: #0b96e5;
    }

    .rosa{
        color: #b600b6;
    }
    .rojo{
        color: #9f0100;
    }
    .color{
        text-align: center;
        font-weight: bold;
    }

    .masuno{
        color: #9f0100;
        font-weight: bold;
    }
</style>

<!-- Headings -->
<td></td>
<tr></tr>

<!--  Bold -->
<td><b>NOMBRE</b></td>
<td><b>APELLIDOS</b></td>
<tr></tr>

@if(!empty($textiles[0]))
    @foreach($textiles as $textil)
        <tr>
            <td width="20">{{ $textil['Nombre'] }}</td>
            <td width="20">{{ $textil['Apellidos'] }}</td>
            <td>{{$textil['Pedido']}}</td>
            <td width="15">{{ $textil['textil'] }}</td>
            <td width="10" align="center" class="{{ ($textil['cantidad']> 1 ? 'masuno' : '') }}">{{ $textil['cantidad'] }}</td>
            <td class="color {{ strtolower($textil['niño']) }}">{{ $textil['niño'] }}</td>
            <td>{{$textil['opcion']}}</td>
        </tr>
    @endforeach
@endif

@if(!empty($multiusos[0]))
    @foreach($multiusos as $multiuso)
        <tr>
            <td width="20">{{ $multiuso['Nombre'] }}</td>
            <td width="20">{{ $multiuso['Apellidos'] }}</td>
            <td>{{ $multiuso['Pedido'] }}</td>
            <td width="15">{{ $multiuso['textil'] }}</td>
            <td width="10" align="center" class="{{ ($multiuso['cantidad']> 1 ? 'masuno' : '') }}">{{ $multiuso['cantidad'] }}</td>
            <td class="color {{ strtolower($multiuso['niño']) }}"><b>{{ $multiuso['niño'] }}</b></td>
            <td>{{$multiuso['opcion']}}</td>
        </tr>
    @endforeach
@endif

@if(!empty($mixtos[0]))
    @foreach($mixtos as $mixto)
        <tr>
            <td width="20">{{ $mixto['Nombre'] }}</td>
            <td width="20">{{ $mixto['Apellidos'] }}</td>
            <td>{{ $mixto['Pedido'] }}</td>
            <td width="15">{{ $mixto['textil'] }}</td>
            <td width="10" align="center" class="{{ ($mixto['cantidad']> 1 ? 'masuno' : '') }}">{{ $mixto['cantidad'] }}</td>
            <td class="color {{ strtolower($mixto['niño']) }}"><b>{{ $mixto['niño'] }}</b></td>
            <td>{{ $mixto['opcion']}}</td>
        </tr>
    @endforeach
@endif


</html>
