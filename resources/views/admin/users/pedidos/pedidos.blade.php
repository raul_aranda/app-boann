@extends('admin.layouts.back')

@section('after-styles')
    {{ Html::style("/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css") }}
@stop

@section('page-header')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../index.html">Home</a>
        </li>
        <li class="breadcrumb-item active">Basic UI</li>
    </ol>
    <h1 class="page-title">
        Listado de Pedidos
    </h1>
    <p class="page-description">Pedidos Realizados</p>
    <div class="page-header-actions">
        @include('admin.partials.pedidos-header-button')
    </div>
@endsection

@section('content')
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Listado de Pedidos</h3>
        </div><!-- /.box-header -->
        <div class="panel-toolbar">
            <div class="filtros">
                {!! Form::open(['route' => 'filtro.pedidos', 'method' => 'post']) !!}
                <div class="container">
                    <div class='col-lg-3'>
                        <div class="form-group">
                            <div class='input-group date' id='datetimepicker6'>
                                {{ Form::text('date1', $fecha1, ['class' => 'form-control',  'id' => 'date1' ]) }}
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                            </div>
                        </div>
                    </div>
                    <div class='col-lg-3'>
                        <div class="form-group">
                            <div class='input-group date' id='datetimepicker7'>
                                {{ Form::text('date2', $fecha2, ['class' => 'form-control',  'id' => 'date2' ]) }}
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    {{ Form::submit('Filtro', ['class' => 'btn btn-success']) }}

                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="panel-body">

            {!! Form::open(['url' => '', 'method' => 'post', 'id' => 'listado']) !!}
            <div class="table-responsive">
                @if($data)
                    <table id="users-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Id</th>
                            <th><a href="{{ route('order.by', ['filtro' => 'fecha']) }}">Fecha</a> </th>
                            <th><a href="{{ route('order.by', ['filtro' => 'cliente']) }}"> Código Cliente</a></th>
                            <th><a href="{{ route('order.by', ['filtro' => 'nombre']) }}">Nombre</a></th>
                            <th style="text-align: center">Num Artículos</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $row)
                            @php ($fecha = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->fecha))
                            @php ($fecha = $fecha->format('d-m-Y H:i:s'))

                            <tr>
                                <td>
                                    @if($filtro == 'true')
                                        {{ Form::checkbox('pedido[]', $row->id, 'yes') }}
                                    @else
                                        {{ Form::checkbox('pedido[]', $row->id, null) }}
                                    @endif
                                </td>
                                <td>{{ $row->id }}</td>
                                <td>{{ $fecha }}</td>
                                <td>{{ $row->id_cliente }}</td>
                                <td>{{ $row->name }}</td>
                                <td style="text-align: center">{{ $row->num_art }}</td>
                                <td><a href="'.route('users.show', $row->id).'" class="btn btn-xs btn-info"><i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="Visualizar"></i></a></td>
                            </tr>
                        @endforeach
                    </table>
                @endif
            </div>
            {!! Form::close() !!}
            @if ($filtro == 'false')
                <div class="container">{{ $data->links() }}</div>
            @endif

        </div>
    </div>
@endsection
@section('after-scripts')
    {{ Html::script("/bower_components/moment/min/moment-with-locales.min.js") }}
    {{ Html::script("/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js") }}
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker6').datetimepicker({
                locale: 'es',
                format: 'DD-MM-YYYY H:mm:ss'
            });

            $('#datetimepicker7').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                locale: 'es',
                format: 'DD-MM-YYYY H:mm:ss'
            });

            $('#btntaller').click (function () {
                var form = $('#listado');
                var baseUrl = "{{ URL::to('/excel-taller') }}";

                form.attr('action', baseUrl);
                form.submit();
            });

            $('#btnalbaranes').click (function () {
                var form = $('#listado');
                var baseUrl = "{{ URL::to('/excel-cabecera') }}";

                form.attr('action', baseUrl);
                form.submit();
            });

            $('#btndetalles').click (function () {
                var form = $('#listado');
                var baseUrl = "{{ URL::to('/excel-detalle') }}";

                form.attr('action', baseUrl);
                form.submit();
            });
        });

    </script>
@stop