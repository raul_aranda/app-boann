<html>
<style type="text/css">
    .azul{
        color: #0b96e5;
    }
    
    .rosa{
        color: #b600b6;
    }
    .rojo{
        color: #9f0100;
    }
    .color{
        text-align: center;
        font-weight: bold;
    }

    .masuno{
        color: #9f0100;
        font-weight: bold;
    }
</style>

<!-- Headings -->
<td></td>
<tr></tr>

<!--Columna A-->
<td></td>
<!--  Bold -->
<td><b>Pedido</b></td>
<tr></tr>
<tr></tr>

@if(!empty($textiles[0]))
    @foreach($textiles as $textil)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $textil['textil'] }}</td>
            <td width="10" align="center" class="{{ ($textil['cantidad']> 1 ? 'masuno' : '') }}">{{ $textil['cantidad'] }}</td>
            <td>{{ $textil['niño'] }}</td>
            <td>{{$textil['opcion']}}</td>
            <td width="20">{{ $textil['Nombre'] }}</td>
            <td width="10">{{ $textil['Id'] }}</td>
            <td>{{$textil['Pedido']}}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif

@if(!empty($multiusos[0]))
    @foreach($multiusos as $multiuso)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $multiuso['textil'] }}</td>
            <td width="10" align="center" class="{{ ($multiuso['cantidad']> 1 ? 'masuno' : '') }}">{{ $multiuso['cantidad'] }}</td>
            <td>{{ $multiuso['niño'] }}</td>
            <td>{{$multiuso['opcion']}}</td>
            <td width="20">{{ $multiuso['Nombre'] }}</td>
            <td width="15">{{ $multiuso['Id'] }}</td>
            <td>{{ $multiuso['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif

@if(!empty($mixtos[0]))
    @foreach($mixtos as $mixto)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $mixto['textil'] }}</td>
            <td width="10" align="center" class="{{ ($mixto['cantidad']> 1 ? 'masuno' : '') }}">{{ $mixto['cantidad'] }}</td>
            <td>{{ $mixto['niño'] }}</td>
            <td>{{ $mixto['opcion']}}</td>
            <td width="20">{{ $mixto['Nombre'] }}</td>
            <td width="15">{{ $mixto['Id'] }}</td>
            <td>{{ $mixto['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif

@if(!empty($pegatinas[0]))
    @foreach($pegatinas as $pegatina)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $pegatina['textil'] }}</td>
            <td width="10" align="center" class="{{ ($pegatina['cantidad']> 1 ? 'masuno' : '') }}">{{ $pegatina['cantidad'] }}</td>
            <td class="color {{ strtolower($pegatina['niño']) }}">{{ $pegatina['niño'] }}</td>
            <td>{{$pegatina['opcion']}}</td>
            <td>{{ '('.$pegatina['Talla'].')' }} {{ $pegatina['Nombre'] }}</td>
            <td>{{ $pegatina['Id'] }}</td>
            <td>{{ $pegatina['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif

@if(!empty($zapatillas[0]))
    @foreach($zapatillas as $zapatilla)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $zapatilla['textil'] }}</td>
            <td width="10" align="center" class="{{ ($zapatilla['cantidad']> 1 ? 'masuno' : '') }}">{{ $zapatilla['cantidad'] }}</td>
            <td class="color {{ strtolower($zapatilla['niño']) }}"><b>{{ $zapatilla['niño'] }}</b></td>
            <td>{{ $zapatilla['opcion'] }}</td>
            <td width="20">{{ $zapatilla['Nombre'] }}</td>
            <td width="15">{{ $zapatilla['Id'] }}</td>
            <td>{{ $zapatilla['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif

@if(!empty($chapas[0]))
    @foreach($chapas as $chapa)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $chapa['textil'] }}</td>
            <td width="10" align="center" class="{{ ($chapa['cantidad']> 1 ? 'masuno' : '') }}">{{ $chapa['cantidad'] }}</td>
            <td class="color {{ strtolower($chapa['niño']) }}">{{ $chapa['niño'] }}</td>
            <td>{{ $chapa['opcion'] }}</td>
            <td width="20">{{ $chapa['Nombre'] }}</td>
            <td width="15">{{ $chapa['Id'] }}</td>
            <td>{{ $chapa['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif

@if(!empty($pulseras[0]))
    @foreach($pulseras as $pulsera)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $pulsera['textil'] }}</td>
            <td width="10" align="center" class="{{ ($pulsera['cantidad']> 1 ? 'masuno' : '') }}">{{ $pulsera['cantidad'] }}</td>
            <td class="color {{ strtolower($pulsera['niño']) }}">{{ $pulsera['niño'] }}</td>
            <td>{{$pulsera['opcion'] }}</td>
            <td width="20">{{ $pulsera['Nombre'] }}</td>
            <td width="15">{{ $pulsera['Id'] }}</td>
            <td>{{ $pulsera['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif

@if(!empty($camisetas[0]))
    @foreach($camisetas as $camiseta)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $camiseta['textil'] }}</td>
            <td width="10" align="center" class="{{ ($camiseta['cantidad'] >1 ? 'masuno' : '') }}">{{ $camiseta['cantidad'] }}</td>
            <td class="color">{{ $camiseta['niño'] }}</td>
            <td>{{$camiseta['opcion']}}</td>
            <td width="20">{{ $camiseta['Nombre'] }}</td>
            <td width="15">{{ $camiseta['Id'] }}</td>
            <td>{{ $camiseta['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif

@if(!empty($bolsas[0]))
    @foreach($bolsas as $bolsa)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $bolsa['textil'] }}</td>
            <td width="10" align="center" class="{{ ($bolsa['cantidad']> 1 ? 'masuno' : '') }}">{{ $bolsa['cantidad'] }}</td>
            <td class="color {{ strtolower($bolsa['niño']) }}">{{ $bolsa['niño'] }}</td>
            <td>{{ $bolsa['opcion'] }}</td>
            <td width="20">{{ $bolsa['Nombre'] }}</td>
            <td width="15">{{ $bolsa['Id'] }}</td>
            <td>{{ $bolsa['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif

@if(!empty($bodys[0]))
    @foreach($bodys as $body)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $body['textil'] }}</td>
            <td width="10" align="center" class="{{ ($body['cantidad']> 1 ? 'masuno' : '') }}">{{ $body['cantidad'] }}</td>
            <td class="color">{{ $body['niño'] }}</td>
            <td>{{ $body['opcion'] }}</td>
            <td width="20">{{ $body['Nombre'] }}</td>
            <td width="15">{{ $body['Id'] }}</td>
            <td>{{ $body['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif

@if(!empty($navidad[0]))
    @foreach($navidad as $nav)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $nav['textil'] }}</td>
            <td width="10" align="center" class="{{ ($nav['cantidad']> 1 ? 'masuno' : '') }}">{{ $nav['cantidad'] }}</td>
            <td class="color">{{ strtoupper($nav['niño']) }}</td>
            <td>{{ $nav['opcion'] }}</td>
            <td width="20">{{ $nav['Nombre'] }}</td>
            <td width="15">{{ $nav['Id'] }}</td>
            <td>{{ $nav['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif

@if(!empty($vajillas[0]))
    @foreach($vajillas as $vajilla)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $vajilla['textil'] }}</td>
            <td width="10" align="center" class="{{ ($vajilla['cantidad']> 1 ? 'masuno' : '') }}">{{ $vajilla['cantidad'] }}</td>
            <td class="color">{{ strtoupper($vajilla['niño']) }}</td>
            <td>{{ $vajilla['opcion'] }}</td>
            <td width="20">{{ $vajilla['Nombre'] }}</td>
            <td width="15">{{ $vajilla['Id'] }}</td>
            <td>{{ $vajilla['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif

@if(!empty($vasos[0]))
    @foreach($vasos as $vaso)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $vaso['textil'] }}</td>
            <td width="10" align="center" class="{{ ($vaso['cantidad']> 1 ? 'masuno' : '') }}">{{ $vaso['cantidad'] }}</td>
            <td class="color">{{ strtoupper($vaso['niño']) }}</td>
            <td>{{ $vaso['opcion'] }}</td>
            <td width="20">{{ $vaso['Nombre'] }}</td>
            <td width="15">{{ $vaso['Id'] }}</td>
            <td>{{ $vaso['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif

@if(!empty($cubiertos[0]))
    @foreach($cubiertos as $cubierto)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $cubierto['textil'] }}</td>
            <td width="10" align="center" class="{{ ($cubierto['cantidad']> 1 ? 'masuno' : '') }}">{{ $cubierto['cantidad'] }}</td>
            <td class="color">{{ strtoupper($cubierto['niño']) }}</td>
            <td>{{ $cubierto['opcion'] }}</td>
            <td width="20">{{ $cubierto['Nombre'] }}</td>
            <td width="15">{{ $cubierto['Id'] }}</td>
            <td>{{ $cubierto['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif

@if(!empty($baberos[0]))
    @foreach($baberos as $babero)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $babero['textil'] }}</td>
            <td width="10" align="center" class="{{ ($babero['cantidad']> 1 ? 'masuno' : '') }}">{{ $babero['cantidad'] }}</td>
            <td class="color">{{ strtoupper($babero['niño']) }}</td>
            <td>{{ $babero['opcion'] }}</td>
            <td width="20">{{ $babero['Nombre'] }}</td>
            <td width="15">{{ $babero['Id'] }}</td>
            <td>{{ $babero['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif

@if(!empty($caja01[0]))
    @foreach($caja01 as $caja1)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $caja1['textil'] }}</td>
            <td width="10" align="center" class="{{ ($caja1['cantidad']> 1 ? 'masuno' : '') }}">{{ $caja1['cantidad'] }}</td>
            <td class="color">{{ strtoupper($caja1['niño']) }}</td>
            <td>{{ $caja1['opcion'] }}</td>
            <td width="20">{{ $caja1['Nombre'] }}</td>
            <td width="15">{{ $caja1['Id'] }}</td>
            <td>{{ $caja1['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif

@if(!empty($caja02[0]))
    @foreach($caja02 as $caja2)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $caja2['textil'] }}</td>
            <td width="10" align="center" class="{{ ($caja2['cantidad']> 1 ? 'masuno' : '') }}">{{ $caja2['cantidad'] }}</td>
            <td class="color">{{ strtoupper($caja2['niño']) }}</td>
            <td>{{ $caja2['opcion'] }}</td>
            <td width="20">{{ $caja2['Nombre'] }}</td>
            <td width="15">{{ $caja2['Id'] }}</td>
            <td>{{ $caja2['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif
@if(!empty($sujeta[0]))
    @foreach($sujeta as $sujetaTela)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $sujetaTela['textil'] }}</td>
            <td width="10" align="center" class="{{ ($sujetaTela['cantidad']> 1 ? 'masuno' : '') }}">{{ $sujetaTela['cantidad'] }}</td>
            <td class="color">{{ strtoupper($sujetaTela['niño']) }}</td>
            <td>{{ $sujetaTela['opcion'] }}</td>
            <td width="20">{{ $sujetaTela['Nombre'] }}</td>
            <td width="15">{{ $sujetaTela['Id'] }}</td>
            <td>{{ $sujetaTela['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif
@if(!empty($ambient[0]))
    @foreach($ambient as $ambientador)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $ambientador['textil'] }}</td>
            <td width="10" align="center" class="{{ ($ambientador['cantidad'] > 1 ? 'masuno' : '') }}">{{ $ambientador['cantidad'] }}</td>
            <td class="color {{ strtolower($ambientador['niño']) }}">{{ $ambientador['niño'] }}</td>
            <td>{{ $ambientador['opcion'] }}</td>
            <td width="20">{{ $ambientador['Nombre'] }}</td>
            <td width="15">{{ $ambientador['Id'] }}</td>
            <td>{{ $ambientador['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif
@if(!empty($mochilas[0]))
    @foreach($mochilas as $mochila)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $mochila['textil'] }}</td>
            <td width="10" align="center" class="{{ ($mochila['cantidad'] > 1 ? 'masuno' : '') }}">{{ $mochila['cantidad'] }}</td>
            <td class="color {{ strtolower($mochila['niño']) }}">{{ $mochila['niño'] }}</td>
            <td>{{ $mochila['opcion'] }}</td>
            <td width="20">{{ $mochila['Nombre'] }}</td>
            <td width="15">{{ $mochila['Id'] }}</td>
            <td>{{ $mochila['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif
@if(!empty($talegas[0]))
    @foreach($talegas as $talega)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $talega['textil'] }}</td>
            <td width="10" align="center" class="{{ ($talega['cantidad'] > 1 ? 'masuno' : '') }}">{{ $talega['cantidad'] }}</td>
            <td class="color {{ strtolower($talega['niño']) }}">{{ $talega['niño'] }}</td>
            <td>{{ $talega['opcion'] }}</td>
            <td width="20">{{ $talega['Nombre'] }}</td>
            <td width="15">{{ $talega['Id'] }}</td>
            <td>{{ $talega['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif

@if(!empty($talegasMini[0]))
    @foreach($talegasMini as $talegaMini)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $talegaMini['textil'] }}</td>
            <td width="10" align="center" class="{{ ($talegaMini['cantidad'] > 1 ? 'masuno' : '') }}">{{ $talegaMini['cantidad'] }}</td>
            <td class="color {{ strtolower($talegaMini['niño']) }}">{{ $talegaMini['niño'] }}</td>
            <td>{{ $talegaMini['opcion'] }}</td>
            <td width="20">{{ $talegaMini['Nombre'] }}</td>
            <td width="15">{{ $talegaMini['Id'] }}</td>
            <td>{{ $talegaMini['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif

@if(!empty($aguaColonias[0]))
    @foreach($aguaColonias as $aguaColonia)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $aguaColonia['textil'] }}</td>
            <td width="10" align="center" class="{{ ($aguaColonia['cantidad'] > 1 ? 'masuno' : '') }}">{{ $aguaColonia['cantidad'] }}</td>
            <td>{{ $aguaColonia['opcion'] }}</td>
            <td width="20">{{ $aguaColonia['Nombre'] }}</td>
            <td width="15">{{ $aguaColonia['Id'] }}</td>
            <td>{{ $aguaColonia['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif

@if(!empty($mascarillaInfantil[0]))
    @foreach($mascarillaInfantil as $maskInfantil)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $maskInfantil['textil'] }}</td>
            <td width="10" align="center" class="{{ ($maskInfantil['cantidad'] > 1 ? 'masuno' : '') }}">{{ $maskInfantil['cantidad'] }}</td>
            <td class="color {{ strtolower($maskInfantil['niño']) }}">{{ $maskInfantil['niño'] }}</td>
            <td>{{ $maskInfantil['opcion'] }}</td>
            <td width="20">{{ $maskInfantil['Nombre'] }}</td>
            <td width="15">{{ $maskInfantil['Id'] }}</td>
            <td>{{ $maskInfantil['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif

@if(!empty($mascarillaAdulto[0]))
    @foreach($mascarillaAdulto as $maskAdulto)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $maskAdulto['textil'] }}</td>
            <td width="10" align="center" class="{{ ($maskAdulto['cantidad'] > 1 ? 'masuno' : '') }}">{{ $maskAdulto['cantidad'] }}</td>
            <td class="color {{ strtolower($maskAdulto['niño']) }}">{{ $maskAdulto['niño'] }}</td>
            <td>{{ $maskAdulto['opcion'] }}</td>
            <td width="20">{{ $maskAdulto['Nombre'] }}</td>
            <td width="15">{{ $maskAdulto['Id'] }}</td>
            <td>{{ $maskAdulto['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif

@if(!empty($bolsasPortaObjetos[0]))
    @foreach($bolsasPortaObjetos as $bolsaPortaObjeto)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $bolsaPortaObjeto['textil'] }}</td>
            <td width="10" align="center" class="{{ ($bolsaPortaObjeto['cantidad'] > 1 ? 'masuno' : '') }}">{{ $bolsaPortaObjeto['cantidad'] }}</td>
            <td>{{ $bolsaPortaObjeto['niño'] }}</td>
            <td>{{ $bolsaPortaObjeto['opcion'] }}</td>
            <td width="20">{{ $bolsaPortaObjeto['Nombre'] }}</td>
            <td width="15">{{ $bolsaPortaObjeto['Id'] }}</td>
            <td>{{ $bolsaPortaObjeto['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif

@if(!empty($cojines[0]))
    @foreach($cojines as $cojin)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $cojin['textil'] }}</td>
            <td width="10" align="center" class="{{ ($cojin['cantidad'] > 1 ? 'masuno' : '') }}">{{ $cojin['cantidad'] }}</td>
            <td>{{ $cojin['niño'] }}</td>
            <td>{{ $cojin['opcion'] }}</td>
            <td width="20">{{ $cojin['Nombre'] }}</td>
            <td width="15">{{ $cojin['Id'] }}</td>
            <td>{{ $cojin['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif

@if(!empty($cepilloDientes[0]))
    @foreach($cepilloDientes as $cepillo)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $cepillo['textil'] }}</td>
            <td width="10" align="center" class="{{ ($cepillo['cantidad'] > 1 ? 'masuno' : '') }}">{{ $cepillo['cantidad'] }}</td>
            <td>{{ $cepillo['niño'] }}</td>
            <td>{{ $cepillo['opcion'] }}</td>
            <td width="20">{{ $cepillo['Nombre'] }}</td>
            <td width="15">{{ $cepillo['Id'] }}</td>
            <td>{{ $cepillo['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif

@if(!empty($cadenasSilicona[0]))
    @foreach($cadenasSilicona as $cadenaSilicona)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $cadenaSilicona['textil'] }}</td>
            <td width="10" align="center" class="{{ ($cadenaSilicona['cantidad'] > 1 ? 'masuno' : '') }}">{{ $cadenaSilicona['cantidad'] }}</td>
            <td>{{ $cadenaSilicona['niño'] }}</td>
            <td>{{ $cadenaSilicona['opcion'] }}</td>
            <td width="20">{{ $cadenaSilicona['Nombre'] }}</td>
            <td width="15">{{ $cadenaSilicona['Id'] }}</td>
            <td>{{ $cadenaSilicona['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif


@if(!empty($chupetesColores[0]))
    @foreach($chupetesColores as $chupeteColor)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $chupeteColor['textil'] }}</td>
            <td width="10" align="center" class="{{ ($chupeteColor['cantidad'] > 1 ? 'masuno' : '') }}">{{ $chupeteColor['cantidad'] }}</td>
            <td>{{ $chupeteColor['niño'] }}</td>
            <td>{{ $chupeteColor['opcion'] }}</td>
            <td width="20">{{ $chupeteColor['Nombre'] }}</td>
            <td width="15">{{ $chupeteColor['Id'] }}</td>
            <td>{{ $chupeteColor['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif


@if(!empty($bolasMadera[0]))
    @foreach($bolasMadera as $bolaMadera)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $bolaMadera['textil'] }}</td>
            <td width="10" align="center" class="{{ ($bolaMadera['cantidad'] > 1 ? 'masuno' : '') }}">{{ $bolaMadera['cantidad'] }}</td>
            <td>{{ $bolaMadera['niño'] }}</td>
            <td>{{ $bolaMadera['opcion'] }}</td>
            <td width="20">{{ $bolaMadera['Nombre'] }}</td>
            <td width="15">{{ $bolaMadera['Id'] }}</td>
            <td>{{ $bolaMadera['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif

@if(!empty($bolasPlastico[0]))
    @foreach($bolasPlastico as $bolaPlastico)
        <tr>
            <td width="10"></td>
            <td width="15">{{ $bolaPlastico['textil'] }}</td>
            <td width="10" align="center" class="{{ ($bolaPlastico['cantidad'] > 1 ? 'masuno' : '') }}">{{ $bolaPlastico['cantidad'] }}</td>
            <td>{{ $bolaPlastico['niño'] }}</td>
            <td>{{ $bolaPlastico['opcion'] }}</td>
            <td width="20">{{ $bolaPlastico['Nombre'] }}</td>
            <td width="15">{{ $bolaPlastico['Id'] }}</td>
            <td>{{ $bolaPlastico['Pedido'] }}</td>
        </tr>
    @endforeach
    <tr></tr>
@endif
</html>
