@if($pedidos)
    <table id="users-table" class="table table-condensed table-hover">
        <thead>
        <tr>
            <th style="font-weight: 600">ID</th>
            <th style="font-weight: 600">Fecha</th>
            <th style="font-weight: 600">Estado</th>
            <th style="text-align: center; font-weight: 600">Num Artículos</th>
            <th style="font-weight: 600; text-align: center">Acciones</th>
        </tr>
        </thead>
        <tbody>
        @foreach($pedidos as $row)
            <tr>
                <td>{{ $row->id }}</td>
                <td>{{ $row->fecha }}</td>
                <td>
                    @if($row->validate == 1)
                        <button class="btn btn-pure btn-success fa fa-check waves-effect waves-circle"
                                type="button"></button>
                    @else
                        <button class="btn btn-pure btn-warning icon md-star waves-effect waves-circle waves-classic"
                                type="button"></button>
                    @endif
                </td>
                <td style="text-align: center">{{ $row->num_art }}</td>
                <td style="text-align: center">
                    <a href="{{ route('pedidos.pdf', ['id' => $row->id])  }}" class="btn btn-xs btn-info"><i
                                class="fa fa-search" data-toggle="tooltip" data-placement="top" title="Ver"></i></a>
                </td>
            </tr>
        @endforeach
    </table>
@endif
