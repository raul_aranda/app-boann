<table class="table table-striped table-hover">
    <tr>
        <th>{{ 'Nombre' }}</th>
        <td>{{ $user->name }}</td>
    </tr>

    <tr>
        <th>{{ 'Email' }}</th>
        <td>{{ $user->email }}</td>
    </tr>

    <tr>
        <th>{{ 'Código Cliente' }}</th>
        <td>{{ $user->codigo_cliente }}</td>
    </tr>

    <tr>
        <th>{{ 'Dirección' }}</th>
        <td>{{ $user->direccion }}</td>
    </tr>

    <tr>
        <th>{{ 'Población' }}</th>
        <td>{{ $user->poblacion . ' ('. $user->cp.')' }}</td>
    </tr>

    <tr>
        <th>{{ 'Provincia' }}</th>
        <td>{{ $user->provincia }}</td>
    </tr>

    <tr>
        <th>{{ 'Teléfono' }}</th>
        <td>{{ $user->tlf }}</td>
    </tr>

    <tr>
        <th>{{ 'Activado' }}</th>
        @if ($user->active == 0)
            <td><label class="label label-danger">No activado</label></td>
        @else
            <td><label class="label label-success">Activado</label></td>
        @endif
    </tr>

    <tr>
        <th>{{ 'Creación' }}</th>
        <td>{{ $user->created_at }} ({{ $user->created_at->diffForHumans() }})</td>
    </tr>

    <tr>
        <th>{{ 'Ultima actualización' }}</th>
        <td>{{ $user->updated_at }} ({{ $user->updated_at->diffForHumans() }})</td>
    </tr>
    <tr>
        <th>{{ 'Solicitud catálogo' }}</th>
        {{--<td>{{ (count($catalogo) == 1)? $catalogo->fecha : 'No solicitado' }} </td>--}}
    </tr>

</table>
