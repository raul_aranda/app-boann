@extends('admin.layouts.back')
@section ('title', 'Administración de Usuarios' . ' | ' . 'Ver Usuario')

@section('page-header')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../index.html">Home</a>
        </li>
        <li class="breadcrumb-item active">Usuarios</li>
    </ol>
    <h1 class="page-title">
        Administración de Usuarios
    </h1>
    <div class="page-header-actions">
        @include('admin.partials.user-header-button')
    </div>
@endsection

@section('content')
    <div class="panel">
        <div class="panel-header with-border">
            <h3 class="panel-title">Detalles de - {{ $tienda }}</h3>

        </div><!-- /.box-header -->

        <div class="panel-body">

            <div role="tabpanel">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#overview" aria-controls="overview" role="tab" data-toggle="tab">Resumen</a>
                    </li>

                    <li role="presentation">
                        <a href="#history" aria-controls="history" role="tab" data-toggle="tab">Historial</a>
                    </li>
                </ul>

                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane mt-30 active" id="overview">
                        @include('admin.users.tabs.resumen')
                    </div><!--tab overview profile-->

                    <div role="tabpanel" class="tab-pane mt-30" id="history">
                        @include('admin.users.tabs.historial')
                    </div><!--tab panel history-->

                </div><!--tab content-->

            </div><!--tab panel-->

        </div><!-- /.box-body -->
    </div><!--box-->
@stop