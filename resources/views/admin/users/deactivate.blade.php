@extends('admin.layouts.app')

@section('after-styles')
    {{ Html::style("css/backend/plugin/datatables/dataTables.bootstrap.min.css") }}
@stop

@section('page-header')
    <h1>
        Administración de Usuarios
        <small>Usuarios activos</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Usuarios activos</h3>

            <div class="box-tools pull-right">
                @include('admin.partials.user-header-button')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                @if($data)
                    <table id="users-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>E-mail</th>
                            <th>Activo</th>
                            <th>Población</th>
                            <th>Provincia</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                    </table>
                @endif
            </div>
        </div>

    </div>
@endsection
@section('after-scripts')
    {{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/backend/plugin/datatables/dataTables.bootstrap.min.js") }}

    <script>
        $(function() {
            $('#users-table').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                },
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("user.get") }}',
                    type: 'get',
                    data: {status: 1, trashed: false}
                },
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'email' },
                    { data: 'active', name: 'active', orderable: false, searchabe: false },
                    { data: 'poblacion', name: 'poblacion' },
                    { data: 'provincia', name: 'provincia' },
                    { data: 'actions', name: 'actions', orderable: false, searchabe: false }
                ],
                order: [[0, "asc"]],
                searchDelay: 500
            });
        });
    </script>
@stop