<div class="site-menubar">
    <div class="site-menubar-body">
        <div>
            <div>
                <ul class="site-menu" data-plugin="menu">
                    <li class="site-menu-item">
                        <a href="{{ route('users.index') }}">
                            <i class="site-menu-icon md-view-compact" aria-hidden="true"></i>
                            <span class="site-menu-title">Principal</span>
                        </a>
                    </li>
                    <li class="site-menu-item {{ Request::is('users', 'users/*') ? 'active' : '' }}">
                        <a href="{{ route('users.index') }}">
                            <i class="site-menu-icon md-google-pages" aria-hidden="true"></i>
                            <span class="site-menu-title">Usuarios</span>
                        </a>
                    </li>
                    <li class="site-menu-item {{ Request::is('mapa') ? 'active' : '' }}">
                        <a href="javascript:void(0)">
                            <i class="site-menu-icon md-palette" aria-hidden="true"></i>
                            <span class="site-menu-title">Mapa</span>
                        </a>
                    </li>
                    <li class="site-menu-item {{ Request::is('frontend') ? 'active' : '' }}">
                        <a href="{{ route('user.frontend') }}">
                            <i class="site-menu-icon md-widgets" aria-hidden="true"></i>
                            <span class="site-menu-title">Hacer Pedidos</span>
                        </a>
                    </li>
                    <li class="site-menu-item {{ Request::is('adminpedidos', 'adminpedidos/*') ? 'active' : '' }}">
                        <a href="{{ route('pedidos.listado') }}">
                            <i class="site-menu-icon md-comment-alt-text" aria-hidden="true"></i>
                            <span class="site-menu-title">Listado Pedidos</span>
                        </a>
                    </li>
                    <li class="site-menu-item {{ Request::is('mensajes', 'mensajes/*') ? 'active' : '' }}">
                        <a href="{{ route('mensajes.index') }}">
                            <i class="site-menu-icon md-puzzle-piece" aria-hidden="true"></i>
                            <span class="site-menu-title">Mensajes</span>
                        </a>
                    </li>
                    <li class="site-menu-item {{ Request::is('mensajes', 'mensajes/*') ? 'active' : '' }}">
                        <a href="{{ route('mensajes.index') }}">
                            <i class="site-menu-icon md-chart"></i>
                            <span class="site-menu-title">Banners</span>
                        </a>
                    </li>
                    <li class="site-menu-item {{ Request::is('articulos', 'articulos/*') ? 'active' : '' }}">
                        <a href="{{ route('articulos.index') }}">
                            <i class="site-menu-icon md-apps"></i>
                            <span class="site-menu-title">Artículos</span>
                        </a>
                    </li>
                    <li class="site-menu-item {{ Request::is('stock', 'stock/*') ? 'active' : '' }}">
                        <a href="{{ url('/stock/vue') }}">
                            <i class="site-menu-icon md-scanner"></i>
                            <span class="site-menu-title">Stock</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>