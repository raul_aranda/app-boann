<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html class="no-js css-menubar" lang="es">
<head>
    <meta charset="UTF-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $page_title or "Chupetes Boann" }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

    <!-- Stylesheets -->
    <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    {!! Html::style('/remark/css/bootstrap-extend.min.css') !!}

    {!! Html::style('/remark/material/topicon/assets/css/site.min.css') !!}
    {!! Html::style('/remark/css/bootstrap-extend.min.css') !!}
    {!! Html::style('/remark/css/purple.min.css') !!}
    <!-- Plugins -->
    {!! Html::style('/remark/global/vendor/animsition/animsition.min.css') !!}
    {!! Html::style('/remark/global/vendor/intro-js/introjs.min.css') !!}
    <link href="{{ asset("css/backend/backend.css")}}" rel="stylesheet" type="text/css" />

    <!-- Fonts -->
    {!! Html::style('/remark/global/fonts/material-design/material-design.min.css') !!}
    {!! Html::style('/remark/global/fonts/brand-icons/brand-icons.min.css') !!}
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

    <!--[if lt IE 9]>
    <script src="../../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    <!--[if lt IE 10]>
    <script src="../../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    <!-- Scripts -->
    {!! Html::script('/remark/global/vendor/breakpoints/breakpoints.js') !!}
    <script>
        Breakpoints();
    </script>

    <!-- Styles -->
    @yield('before-styles')

    @yield('after-styles')

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>


</head>
<body class="animsition site-navbar-small">


<!-- Header -->
@include('admin.layouts.header')

<!-- Sidebar -->
@include('admin.layouts.sidebar')

<!-- Content Wrapper. Contains page content -->
<div class="page">
    <!-- Content Header (Page header) -->
    <div class="page-header">
        @yield('page-header')
    </div>
    <!-- Main content -->
    <div class="page-content">
        @include('admin.partials.messages')
        @yield('content')
    </div><!-- /.content -->
</div><!-- /.content-wrapper -->

<!-- Footer -->

@include('admin.layouts.footer')


<!-- Core  -->
{!! Html::script('/remark/global/vendor/babel-external-helpers/babel-external-helpers.js') !!}
{!! Html::script('/remark/global/vendor/jquery/jquery.js') !!}
{!! Html::script('/remark/global/vendor/tether/tether.js') !!}

{!! Html::script('/remark/global/vendor/bootstrap/bootstrap.js') !!}
{!! Html::script('/remark/global/vendor/animsition/animsition.js') !!}

{!! Html::script('/remark/global/vendor/ashoverscroll/jquery-asHoverScroll.js') !!}

<!-- Plugins -->
{!! Html::script('/remark/global/vendor/switchery/switchery.min.js') !!}
{!! Html::script('/remark/global/vendor/intro-js/intro.js') !!}

<!-- Scripts -->
{!! Html::script('/remark/global/js/State.js') !!}
{!! Html::script('/remark/global/js/Component.js') !!}
{!! Html::script('/remark/global/js/Plugin.js') !!}
{!! Html::script('/remark/global/js/Base.min.js') !!}
{!! Html::script('/remark/global/js/Config.js') !!}
{!! Html::script('/remark/material/topicon/assets/js/Section/Menubar.js') !!}
{!! Html::script('/remark/material/topicon/assets/js/Section/Sidebar.js') !!}
{!! Html::script('/remark/material/topicon/assets/js/Section/PageAside.min.js') !!}

<!-- Config -->

<!-- Page -->
{!! Html::script('/remark/material/topicon/assets/js/Site.js') !!}

<script>
    (function(document, window, $) {
        'use strict';
        var Site = window.Site;
        $(document).ready(function() {
            Site.run();
        });
    })(document, window, jQuery);
</script>

<!-- REQUIRED JS SCRIPTS -->
<!-- JavaScripts -->
@yield('before-scripts')

@yield('after-scripts')
</body>
</html>