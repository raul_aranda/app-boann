<form class="form-horizontal">
    <fieldset>
        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="Nombre">Nombre</label>
            <div class="col-md-4">
                <input id="Nombre" name="Nombre" placeholder="Nombre" class="form-control input-md" required="" type="text" value="{{Auth::user()->name}}">
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="email">Email</label>
            <div class="col-md-4">
                <div class="input-group margin-bottom-sm">
                    <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                    <input class="form-control" type="text" placeholder="Email" value="{{Auth::user()->email}}">
                </div>
            </div>

        </div>


        <!-- Text input-->
        <!--<div class="form-group">
            <label class="col-md-4 control-label" for="pwd">Contraseña</label>
            <div class="col-md-4">
                <input id="pwd" name="pwd" placeholder="Contraseña" class="form-control input-md" required="" type="text" value="">
            </div>
        </div>-->
        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="pwd">Código Cliente</label>
            <div class="col-md-2">
                <input id="pwd" name="pwd" placeholder="Contraseña" class="form-control input-md" required="" type="text" value="{{Auth::user()->codigo_cliente}}">
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="pwd">Dirección</label>
            <div class="col-md-4">
                <input id="pwd" name="pwd" placeholder="Contraseña" class="form-control input-md" required="" type="text" value="{{Auth::user()->direccion}}">
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="pwd">Cp</label>
            <div class="col-md-1">
                <input id="pwd" name="pwd" placeholder="Contraseña" class="form-control input-md" required="" type="text" value="{{Auth::user()->cp}}">
            </div>
        </div>
        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="pwd">Población</label>
            <div class="col-md-4">
                <input id="pwd" name="pwd" placeholder="Contraseña" class="form-control input-md" required="" type="text" value="{{Auth::user()->poblacion}}">
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="pwd">Provincia</label>
            <div class="col-md-4">
                <input id="pwd" name="pwd" placeholder="Contraseña" class="form-control input-md" required="" type="text" value="{{Auth::user()->provincia}}">
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="pwd">Teléfono</label>
            <div class="col-md-2">
                <input id="pwd" name="pwd" placeholder="Contraseña" class="form-control input-md" required="" type="text" value="{{Auth::user()->tlf}}">
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="pwd">NIF/CIF</label>
            <div class="col-md-4">
                <input id="pwd" name="pwd" placeholder="Contraseña" class="form-control input-md" required="" type="text" value="{{Auth::user()->barras}}">
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="pwd">Tu comercio online</label>
            <div class="col-md-4">
                <input id="txtcomercio" name="txtcomercio" placeholder="Tu comercio online" class="form-control input-md" required="" type="text" value="{{Auth::user()->agente}}">
            </div>
            <div class="col-md-2">
                <button type="button" onclick="guardaComercioOnline()">Guardar</button>
            </div>
        </div>
        <input type="hidden" id="txtuserId" value="{{ Auth::user()->id }}">
    </fieldset>
</form>

