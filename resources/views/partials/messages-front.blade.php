@extends('layouts.home')

@section('mensajes')
    @if($mensaje['title'] == 'Error')
        <div class="oaerror danger">
            <strong>{{ $mensaje['title'].': ' }}</strong>{{ $mensaje['message'] }}
        </div>
        <input type="hidden" id="txtError" name="txtError" value="error">
    @endif
    @if($mensaje['title'] == 'Ok')
        <div class="oaerror success">
            <strong>{{ $mensaje['title'].': ' }}</strong>{{ $mensaje['message'] }}
        </div>
        <input type="hidden" id="txtError" name="txtError" value="si">
    @endif
@endsection