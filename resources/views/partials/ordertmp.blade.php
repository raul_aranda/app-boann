

<table class="table table-striped table-bordered" style="margin-bottom: 10px;">
    <thead>
    <tr>
        <th width="2%">No.</th>
        <th width="30%">Producto</th>
        <th width="5%">Cantidad</th>
        <th width="20%">Color</th>
        <th width="10%">Tetina</th>
        <th width="10%">Talla</th>
        <th width="19%">Nombre</th>
        <th width="4%"></th>
    </tr>
    </thead>
    <tbody>
    @if (count($data) != 0)
        @php($contador = 1)
    @foreach($data as $item)
        <tr>
            <td>{{$contador}}</td>
            <td>{{$item->descripcion}}</td>
            <td>{{$item->cantidad}}</td>
            <td>{{$item->descripcion_color}}</td>
            <td>{{$item->tetina}}</td>
            <td>{{$item->talla}}</td>
            <td>{{$item->nombre}}</td>
            <td>
                <a href="#" class="btn btn-default" onclick="deleteRecord('{{ $item->id_pedido }}')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
            </td>
        </tr>
        @php($contador ++)
    @endforeach

    @else
        <tr><td colspan="8">No existe ninguna línea de producto</td></tr>
    @endif


    </tbody>
</table>
