<!-- Modal -->
<div class="modal fade" id="dropshippingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <!-- Modal content -->
        <div class="modal-contentDrop">
            <div class="modal-headerDrop">
                <span class="close" id="closeModal">&times;</span>
                <h5 style="color: white">Formulario de DropShipping</h5>
            </div>
            <form id="formDropShipping">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group">
                            {{ Form::label('nombre', 'Nombre*', ['class' => 'col-md-3 control-label']) }}

                            <div class="col-md-8">
                                {{ Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Nombre', 'id' => 'nombre' ]) }}
                            </div><!--col-lg-10-->
                        </div><!--form control-->
                    </div>
                    <div class="row">
                        <div class="form-group">
                            {{ Form::label('apellidos', 'Apellidos*', ['class' => 'col-md-3 control-label']) }}

                            <div class="col-md-4">
                                {{ Form::text('apellido1', null, ['class' => 'form-control', 'placeholder' => 'Primer apellido', 'id' => 'apellido1' ]) }}
                            </div>
                            <div class="col-md-4">
                                {{ Form::text('apellido2', null, ['class' => 'form-control', 'placeholder' => 'Segundo apellido', 'id' => 'apellido2' ]) }}
                            </div><!--col-lg-10-->
                        </div><!--form control-->
                    </div>
                    <div class="row">
                        <div class="form-group">
                            {{ Form::label('telefono', 'Teléfono de contacto*', ['class' => 'col-md-3 control-label']) }}

                            <div class="col-md-5">
                                {{ Form::text('telefono', null, ['class' => 'form-control', 'placeholder' => 'Teléfono de contacto', 'id' => 'telefono' ]) }}
                            </div><!--col-lg-10-->
                        </div><!--form control-->
                    </div>
                    <div class="row">
                        <div class="form-group">
                            {{ Form::label('cpo', 'Código Postal*', ['class' => 'col-md-3 control-label']) }}

                            <div class="col-md-3">
                                {{ Form::text('cpo', null, ['class' => 'form-control', 'placeholder' => 'C.P.', 'id' => 'cpo' ]) }}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            {{ Form::label('provincia', 'Provincia', ['class' => 'col-md-3 control-label']) }}
                            <div class="col-md-5">

                                <select id="provincia" class="form-control">

                                </select>
                            </div><!--col-lg-10-->
                        </div><!--form control-->
                    </div>
                    <div class="row">
                        <div class="form-group">
                            {{ Form::label('localidad', 'Localidad*', ['class' => 'col-md-3 control-label']) }}

                            <div class="col-md-5">
                                <select class="form-control" id="localidad">
                                </select>
                                {{-- Form::text('localidad', null, ['class' => 'form-control', 'placeholder' => 'Localidad', 'id' => 'localidad' ]) --}}
                            </div><!--col-lg-10-->
                        </div><!--form control-->
                    </div>
                    <div class="row">
                        <div class="form-group">
                            {{ Form::label('direccion', 'Dirección*', ['class' => 'col-md-3 control-label']) }}

                            <div class="col-md-3">
                                <select name="" id="via" class="form-control">
                                    <option value="AV">Avenida</option>
                                    <option value="CL">Calle</option>
                                    <option value="CJ">Callej&oacute;n</option>
                                    <option value="CM">Camino</option>
                                    <option value="CR">Carretera</option>
                                    <option value="GL">Glorieta</option>
                                    <option value="PJ">Pasaje</option>
                                    <option value="PS">Paseo</option>
                                    <option value="PZ">Plaza</option>
                                    <option value="PG">Poligono</option>
                                    <option value="RB">Rambla</option>
                                    <option value="Residencia">Residencia</option>
                                    <option value="RD">Ronda</option>
                                    <option value="TR">Traves&iacute;a</option>
                                    <option value="UR">Urbanizaci&oacute;n</option>
                                    <option value="VI">V&iacute;a</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                {{ Form::text('calle', null, ['class' => 'form-control', 'placeholder' => 'Nombre calle', 'id' => 'calle' ]) }}
                            </div>
                            <div class="col-md-1" style="width: 80px">
                                {{ Form::text('numero', null, ['class' => 'form-control', 'placeholder' => 'Num', 'id' => 'numero' ]) }}
                            </div>
                        </div><!--form control-->
                    </div>
                    <div class="row">
                        <div class="form-group">
                            {{ Form::label('resto', 'Resto dirección', ['class' => 'col-md-3 control-label']) }}
                            <div class="col-md-7">
                                {{ Form::text('resto', null, ['class' => 'form-control', 'placeholder' => 'Resto dirección', 'id' => 'resto' ]) }}
                            </div><!--col-lg-10-->
                        </div><!--form control-->
                    </div>
                    <input type="hidden" id="idArticulo" value="">
                </div>

                <div class="modal-footer">
                    <hr>
                    <div class="col-sm-12">
                        <button type="button" id="cancelar" class="btn btn-default btn-pure waves-effect waves-classic" data-dismiss="modal">Cancelar</button>
                        <input type="submit" id="guardar" class="btn btn-primary waves-effect waves-classic" value="Realizar pedido">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

