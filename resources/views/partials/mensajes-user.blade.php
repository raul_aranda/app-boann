@extends('layouts.home')

@section('mensajes-user')
    <div class="modal fade" id="modal-form" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1" style="display: none;">
        <div class="modal-dialog modal-center">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="moda">Modal Title</h4>
                </div>
                <div class="modal-body">
                    <p id="modal-body">One fine body…</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-pure waves-effect waves-classic" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary waves-effect waves-light">Visto</button>
                </div>
            </div>
        </div>
    </div>

<div class="page-content">
    <ul class="taskboard-stages ui-sortable" id="taskboard-stages">
        <li class="taskboard-stage">
            <header class="taskboard-stage-header ui-sortable-handle">
                <h5 class="taskboard-stage-title">Listado de Mensajes</h5>
            </header>
            <div class="taskboard-stage-content">
                <ul class="list-group taskboard-list ui-sortable">
                    @foreach($mensaje as $row)
                        <li class="list-group-item {{ ($row->leido == 1)? 'priority-normal' : 'priority-high' }} ui-sortable-handle" data-taskboard="slidePanel">
                            <div class="checkbox-custom checkbox-primary">
                                <input name="checkbox" type="checkbox" {{ ($row->leido == 1)? 'checked' : ''}}>
                                <label id="title{{$row->id}}" class="task-title"><h5>{{ $row->titulo }}</h5></label>
                            </div>
                            <label id="body-message{{$row->id}}" ><?php echo $row->mensaje;?></label>
                            @php($fecha = \Carbon\Carbon::parse($row->created_at)->format('d-m-Y'))
                            <div class="task-badges"><span class="task-badge task-badge-subtask icon md-calendar">{{ $fecha }}</span></div>
                            <ul class="task-members">
                                @if($row->leido == 1)
                                    <li><i class="fa fa-check fa-2x" aria-hidden="true" style="color: green;"></i>
                                    </li>
                                @else
                                    <!--<li><a href="{{ route('mensaje.leido', $row->id) }}" class="btn btn-primary waves-effect waves-light">Marcar leído</a></li>-->
                                    <button onclick="leido({{ $row->id }})" class="btn btn-primary waves-effect waves-light">Marcar leído</button>
                                @endif
                            </ul>
                        </li>
                        @endforeach
                    </li>
                </ul>
            </div>
        </li>
    </ul>
</div>
    <script>

        /*function modalForm(id){
            $(".modal-title").text($("#title"+id).text());
            $("#modal-body").text($("#body-message"+id).text());
            $('#modal-form').modal();
        }*/
    </script>
@endsection